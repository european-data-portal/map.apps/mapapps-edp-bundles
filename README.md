# European Data Portal - Map Viewer

The Map Viewer of European Data Portal is based on [map.apps](http://www.conterra.de/mapapps).
map.apps is a commercial software product of con terra. It is developed as a client solution that runs within
the user’s browser. It is based on Esri’s JavaScript API thus being able to represent common data formats
such as OGC WMS, Tiled Mapping Service as well as Open Street Map data.

This software component enhances map.apps with certain additional functionality
and does not provide any of the original map.apps source code. You will require map.apps (Version 4.5.x)
to be able to build project.

## Installation

### Preparation

1. Install a supported Tomcat version (Apache Tomcat 7.0.x [7.0.19 or higher] with JDK 1.7.x [Update
25 or higher]; Apache Tomcat 8.0.x [8.0.14 or higher] with JDK 1.8.x [Update 25 or higher])
2. Install Maven 3.x
3. Install map.apps 4.5.x into the Tomcat instance as described in the map.apps Product Installation
Guide. Rename the resulting webapp to “mapapps” (as described in the first step of the Guide) –
otherwise you possibly have to adjust reverse proxy configurations.
a. Adjust the login credentials via the `application.properties` (located at `webapps/mapapps/WEB-INF/classes`)
– properties security.user.admin.* and security.user.editor.*

4. Clone or download the map.apps Git Repository
5. Copy the contents of `conterra_map.apps_4.5.0/sdk/m2-repository` to your local Maven repository (e.g.
USER_HOME/.m2/repository)
6. Create a file `deploy.properties` with the following contents in the Git root directory (you can use the `deploy.properties.template`):

```
deployment.admin.username=<security.user.admin.name of step 3a> 
deployment.admin.password=<security.user.admin.password of step 3a>
deployment.server.url=http://localhost:8080/mapapps
```

7. Copy over some specific libraries into the `lib` folder. The files are contained in the map.apps installation (`conterra_map.apps_4.5.0/sdk/libs/map.apps-3.10.0` and `conterra_map.apps_4.5.0/sdk/libs`). The folder should contain the following files:

![lib folder contents](./lib_folder.png "lib folder contents")


8. Run the installation script `install.sh` in the `lib` folder.

### Deployment

1. Now deploy the map.apps European Data Portal application, running
`mvn clean install -P compress,deploy -Ddeployment.properties=./deploy.properties`

2. Adjust the proxy URL (required to allow loading of external web services and data):

Add (or adjust) the following properties in the `application.properties` (located at webapps/mapapps/WEB-INF/classes).

```
proxy.service.location=/mapapps-proxy
proxy.use.always=false
```

3. Finally, you need to copy two files from the src/main/js/apps/edp-mapapps directory
to webapps/mapapps:
   * index.html
   * init.css
4. Restart Tomcat or reload the mapapps Context.

## Obtaining a map.apps License

In order to use this software component you need a license for
[con terra map.apps](http://www.conterra.de/mapapps) (version 4.5.0).
An evaluation license will be available on request. For further information,
please get in touch with:

* [edp@conterra.de](mailto:edp@conterra.de)

## License

This component is released under the Apache License 2.0.
