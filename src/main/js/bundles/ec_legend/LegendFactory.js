define(["dojo/_base/lang", "dojo/_base/declare", "./LegendWidget"], function(
  d_lang,
  declare,
  LegendWidget
) {
  return declare([], {
    createInstance: function() {
      var legendOpts = d_lang.mixin({}, this._properties.legendOpts || {}, {
        esriMap: this._esriMap,
        mapModel: this._mapModel
      });
      return new LegendWidget(legendOpts);
    }
  });
});
