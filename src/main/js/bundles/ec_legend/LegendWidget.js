define([
  "dojo/_base/declare",
  "dojo/dom-construct",
  "dijit/_Widget",
  "esri/dijit/Legend",
  "./Legend",
  "legend/LegendUI"
], function(declare, domConstruct, _Widget, EsriLegend, Legend, LegendUI) {
  return declare([_Widget, LegendUI], {
    baseClass: "ecLegend",
    _createLegend: function() {
      var legendNode = (this._legendNode = domConstruct.create(
        "div",
        {},
        this.domNode
      ));
      return new Legend(
        {
          map: this.esriMap,
          respectCurrentMapScale: this.respectCurrentMapScale,
          arrangement: this.alignmentLeft
            ? EsriLegend.ALIGN_LEFT
            : EsriLegend.ALIGN_RIGHT,
          mapModel: this.mapModel,
          showBaseLayer: this.showBaseLayer,
          layerInfos: this.layerInfos,
          autoUpdate: this.autoUpdate
        },
        legendNode
      );
    }
  });
});
