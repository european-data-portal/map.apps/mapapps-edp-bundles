define([
  "dojo/_base/declare",
  "dijit/_Widget",
  "dijit/_TemplatedMixin",
  "dijit/_WidgetsInTemplateMixin",
  "./Utils",
  "dojo/text!./templates/legal-disclaimer.html",
  "dijit/layout/BorderContainer",
  "dijit/layout/ContentPane",
  "dijit/form/Button"
], function(
  declare,
  _Widget,
  _TemplatedMixin,
  _WidgetsInTemplateMixin,
  Utils,
  templateStringContent,
  BorderContainer,
  ContentPane,
  Button
) {
  return declare([_Widget, _TemplatedMixin, _WidgetsInTemplateMixin], {
    baseClass: "ecDisclaimerWrapper",
    templateString: templateStringContent,
    constructor: function(args) {
      this.overlayUtils = args.overlayUtils;
    },
    buildRendering: function() {
      this.inherited(arguments);
    },
    _closeDisclaimer: function() {
      Utils.hideLegalDisclaimer();
      //we disable this, at its very buggy
      //                            this.overlayUtils.removeGlobalOverlay();
    }
  });
});
