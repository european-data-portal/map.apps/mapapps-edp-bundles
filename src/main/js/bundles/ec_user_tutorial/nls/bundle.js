define({
  "root": {
    "ec_user_tutorial": {
      "bundleName": "ec-user-tutorial",
      "bundleDescription": "ec-user-tutorial",
      "externalResourceDisclaimer": {
        "introduction": "This map viewer accesses external services and displays their data. These external services are not maintained by the European Commission and therefore we have no influence on their availability/stability. The following issues may occur:",
        "rotatingCircle": "A rotating circle in the map view indicates that the viewer is waiting for a response from a data source. This may be caused by an unavailable server or network issues",
        "layerZoomLevel": "Some layer sources may not display at certain zoom levels. This results from restrictions set by the external service provider. You may need to zoom in or out in order to display the data of the layer",
        "layerPanning": "Data accessed from an external server may be located outside the current map view. This map viewer tries to automatically identify the covered area. Still, in some cases the metadata are not complete or erroneous so that you have to pan to the actual extent.",
        "detailInformation": "If you request detailed information for a selected feature this will be handled by a specific request to the map server. If the response to this request is malformed or empty (e.g. due to a server error) no information can be displayed and an empty window will show up.",
        "technicalSupport": "If you encounter any other problems do not hesitate to get in touch with the technical support:"
      },
      "tutorial": {
        "welcome": "Welcome to the map viewer of the European Data Portal",
        "familiarise": "This small introduction has the aim to familiarise you with the elements and functionality of the map viewer.",
        "navigation": "Click and hold the left mouse button to pan on the map.",
        "zoom": "These buttons change the zoom level on the map. Alternatively you can use the mouse wheel to adjust the zoom.",
        "features": "Additional functionality is available through these buttons. They act as toggles and enable or disable the given functionality.",
        "legend": "The legend can be used to examine the available map layers and enable or disable their current display on the map. The names are directly derived from the externally accessed service.",
        "transparency": "You can also adjust the transparency of a layer.",
        "featureinfo": "Some data can be examined even in more detail. When this feature is activated you can click on the map to query additional information.",
        "donotshow": "Do not display this welcome screen again.",
        "okButton": "Ok",
        "closeButton": "Close"
      }
    }
  },
  "bg": true,
  "cs": true,
  "da": true,
  "de": true,
  "el": true,
  "es": true,
  "et": true,
  "fi": true,
  "fr": true,
  "ga": true,
  "hr": true,
  "hu": true,
  "it": true,
  "lt": true,
  "lv": true,
  "mt": true,
  "nl": true,
  "pl": true,
  "pt": true,
  "ro": true,
  "sk": true,
  "sl": true,
  "sv": true
});