define({
  "ec_user_tutorial": {
    "bundleName": "ec-felhasználó-bemutató",
    "bundleDescription": "ec-felhasználó-bemutató",
    "externalResourceDisclaimer": {
      "introduction": "Ez a térkép megjelenítő hozzáfér a külső szolgáltatások és megjeleníti az adatokat. Ezek a külső szolgáltatások nem tartja fenn az Európai Bizottság, és ezért nincs hatással a rendelkezésre állás / stabilitást. A következő problémák jelentkezhetnek:",
      "rotatingCircle": "A forgó kört a térképen jelzi, hogy a néző vár választ egy adatforrást. Ezt okozhatja nem elérhető kiszolgálót vagy hálózati probléma",
      "layerZoomLevel": "Néhány réteg források esetleg nem jelennek meg bizonyos nagyítási szinteken. Ez abból korlátozásai külső szolgáltató. Szükség lehet nagyítani vagy kicsinyíteni annak érdekében, hogy az adatok megjelenítésére a réteg",
      "layerPanning": "Adatokhoz való hozzáférés a külső szerveren kívül is lehetnek az aktuális térkép nézetet. Ez a térkép megjelenítő megpróbálja automatikusan azonosítani a lefedett terület. Mégis, bizonyos esetekben a metaadat nem teljes vagy hibás úgy, hogy a serpenyőt a tényleges mértékét.",
      "detailInformation": "Ha részletes tájékoztatást kért a kiválasztott funkció ez kezeli külön kérése, hogy a térkép szerver. Ha a válasz erre a felkérésre hibás, vagy üres (pl miatt a szerver hibája) nincs információ jeleníthető meg, és egy üres ablak fog megjelenni.",
      "technicalSupport": "Ha bármilyen más probléma, ne habozzon, vegye fel a kapcsolatot a technikai támogatást:"
    },
    "tutorial": {
      "welcome": "Üdvözöljük a térképen nézőt az European Data Portal",
      "familiarise": "Ez a kis bevezetés az a célja, hogy megismertesse Önt az elemekkel és a funkcionalitás a térképen nézőt.",
      "navigation": "Kattintson és tartsa lenyomva a bal egérgombot, hogy pan a térképen.",
      "zoom": "Ezek a gombok megváltoztathatja a nagyítás mértékét a térképen. Másik lehetőség, hogy a görgő a zoom beállításához.",
      "features": "További funkciók keresztül érhető el ezeket a gombokat. Úgy tesznek, és kikapcsolja, és engedélyezze vagy tiltsa le az adott funkciót.",
      "legend": "A legenda lehet használni, hogy vizsgálja meg a rendelkezésre álló térkép rétegek és engedélyezze vagy tiltsa le a jelenlegi képernyő a térképen. A nevek közvetlenül levezethető a külsőleg elérhető szolgáltatás.",
      "transparency": "Azt is beállíthatja az átláthatóságot egy réteg.",
      "featureinfo": "Néhány adat lehet vizsgálni még részletesebben. Ha ez a funkció be van kapcsolva akkor kattintson a térképen lekérdezni további információkat.",
      "donotshow": "Ne jelenjen meg ez üdvözlő képernyő újra.",
      "okButton": "Oké",
      "closeButton": "Bezár"
    }
  }
});