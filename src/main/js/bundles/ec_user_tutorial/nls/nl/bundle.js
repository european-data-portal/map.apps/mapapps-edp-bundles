define({
  "ec_user_tutorial": {
    "bundleName": "ec-gebruiker-zelfstudie",
    "bundleDescription": "ec-gebruiker-zelfstudie",
    "externalResourceDisclaimer": {
      "introduction": "Deze kaart kijker toegang tot externe diensten en geeft hun gegevens. Deze externe diensten worden niet beheerd door de Europese Commissie en dus hebben we geen invloed op de beschikbaarheid / stabiliteit. De volgende problemen kunnen optreden:",
      "rotatingCircle": "Een draaiende cirkel in de kaartweergave geeft aan dat de kijker op een reactie van een gegevensbron wacht. Dit kan worden veroorzaakt door een server niet beschikbaar is of netwerkproblemen",
      "layerZoomLevel": "Sommige laag bronnen kan niet worden weergegeven op bepaalde zoomniveaus. Dit vloeit voort uit de beperkingen door de externe dienstverlener te stellen. Het kan nodig zijn om in of uit te zoomen, om de gegevens van de laag weer te geven",
      "layerPanning": "Data toegankelijk vanaf een externe server kan zich buiten de huidige kaartweergave. Deze kaart kijker probeert de overdekte ruimte automatisch te identificeren. Nog, in sommige gevallen de metadata zijn niet compleet of onjuiste zodat u de pan op de werkelijke omvang.",
      "detailInformation": "Als u gedetailleerde informatie aan te vragen voor een geselecteerde functie zal dit worden behandeld door een specifiek verzoek om de kaart server. Als het antwoord op dit verzoek is onjuist of leeg is (bijvoorbeeld als gevolg van een server error) geen informatie kan worden getoond en een leeg scherm zal verschijnen.",
      "technicalSupport": "Als u problemen andere problemen aarzel dan niet om contact met de technische ondersteuning te krijgen:"
    },
    "tutorial": {
      "welcome": "Welkom op de kaart kijker van de European Data Portal",
      "familiarise": "Dit kleine introductie heeft als doel om u vertrouwd te maken met de elementen en de functionaliteit van de kaart van de kijker.",
      "navigation": "Klik en houd de linkermuisknop ingedrukt om te pannen op de kaart.",
      "zoom": "Deze knoppen veranderen het zoomniveau op de kaart. U kunt ook het muiswiel gebruiken om de zoomfunctie aan te passen.",
      "features": "Extra functionaliteit is beschikbaar via deze knoppen. Ze fungeren als schakelt en in- of uitschakelen van de gegeven functionaliteit.",
      "legend": "De legende kan worden gebruikt om de beschikbare kaart lagen onderzoeken en in- of uitschakelen van hun huidige weergave op de kaart. De namen zijn rechtstreeks afgeleid van de extern toegankelijke dienst.",
      "transparency": "U kunt ook de transparantie van een laag aan te passen.",
      "featureinfo": "Bepaalde gegevens kunnen worden onderzocht nog nader. Wanneer deze functie is geactiveerd kunt u klikken op de kaart om aanvullende informatie te vragen.",
      "donotshow": "Doe dit welkomstscherm niet weer te geven.",
      "okButton": "Ok",
      "closeButton": "Dicht"
    }
  }
});