define({
  "ec_user_tutorial": {
    "bundleName": "ec-vartotojas-pamoka",
    "bundleDescription": "ec-vartotojas-pamoka",
    "externalResourceDisclaimer": {
      "introduction": "Šis planas žiūrovas prieina išorės tarnybas ir rodo savo duomenis. Šie išoriniai paslaugos nėra palaikoma Europos Komisijai ir todėl mes turime ne nuo jų buvimo / stabilumo įtaką. Gali pasireikšti šie klausimai:",
      "rotatingCircle": "Besisukantis ratas žemėlapio vaizdą rodo, kad žiūrovo laukia iš duomenų šaltinio atsaką. Tai gali būti dėl negrąžinamų serveryje ar tinkle klausimais",
      "layerZoomLevel": "Kai sluoksnis šaltiniai negali rodyti tam tikru masteliu Tai kyla iš išorės paslaugų teikėjo nustatytų apribojimų, gali tekti didinti arba mažinti mastelį, kad būtų rodoma sluoksnio duomenis",
      "layerPanning": "Atvertas iš išorinio serverio Duomenys gali būti įrengtas už dabartinę žemėlapio rodinyje. Šis planas žiūrovas bando automatiškai nustatyti padengtas plotas. Vis dėlto kai kuriais atvejais metaduomenys neišsami arba klaidinga todėl, kad jūs turite slinkti į faktinę mastu.",
      "detailInformation": "Jei prašote išsamią informaciją pasirinktoje funkcija tai bus tvarkomi konkrečiu prašymu į žemėlapį serveryje. Jei į šį prašymą atsakymas yra neteisingas formatas arba tuščia (pavyzdžiui, dėl serverio klaidos) Nr informacija gali būti rodoma ir tuščias langas bus rodomas.",
      "technicalSupport": "Jei susidūrėte su kokiomis nors kitų problemų, nedvejodami susisiekite su techninės pagalbos:"
    },
    "tutorial": {
      "welcome": "Sveiki atvykę į žemėlapį žiūrovas European Data Portal",
      "familiarise": "Šis mažas įvedimas turi tikslą supažindinti Jus su elementais ir funkcionalumo žemėlapyje žiūrovą.",
      "navigation": "Spauskite ir laikykite nuspaudę kairįjį pelės mygtuką, norėdami slinkti žemėlapyje.",
      "zoom": "Šie mygtukai pakeisti mastelį žemėlapyje. Arba galite naudoti pelės ratuką nustatykite priartinimą.",
      "features": "Papildomas funkcionalumas yra prieinama per šių mygtukų. Jie veikia kaip perjungia ir įjungti arba išjungti tikrą funkcionalumą.",
      "legend": "Legenda gali būti naudojamas išnagrinėti galimus žemėlapio sluoksnius ir įjungti arba išjungti savo dabartinę rodomos žemėlapyje. Pavadinimai yra tiesiogiai išauginti iš išorės prieinama paslauga.",
      "transparency": "Jūs taip pat galite reguliuoti sluoksnio skaidrumas.",
      "featureinfo": "Kai kurie duomenys gali būti nagrinėjamas net išsamiau. Kai ši funkcija įjungta, jūs galite spustelėti ant žemėlapio užklausą papildomos informacijos.",
      "donotshow": "Nerodyti šį pasveikinimo ekraną dar kartą.",
      "okButton": "Gerai",
      "closeButton": "Arti"
    }
  }
});