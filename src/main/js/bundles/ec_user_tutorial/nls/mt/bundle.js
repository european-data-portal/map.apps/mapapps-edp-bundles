define({
  "ec_user_tutorial": {
    "bundleName": "ec-utent tutorja",
    "bundleDescription": "ec-utent tutorja",
    "externalResourceDisclaimer": {
      "introduction": "Dan telespettatur mappa aċċessi servizzi esterni u turi  data tagħhom. Dawn is-servizzi esterni mhumiex miżmuma mill-Kummissjoni Ewropea u għalhekk għandna l-ebda influwenza fuq id-disponibbiltà tagħhom / istabbiltà. Il-kwistjonijiet li ġejjin jistgħu jseħħu:",
      "rotatingCircle": "Ċirku li jdur fil-fehma mappa tindika li l-telespettatur qed jistenna għal rispons minn sors tad-data Dan jista 'jkun ikkawżat minn server mhux disponibbli jew kwistjonijiet tan-network",
      "layerZoomLevel": "Xi sorsi saff ma jistax jesponi f&#39;ċerti livelli zoom. Dan jirriżulta minn restrizzjonijiet stabbiliti mill-fornitur estern tas-servizzi. Jista &#39;jkollok bżonn biex zoom in jew out sabiex juru d-data tas-saff",
      "layerPanning": "Data li jkollhom fakoltà minn server estern jistgħu jinstabu barra mill-opinjoni attwali mappa. Dan telespettatur mappa jipprova jidentifika awtomatikament iż-żona koperta. Xorta, f&#39;xi każijiet il-metadata mhumiex kompluti jew żbaljati sabiex ikollok biex pan sal-limitu attwali.",
      "detailInformation": "Jekk inti titlob informazzjoni dettaljata għal karatteristika magħżula dan ser jiġu ttrattati permezz ta &#39;talba speċifika lill-server mappa. Jekk ir-rispons għal din it-talba hija uretri jew vojta (eg minħabba żball server)-ebda informazzjoni tista &#39;tintwera u tieqa vojta se juru up.",
      "technicalSupport": "Jekk inti tiltaqa &#39;ma xi problemi oħra toqgħodx lura milli tagħmel kuntatt ma&#39; l-appoġġ tekniku:"
    },
    "tutorial": {
      "welcome": "Merħba għall-telespettatur mappa European Data Portal",
      "familiarise": "Din l-introduzzjoni żgħira għandu l-għan li jiffamiljarizzaw inti ma &#39;l-elementi u l-funzjonalità tal-telespettatur mappa.",
      "navigation": "Ikklikkja u żomm il-buttuna maws xellug biex pan fuq il-mappa.",
      "zoom": "Dawn il-buttuni jibdlu l-livell zoom fuq il-mappa. Inkella tista &#39;tuża l-rota maws biex taġġusta l-zoom.",
      "features": "Funzjonalità addizzjonali hija disponibbli permezz ta &#39;dawn buttuni. Huma jaġixxu bħala toggles u jippermettu jew iwaqqaf il-funzjonalità partikolari.",
      "legend": "Il-leġġenda jistgħu jintużaw biex jeżamina l-saffi mappa disponibbli u jippermettu jew iwaqqaf wiri attwali tagħhom fuq il-mappa. L-ismijiet huma direttament derivati ​​mis-servizz esternament aċċessati.",
      "transparency": "Tista &#39;wkoll jadattaw il-trasparenza ta&#39; saff.",
      "featureinfo": "Xi data tista &#39;tiġi eżaminata wkoll f&#39;aktar dettall. Meta din il-karatteristika huwa attivat tista &#39;tagħfas fuq il-mappa li jsaqsu informazzjoni addizzjonali.",
      "donotshow": "Ma jurux dan l-iskrin milqugħa mill-ġdid.",
      "okButton": "Ok",
      "closeButton": "Agħlaq"
    }
  }
});