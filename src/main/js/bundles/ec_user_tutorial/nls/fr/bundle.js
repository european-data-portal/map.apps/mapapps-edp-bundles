define({
  "ec_user_tutorial": {
    "bundleName": "ec-utilisateur-tutoriel",
    "bundleDescription": "ec-utilisateur-tutoriel",
    "externalResourceDisclaimer": {
      "introduction": "Cette carte spectateur accède aux services externes et affiche leurs données. Ces services externes ne sont pas entretenus par la Commission européenne et nous devons donc aucune influence sur leur disponibilité / stabilité. Les questions suivantes peuvent se produire:",
      "rotatingCircle": "Un cercle tournant dans la vue de la carte indique que le spectateur attend une réponse d&#39;une source de données. Cela peut être causé par un des problèmes de serveur ou réseau indisponible",
      "layerZoomLevel": "Certaines sources de la couche peuvent ne pas afficher à certains niveaux de zoom. Il en résulte des restrictions définies par le fournisseur de service externe. Vous pouvez avoir besoin de zoomer ou dézoomer pour afficher les données de la couche",
      "layerPanning": "Données accédées depuis un serveur externe peuvent être situés en dehors de la vue actuelle de la carte. Cette carte spectateur tente d&#39;identifier automatiquement la zone couverte. Pourtant, dans certains cas, les métadonnées ne sont pas complètes ou erronées afin que vous avez pour vous déplacer dans la mesure réelle.",
      "detailInformation": "Si vous demandez des informations détaillées pour une fonction sélectionnée ce sera assurée par une demande spécifique au serveur de la carte. Si la réponse à cette demande est malformé ou vide (par exemple en raison d&#39;une erreur de serveur) aucune information ne peut être affichée et une fenêtre vide apparaîtra.",
      "technicalSupport": "Si vous rencontrez d&#39;autres problèmes ne pas hésiter à entrer en contact avec l&#39;appui technique:"
    },
    "tutorial": {
      "welcome": "Bienvenue à la carte spectateur du European Data Portal",
      "familiarise": "Cette petite introduction a pour but de vous familiariser avec les éléments et les fonctionnalités de la carte spectateur.",
      "navigation": "Cliquez et maintenez le bouton gauche de la souris pour vous déplacer sur la carte.",
      "zoom": "Ces boutons changent le niveau de zoom sur la carte. Sinon, vous pouvez utiliser la molette de la souris pour ajuster le zoom.",
      "features": "Une fonctionnalité supplémentaire est disponible par le biais de ces boutons. Ils agissent comme bascule et activer ou désactiver la fonctionnalité donnée.",
      "legend": "La légende peut être utilisé pour examiner les couches cartographiques disponibles et activer ou désactiver l&#39;affichage actuel sur la carte. Les noms sont directement dérivées du service accédé à l&#39;extérieur.",
      "transparency": "Vous pouvez également ajuster la transparence d&#39;une couche.",
      "featureinfo": "Certaines données peuvent être examinés même plus en détail. Lorsque cette fonction est activée, vous pouvez cliquer sur la carte pour demander des informations supplémentaires.",
      "donotshow": "Ne pas afficher à nouveau l&#39;écran de bienvenue.",
      "okButton": "D&#39;accord",
      "closeButton": "Fermer"
    }
  }
});