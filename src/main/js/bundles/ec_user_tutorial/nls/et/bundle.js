define({
  "ec_user_tutorial": {
    "bundleName": "EÜ-kasutaja-õpetus",
    "bundleDescription": "EÜ-kasutaja-õpetus",
    "externalResourceDisclaimer": {
      "introduction": "See kaart vaataja pääseb sisseostetavate teenuste ja kuvab oma andmed. Need välised teenused ei ole haldab Euroopa Komisjon ja seega ei ole meil mõju nende kättesaadavus / stabiilsus. Järgnevad küsimused võivad tekkida:",
      "rotatingCircle": "Pöörlev ring kaardi vaade näitab, et vaataja ootab vastust andmeallikas. See võib olla põhjustatud mittekättesaadava server või võrguküsimustega",
      "layerZoomLevel": "Mõned kiht allikatest võib näidata teatud suurenduse taset. See tuleneb poolt kehtestatud piirangud väline teenuseosutaja. Võimalik, et peate sisse või välja suumimiseks, et kuvada andmeid kihi",
      "layerPanning": "Andmed külastatud välisest server võib asuda väljaspool praeguse kaardi vaade. See kaart vaataja üritab automaatselt tuvastada hõlmatud ala. Siiski, mõningatel juhtudel metaandmed ei ole täielik või ekslik, nii et sa pead pan tegelikku ulatust.",
      "detailInformation": "Kui teil nõuda üksikasjalikku teavet valitud funktsioon selle eest hoolitseb konkreetse taotluse kaardiserver. Kui vastus sellele palvele on vigane või tühjad (nt tänu serveri viga) andmed ei kuvata ja tühja akna ilmub.",
      "technicalSupport": "Kui sul tekib mingeid muid probleeme ärge kartke võtke tehniline tugi:"
    },
    "tutorial": {
      "welcome": "Tere tulemast kaart vaataja European Data Portal",
      "familiarise": "See väike sissejuhatus uurimistöö eesmärgiks on tutvustada teile elemente ja funktsionaalsust kaardi vaataja.",
      "navigation": "Klõpsake ja hoidke hiire vasakut nuppu, et pan kaardil.",
      "zoom": "Need nupud muuta suurenduse taset kaardil. Alternatiivina saab kasutada hiire ratast reguleerida suumi.",
      "features": "Täiendav funktsionaalsus on saadaval neid nuppe. Nad tegutsevad lülitab ja lubada või keelata teatud funktsioone.",
      "legend": "Legend saab tutvuda saadaval kaardikihid ning lubada või keelata oma praeguse ekraani kaardil. Nimed on otseselt tuletatud väliselt külastatud teenust.",
      "transparency": "Samuti saab muuta läbipaistvamaks kiht.",
      "featureinfo": "Mõned andmed saab uurida isegi üksikasjalikumalt. Kui see funktsioon on aktiveeritud võite klõpsata kaardil pärida täiendavat infot.",
      "donotshow": "Ära näita seda tiitelkuvaks uuesti.",
      "okButton": "Okei",
      "closeButton": "Lähedane"
    }
  }
});