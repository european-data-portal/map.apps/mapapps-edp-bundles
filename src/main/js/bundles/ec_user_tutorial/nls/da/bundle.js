define({
  "ec_user_tutorial": {
    "bundleName": "ec-user-tutorial",
    "bundleDescription": "ec-user-tutorial",
    "externalResourceDisclaimer": {
      "introduction": "Dette kort seeren adgang eksterne tjenester og viser deres data. Disse eksterne tjenester er ikke vedligeholdes af Europa-Kommissionen, og derfor har vi ingen indflydelse på deres tilgængelighed / stabilitet. Følgende problemer kan opstå:",
      "rotatingCircle": "En roterende cirkel i kortvisningen angiver, at seeren venter på et svar fra en datakilde. Dette kan være forårsaget af en utilgængelig server eller netværksproblemer",
      "layerZoomLevel": "Nogle lag kilder vises muligvis ikke på bestemte zoomniveauer. Dette skyldes restriktioner fastsat af den eksterne tjenesteyder. Du kan få brug for at zoome ind eller ud for at vise data i laget",
      "layerPanning": "Data tilgås fra en ekstern server, kan være placeret uden for den aktuelle kortvisning. Dette kort seeren forsøger automatisk at identificere det dækkede område. Stadig, i nogle tilfælde metadataene er ikke komplet eller fejlagtig, så du er nødt til at panorere til den faktiske omfang.",
      "detailInformation": "Hvis du anmoder om detaljerede oplysninger om en valgt funktion dette vil blive håndteret af en specifik anmodning til kortet serveren. Hvis svaret på denne anmodning er forkert udformet eller tom (fx på grund af en serverfejl) ingen oplysninger kan vises og et tomt vindue vil dukke op.",
      "technicalSupport": "Hvis du støder på andre problemer tøv ikke med at komme i kontakt med den tekniske support:"
    },
    "tutorial": {
      "welcome": "Velkommen til kortet seeren af ​​den European Data Portal",
      "familiarise": "Denne lille introduktion har til formål at gøre dig fortrolig med de elementer og funktionalitet på kortet beskueren.",
      "navigation": "Klik og hold venstre museknap nede for at panorere på kortet.",
      "zoom": "Disse knapper ændre zoomniveauet på kortet. Alternativt kan du bruge musehjulet til at justere zoom.",
      "features": "Ekstra funktionalitet er tilgængelig via disse knapper. De fungerer som skifter, og aktivere eller deaktivere den givne funktionalitet.",
      "legend": "Legenden kan bruges til at undersøge de tilgængelige kortlag og aktivere eller deaktivere deres nuværende visning på kortet. Navnene er direkte afledt af eksternt adgang tjeneste.",
      "transparency": "Du kan også justere gennemsigtigheden af ​​et lag.",
      "featureinfo": "Nogle data kan undersøges selv i flere detaljer. Når denne funktion er aktiveret, kan du klikke på kortet for at forespørge yderligere oplysninger.",
      "donotshow": "Vis ikke denne velkommen skærmen igen.",
      "okButton": "Ok",
      "closeButton": "Luk"
    }
  }
});