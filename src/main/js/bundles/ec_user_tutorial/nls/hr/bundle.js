define({
  "ec_user_tutorial": {
    "bundleName": "ec-korisnik-udžbenik",
    "bundleDescription": "ec-korisnik-udžbenik",
    "externalResourceDisclaimer": {
      "introduction": "Ovaj preglednik karta pristupa vanjskim uslugama i prikazuje svoje podatke. Ove vanjske usluge nisu održavana od strane Europske komisije i zato nemamo utjecaja na njihovu dostupnost / stabilnost. Sljedeća pitanja mogu se pojaviti:",
      "rotatingCircle": "Rotirajuće krug u prikazu karte ukazuje na to da gledatelj čeka odgovor iz izvora podataka. To može biti uzrokovano nedostupnih poslužitelja ili mreža pitanja",
      "layerZoomLevel": "Neki izvori sloj ne može prikazati na nekim razinama zumiranja. To proizlazi iz ograničenja postavljenih od strane vanjskog davatelja usluga. Možda ćete morati povećavati ili smanjivati ​​kako bi se prikazali podatke sloja",
      "layerPanning": "Podaci pristupiti iz vanjskog poslužitelja može se nalaziti izvan trenutnom prikazu karte. Ovaj preglednik karta nastoji automatski prepoznati natkriveni prostor. Ipak, u nekim slučajevima metapodataka nisu potpuni ili pogrešna, tako da ćete morati pomicanje do stvarnog mjeri.",
      "detailInformation": "Ako zatražite detaljne informacije za odabranu značajku to će biti obrađene od strane određenog zahtjev poslužitelju na karti. Ako je odgovor na ovaj zahtjev nije ispravan ili prazna (npr zbog pogreške poslužitelja) nema informacija može se prikazati i prazan prozor će se pojaviti.",
      "technicalSupport": "Ako naiđete na bilo kakve druge probleme ne ustručavajte se dobiti u kontaktu s tehničke podrške:"
    },
    "tutorial": {
      "welcome": "Dobrodošli u pregledniku karti European Data Portal",
      "familiarise": "Ovaj mali uvod ima za cilj da vas upoznaju s elementima i funkcionalnosti pregledniku karti.",
      "navigation": "Kliknite i držite lijevu tipku miša za kretanje na karti.",
      "zoom": "Ovi gumbi promijeniti razinu zumiranja na karti. Alternativno možete koristiti kotačić miša za podešavanje zuma.",
      "features": "Dodatna funkcionalnost dostupna je kroz ove tipke. Oni djeluju kao prebacuje i omogućiti ili onemogućiti dati funkcionalnosti.",
      "legend": "Legenda se može koristiti za ispitivanje dostupnih slojeva karte i omogućiti ili onemogućiti njihov trenutni prikaz na karti. Nazivi izravno izvedene iz vanjske pristupa uslugu.",
      "transparency": "Također možete podesiti transparentnost sloja.",
      "featureinfo": "Neki podaci mogu se ispitati još detaljnije. Kad je ova značajka možete kliknuti na karti na upit dodatne informacije.",
      "donotshow": "Nemoj više prikazivati ​​ovaj zaslon dobrodošlice.",
      "okButton": "U redu",
      "closeButton": "Zatvori"
    }
  }
});