define({
  "ec_user_tutorial": {
    "bundleName": "ec-utente dimostrativi",
    "bundleDescription": "ec-utente dimostrativi",
    "externalResourceDisclaimer": {
      "introduction": "Questa mappa spettatore accede ai servizi esterni e visualizza i loro dati. Questi servizi esterni non sono mantenuti da parte della Commissione europea e quindi non abbiamo alcuna influenza sulla loro disponibilità / stabilità. Potrebbero verificarsi i seguenti problemi:",
      "rotatingCircle": "Un cerchio in rotazione nella vista mappa indica che lo spettatore è in attesa di una risposta da una fonte di dati. Ciò può essere causato da un server non disponibile o problemi di rete.",
      "layerZoomLevel": "Alcune fonti dei livelli potrebbero non essere visualizzati a certi livelli di zoom. Ciò risulta dalle restrizioni stabilite dal fornitore esterno di servizi. Potrebbe essere necessario ingrandire o ridurre in modo da visualizzare i dati del livello",
      "layerPanning": "I dati a cui si accede da server esterni possono essere situati al di fuori della vista mappa corrente. Questa mappa spettatore cerca di individuare automaticamente l&#39;area coperta. Tuttavia, in alcuni casi, i metadati non sono complete o mancanti in modo da avere per fare una panoramica per quanto reale.",
      "detailInformation": "Se si richiedono informazioni dettagliate per una funzione selezionata sarà gestito da una specifica richiesta al server mappa. Se la risposta a questa richiesta non è corretto o vuoto (ad esempio a causa di un errore del server) nessuna informazione può essere visualizzato e una finestra vuota apparirà.",
      "technicalSupport": "Se si verificano altri problemi non esitate a mettervi in ​​contatto con il supporto tecnico:"
    },
    "tutorial": {
      "welcome": "Benvenuti nel map viewer del European Data Portal",
      "familiarise": "Questa piccola introduzione ha lo scopo di familiarizzare con gli elementi e le funzionalità della mappa spettatore.",
      "navigation": "Clicca e tieni premuto il tasto sinistro del mouse per eseguire una panoramica sulla mappa.",
      "zoom": "Questi pulsanti modificano il livello di zoom sulla mappa. In alternativa è possibile utilizzare la rotella del mouse per regolare lo zoom.",
      "features": "Ulteriori funzionalità è disponibile attraverso questi pulsanti. Agiscono come alterna e attivare o disattivare la funzionalità di data.",
      "legend": "La leggenda può essere utilizzato per esaminare i layer disponibili e attivare o disattivare la visualizzazione attuale sulla mappa. I nomi sono direttamente derivati ​​dal servizio cui si accede dall&#39;esterno.",
      "transparency": "È inoltre possibile regolare la trasparenza di un livello.",
      "featureinfo": "Alcuni dati possono essere esaminati anche in maggior dettaglio. Quando viene attivata questa funzione è possibile fare clic sulla mappa per interrogare ulteriori informazioni.",
      "donotshow": "Non visualizzare più questa schermata di benvenuto.",
      "okButton": "Ok",
      "closeButton": "Chiudere"
    }
  }
});