define({
  "ec_user_tutorial": {
    "bundleName": "ec-uuporabnik-tutorski",
    "bundleDescription": "ec-uuporabnik-tutorski",
    "externalResourceDisclaimer": {
      "introduction": "Ta zemljevid gledalec dostopov zunanjih storitev ter prikaže svoje podatke. Te zunanje storitve, ki jih Evropska komisija ne vzdržuje in zato ne moremo vplivati ​​na njihovo razpoložljivost / stabilnost. Se lahko pojavijo naslednja vprašanja:",
      "rotatingCircle": "Vrteči krog v pogledu na zemljevid kaže, da gledalec čaka na odgovor od vira podatkov. To jih strežnik ni na voljo ali omrežnih vprašanjih lahko povzročil",
      "layerZoomLevel": "Nekateri viri sloj ne sme prikazati na nekaterih ravneh povečave. To je posledica omejitev, ki jih ima zunanji ponudnik storitev. Morda boste morali povečati ali pomanjšati sliko, da se prikažejo podatke plasti",
      "layerPanning": "Podatki, do katerih dostopate iz zunanjega strežnika se lahko nahajajo izven trenutnem pogledu zemljevida. Ta zemljevid gledalec poskuša samodejno prepozna pokrito območje. Kljub temu pa je v nekaterih primerih so metapodatki ni popolna ali napačna, tako da boste morali premikate z dejanskim obsegu.",
      "detailInformation": "Če zahtevate podrobne informacije za izbrano funkcijo bo to ravnati s posebno zahtevo na zemljevidu strežnik. Če je odgovor na to zahtevo je napačno ali prazna (na primer zaradi napake na strežniku) nobene informacije se lahko prikažejo in prazno okno bodo prikazani.",
      "technicalSupport": "Če naletite na kakršne koli druge težave, ne oklevajte, da stopijo v stik s tehnično podporo:"
    },
    "tutorial": {
      "welcome": "Dobrodošli na zemljevidu gledalca European Data Portal",
      "familiarise": "Ta majhen uvod ima cilj, da vas seznanijo z elementi in funkcionalnosti zemljevidu gledalca.",
      "navigation": "Kliknite in držite levi gumb miške za premikanje po zemljevidu.",
      "zoom": "Ti gumbi spremenite stopnjo povečave na zemljevidu. Alternativno lahko uporabite miško kolo za nastavitev zooma.",
      "features": "Dodatna funkcija je na voljo prek teh gumbov. Delujejo kot preklaplja in omogočite ali onemogočite določeno funkcionalnost.",
      "legend": "Legenda se lahko uporablja, da preuči razpoložljive zemljevide plasti in omogočiti ali onemogočiti njihovega trenutnega prikaza na zemljevidu. Imena so neposredno izhaja iz zunanje dostopne storitve.",
      "transparency": "Lahko prilagodite tudi preglednost plasti.",
      "featureinfo": "Nekateri podatki se lahko podrobno proučiti tudi. Ko je ta funkcija aktivirana lahko kliknete na zemljevidu, da se pozanima dodatne informacije.",
      "donotshow": "Spet ne prikaže ta pozdravni zaslon.",
      "okButton": "V redu",
      "closeButton": "Zapri"
    }
  }
});