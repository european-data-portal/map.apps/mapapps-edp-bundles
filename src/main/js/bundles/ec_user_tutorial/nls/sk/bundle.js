define({
  "ec_user_tutorial": {
    "bundleName": "ec-uužívateľ-konzultácia",
    "bundleDescription": "ec-užívateľ-konzultácia",
    "externalResourceDisclaimer": {
      "introduction": "Táto mapa prehliadač pristupuje externé služby a zobrazí ich dáta. Tieto externé služby nie sú udržiavané Európskou komisiou, a preto nemajú žiadny vplyv na ich dostupnosti / stabilita. Môže nastať nasledujúce problémy:",
      "rotatingCircle": "Rotujúce kruh v zobrazení mapy ukazuje, že je divák čaká na odpoveď zo zdroja údajov. To môže byť spôsobené tým, nedostupnom serveri siete či",
      "layerZoomLevel": "Niektoré zdroje vrstiev sa nemusia zobrazovať na určitých úrovniach priblíženia. To má za následok od obmedzení stanovených externého poskytovateľa služieb. Možno budete musieť priblížiť alebo vzdialiť aby bolo možné zobraziť dáta vrstvy",
      "layerPanning": "Sprístupnené údaje z externého servera môžu byť umiestnené mimo aktuálne zobrazenie mapy. Táto mapa prehliadač pokúsi automaticky určiť pokrytú oblasť. Napriek tomu sa v niektorých prípadoch metadáta nie sú úplné alebo chybné, takže budete musieť posunúť k skutočnej miere.",
      "detailInformation": "Ak máte požiadať o podrobnejšie informácie o vybraného prvku toto bude spracovaná špeciálne žiadosti do mapového servera. V prípade, že odpoveď na túto žiadosť, je poškodený alebo prázdny (napríklad v dôsledku chyby servera) žiadne informácie môžu byť zobrazené a prázdne okno sa zobrazí.",
      "technicalSupport": "Ak narazíte na akékoľvek iné problémy, neváhajte sa dostať do kontaktu s technickou podporou:"
    },
    "tutorial": {
      "welcome": "Vitajte na mape divákovi European Data Portal",
      "familiarise": "Tento malý úvod má za cieľ zoznámiť vás s prvkami a funkcie z perspektívy diváka.",
      "navigation": "Kliknite a držte ľavé tlačidlo myši posúvať na mape.",
      "zoom": "Tieto tlačidlá zmeniť úroveň zväčšenia na mape. Prípadne môžete použiť koliesko myši pre nastavenie zoomu.",
      "features": "Ďalšie funkcie je k dispozícii prostredníctvom týchto tlačidiel. Pôsobí ako prepínače a povoliť alebo zakázať danej funkcie.",
      "legend": "Legenda možno skúmať dostupné mapové vrstvy a povoliť alebo zakázať ich aktuálne zobrazenie na mape. Mená sú priamo odvodené z vonka prístupné služby.",
      "transparency": "Môžete tiež nastaviť priehľadnosť vrstvy.",
      "featureinfo": "Niektoré údaje môžu byť skúmané aj podrobnejšie. Ak je aktivovaná táto funkcia, môžete kliknúť na mape na dotaz ďalšie informácie.",
      "donotshow": "Nezobrazovať uvítaciu obrazovku znova.",
      "okButton": "Ok",
      "closeButton": "Zavrieť"
    }
  }
});