define([
  "dojo/_base/declare",
  "dojo/query",
  "dojo/dom-construct",
  "./Utils",
  "./DisclaimerWidget"
], function(declare, query, domConstruct, Utils, DisclaimerWidget) {
  return declare([], {
    overlayUtils: null,
    _removedAfterStartup: false,
    activate: function() {
      Utils.hideLegalDisclaimer();
      if (Utils.isTutorialDisabled()) {
        Utils.hideInfoDisclaimer();
        //we disable this, at its very buggy
        //this.overlayUtils.removeGlobalOverlay();

        this._removedAfterStartup = true;
      }
      var discI18n = this._i18n.get().externalResourceDisclaimer;
      discI18n.welcome = this._i18n.get().tutorial.welcome;
      var ui = new DisclaimerWidget({
        i18n: discI18n,
        overlayUtils: this.overlayUtils
      });
      var disclaimer = query(".disclaimer.info");
      if (disclaimer && disclaimer.length === 1) {
        domConstruct.place(ui.domNode, disclaimer[0]);
      }
    },

    removeAfterStartup: function() {
      if (this._removedAfterStartup) {
        //we disable this, at its very buggy
        //this.overlayUtils.removeGlobalOverlay();
      }
    },

    onTutorialAvailable: function() {
      Utils.displayDisclaimerButtons();
    },

    handleEvent: function(event) {
      if (
        event.getTopic() === "ec/odp/ON_EXTENT_ERROR" ||
        event.getTopic() === "ec/odp/extent/APPLIED"
      ) {
        Utils.displayDisclaimerButtons();
        //if (!Utils.isTutorialDisabled()) {
        //this.overlayUtils.addGlobalOverlay();
        //}
      }
    }
  });
});
