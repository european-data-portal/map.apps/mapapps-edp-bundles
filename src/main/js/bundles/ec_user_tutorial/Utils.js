define([
  "dojo/cookie",
  "dojo/dom-construct",
  "dojo/query",
  "dojo/dom-class",
  "dojo/_base/window"
], function(cookie, domConstruct, query, domClass, win) {
  var cookieKey = "ShowTutorial";
  var autoTutorialDisabled = false;

  var disableTutorial = function() {
    cookie(cookieKey, false);
  };

  var enableTutorial = function() {
    cookie(cookieKey, true);
  };

  var isTutorialDisabled = function() {
    if (autoTutorialDisabled) {
      return true;
    }

    var value = cookie(cookieKey);
    return "false" === value;
  };

  var hideInfoDisclaimer = function() {
    hideDisclaimer("hideInfoDisclaimer");
  };

  var showInfoDisclaimer = function(hideTutorialOnClose) {
    autoTutorialDisabled = hideTutorialOnClose;
    showDisclaimer("hideInfoDisclaimer");

    if (hideTutorialOnClose) {
      disableTutorial();
    }
  };

  var displayDisclaimerButtons = function() {
    query(".disclaimer-footer").style("display", "block");
    query(".disclaimer-footer-loading").style("display", "none");
  };

  var hideLegalDisclaimer = function() {
    hideDisclaimer("hideLegalDisclaimer");
  };

  var showLegalDisclaimer = function() {
    showDisclaimer("hideLegalDisclaimer");
  };

  var hideDisclaimer = function(className) {
    domClass.add(win.body(), className);
  };

  var showDisclaimer = function(className) {
    domClass.remove(win.body(), className);
  };

  return {
    hideInfoDisclaimer: hideInfoDisclaimer,
    showInfoDisclaimer: showInfoDisclaimer,
    hideLegalDisclaimer: hideLegalDisclaimer,
    showLegalDisclaimer: showLegalDisclaimer,
    disableTutorial: disableTutorial,
    enableTutorial: enableTutorial,
    isTutorialDisabled: isTutorialDisabled,
    displayDisclaimerButtons: displayDisclaimerButtons
  };
});
