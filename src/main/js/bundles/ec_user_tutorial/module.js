define([
  "./UserTutorial",
  "./Disclaimer",
  "./DisclaimerWidget",
  "./LegalDisclaimer",
  "./LegalDisclaimerWidget",
  "./Utils"
], {});
