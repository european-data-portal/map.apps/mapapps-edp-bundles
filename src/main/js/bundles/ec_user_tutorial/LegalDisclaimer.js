define([
  "dojo/_base/declare",
  "dojo/query",
  "dojo/dom-construct",
  "dojo/_base/lang",
  "./Utils",
  "./LegalDisclaimerWidget"
], function(
  declare,
  query,
  domConstruct,
  d_lang,
  Utils,
  LegalDisclaimerWidget
) {
  return declare([], {
    overlayUtils: null,
    activate: function() {
      Utils.hideLegalDisclaimer();
      this.addLegalDisclaimerWidget();
      this.enableLegalDisclaimerButton();
    },
    addLegalDisclaimerWidget: function() {
      var disclaimer = query(".disclaimer.legal");
      var ui = new LegalDisclaimerWidget({
        i18n: this._i18n.get(),
        overlayUtils: this.overlayUtils
      });
      if (disclaimer && disclaimer.length === 1) {
        domConstruct.place(ui.domNode, disclaimer[0]);
      }
    },
    enableLegalDisclaimerButton: function() {
      var tutorialButton = query(".legalDisclaimerButton");
      if (tutorialButton.length > 0) {
        tutorialButton.connect(
          "onclick",
          d_lang.hitch(this, function() {
            //we disable this, at its very buggy
            //this.overlayUtils.addGlobalOverlay();
            Utils.showLegalDisclaimer();
          })
        );
      }
    }
  });
});
