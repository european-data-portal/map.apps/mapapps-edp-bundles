define([
  "dojo/_base/lang",
  "dojo/_base/declare",
  "dojo/store/Memory",
  "dojo/_base/array",
  "dijit/tree/ObjectStoreModel",
  "dijit/Tree"
], function(lang, declare, Memory, array, ObjectStoreModel, Tree) {
  return declare([], {
    _mapModel: null,
    desktopWindowSize: {
      w: 250,
      h: 200
    },
    mobileWindowSize: {
      w: "100%",
      h: "40%",
      b: 30
    },
    constructor: function(opts) {
      var desktopWindowSize =
        (opts && opts.desktopWindowSize) || this.desktopWindowSize;
      var mobileWindowSize =
        (opts && opts.mobileWindowSize) || this.mobileWindowSize;
      this.contentRules = [
        {
          matches: lang.hitch(this, "matchesDesktop"),
          window: {
            marginBox: desktopWindowSize
          }
        },
        {
          matches: lang.hitch(this, "matchesMobile"),
          window: {
            marginBox: mobileWindowSize
          },
          noInfoWindow: true
        }
      ];
    },
    matchesDesktop: function(content, context) {
      return this._matchesContext(context) && !this._isMobile(content);
    },
    matchesMobile: function(content, context) {
      return this._matchesContext(context) && this._isMobile(content);
    },
    _isMobile: function(content) {
      return content && content.mobile;
    },
    _matchesContext: function(context) {
      return context && context.graphic && context.graphic.attributes;
    },
    /**
     * This is called by the ContentViewer for creating the "popup content" view.
     */
    createWidget: function(params) {
      return this.createTree(params.context.graphic.attributes);
    },
    _addData: function(obj, parent, nodes) {
      for (var key in obj) {
        if (!obj.hasOwnProperty(key) || this._isBlacklisted(key)) {
          continue;
        }
        var value = obj[key];
        if (value !== null && lang.isObject(value)) {
          nodes.push({
            id: key,
            name: key,
            parent: parent
          });
          this._addData(value, key, nodes);
        } else {
          nodes.push({
            id: key,
            name: key + ": " + value,
            parent: parent,
            isLeaf: true
          });
        }
      }
    },
    _isBlacklisted: function(key) {
      var blackList = this._properties.propertiesBlackList;
      return blackList.length && array.indexOf(blackList, key) > -1;
    },
    _createData: function(attributes) {
      var nodes = [{ id: "root" }];
      this._addData(attributes, nodes[0].id, nodes);
      return nodes;
    },
    createTree: function(attributes) {
      var tree = new Tree({
        model: new ObjectStoreModel({
          query: { id: "root" },
          store: new Memory({
            data: this._createData(attributes),
            getChildren: function(object) {
              return this.query({ parent: object.id });
            }
          }),
          mayHaveChildren: function(item) {
            return !item.isLeaf;
          }
        }),
        showRoot: false,
        openOnClick: true,
        getIconClass: function() {
          /* don't use icons */
        }
      });
      return tree;
    }
  });
});
