define([
  "dojo/_base/declare",
  "dojo/_base/connect",
  "ct/ui/desktop/util",
  "./WmsLayerSelectorWidget"
], function(declare, d_connect, util, WmsLayerSelectorWidget) {
  return declare([], {
    createInstance: function() {
      var list = this._dynamicWmsLayer.getLayersList();

      if (!list || list.length === 0) {
        console.warn("no layers available. Skipping layer selector");
        throw new Error("no layers available. Skipping layer selector");
      }

      var opts = {
        preselectedLayers: this._properties.preselectedLayers || 1,
        i18n: this._i18n.get().ui,
        layers: list
      };
      this._widget = new WmsLayerSelectorWidget(opts);
      d_connect.connect(
        this._widget,
        "onSelectedLayers",
        this,
        function(layers) {
          this._dynamicWmsLayer.addLayersToMap(layers);
          var w = util.findEnclosingWindow(this._widget);
          if (w && w.hide) {
            w.hide();
          }
        }
      );
      return this._widget;
    }
  });
});
