define([
  "dojo/_base/declare",
  "dijit/_Widget",
  "dijit/_TemplatedMixin",
  "dijit/_WidgetsInTemplateMixin",
  "dijit/form/CheckBox",
  "dojo/string",
  "dojo/_base/lang",
  "dojo/_base/array",
  "dojo/dom-construct",
  "dojo/text!./templates/SelectorBody.html",
  "dijit/layout/ContentPane",
  "dijit/layout/BorderContainer"
], function(
  declare,
  _Widget,
  _TemplatedMixin,
  _WidgetsInTemplateMixin,
  CheckBox,
  d_string,
  d_lang,
  d_array,
  domConstruct,
  templateStringContent,
  ContentPane,
  BorderContainer
) {
  return declare([_Widget, _TemplatedMixin, _WidgetsInTemplateMixin], {
    baseClass: "ecWmsLayerSelector",
    templateString: templateStringContent,
    constructor: function(args) {
      this._layers = args.layers;
      this.preselectedLayers = args.preselectedLayers;
      this.i18n = args.i18n;
      this.i18n.hint = d_string.substitute(this.i18n.hint, {
        countToProvideSelection: this._layers.length
      });
    },
    buildRendering: function() {
      this.inherited(arguments);
      this._createSelectAllCheckBox();
      this._createLayersCheckBoxes();
    },
    _createSelectAllCheckBox: function() {
      new CheckBox(
        {
          label: "label",
          value: "value",
          checked: false,
          onChange: d_lang.hitch(this, function(checked) {
            this._setAllCheckboxesTo(checked);
          })
        },
        this._checkAllNode
      );
    },
    _setAllCheckboxesTo: function(bool) {
      d_array.forEach(this._layerCheckboxes, function(checkbox) {
        checkbox.setChecked(bool);
      });
    },
    _createLayersCheckBoxes: function() {
      this._layerCheckboxes = d_array.map(
        this._layers,
        function(layer, idx) {
          var wrapper = domConstruct.create("div");
          var checkBox = new CheckBox({
            value: layer,
            checked: this.preselectedLayers-- > 0 ? true : false
          });
          var label = domConstruct.create("label", {
            for: checkBox.id,
            innerHTML: layer.title
          });
          wrapper.appendChild(checkBox.domNode);
          wrapper.appendChild(label);
          domConstruct.place(wrapper, this._layerListNode, "before");
          return checkBox;
        },
        this
      );
    },
    _createSelectedLayers: function() {
      var filtered = d_array.filter(this._layerCheckboxes, function(checkBox) {
        return checkBox.checked;
      });
      var mapped = d_array.map(filtered, function(checkBox) {
        return checkBox.value;
      });
      return mapped;
    },
    show: function() {},
    hide: function() {},
    _selectLayersClicked: function() {
      this.onSelectedLayers(this._createSelectedLayers());
    },
    onSelectedLayers: function() {}
  });
});
