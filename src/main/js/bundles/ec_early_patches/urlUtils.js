define(["esri/urlUtils"], function(urlUtils) {
  urlUtils.downgradeToHTTP = function(url) {
    return url;
  };

  urlUtils.upgradeToHTTPS = function(url) {
    return url;
  };

  return urlUtils;
});
