define({
	europeanCommission: "Europska komisija",
	lang: "hr",
	headerTitle: "Europski Portal Podataka",
	betaPageLink: "hr/what-we-do",
	legalNotice: "Pravna obavijest",
	legalNoticeLink: "hr/legal-notice",
	cookies: "Kola\u010di\u0107i",
	cookiesLink: "hr/cookies",
	contact: "Kontakt",
	search: "Pretra\u017eivanje",
	legalDisclaimer: "Pravna obavijest",
	routing: {
		title: "Usmjeravanje"
	},
	streetview: {
		title: "Street View"
	},
	mapdesk: {
		title: "Karta Desk"
	},
	overviewmap: {
		title: "Pregled Map"
	},
	mapflow: {
		title: "Karta protoka"
	},
	followme: {
		title: "Slijedite me"
	},
	magnifier: {
		title: "Pove\u0107alo"
	},
	gallery: {
		title: "Galerija"
	},
	resultcenter: {
		title: "Centar Rezultat"
	},
	measurement: {
		title: "Mjerni alati"
	},
	bookmarks: {
		title: "Prostorni oznake"
	},
	parameterURL: {
		title: "Link alat"
	},
	tableofcontents: {
		title: "TOC"
	},
	legend: {
		title: "Legenda"
	},
	layerSelector: {
		title: "Odaberi slojeve"
	},
	coordfinder: {
		title: "Koordinatni Finder"
	},
	agsPringing: {
		title: "AGS ispis"
	},
	addThis: {
		title: "Social Bookmarking Tool"
	},
	agolmapFinder: {
		title: "ArcGIS Online Web Map Finder"
	},
	drawSymbolChooser: {
		title: "Odaberite simbol na mjesto na karti ..."
	},
	redliningStyleProperties: {
		title: "Nacrtaj Svojstva"
	}
});