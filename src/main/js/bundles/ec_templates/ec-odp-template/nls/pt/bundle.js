define({
	europeanCommission: "Comiss\u00e3o Europeia",
	lang: "pt",
	headerTitle: "Portal Europeu de Dados",
	betaPageLink: "pt/what-we-do",
	legalNotice: "Advert\u00eancia jur\u00eddica",
	legalNoticeLink: "pt/legal-notice",
	cookies: "Cookies",
	cookiesLink: "pt/cookies",
	contact: "Contacto",
	search: "Pesquisa",
	legalDisclaimer: "Cl\u00E1usula de isen\u00E7\u00E3o de responsabilidade",
	routing: {
		title: "Routing"
	},
	streetview: {
		title: "Street View"
	},
	mapdesk: {
		title: "Mapa Desk"
	},
	overviewmap: {
		title: "Vis\u00e3o geral do mapa"
	},
	mapflow: {
		title: "Mapa de Fluxo"
	},
	followme: {
		title: "Me Siga"
	},
	magnifier: {
		title: "Lupa"
	},
	gallery: {
		title: "Galeria"
	},
	resultcenter: {
		title: "Centro de Resultado"
	},
	measurement: {
		title: "Ferramentas de Medi\u00e7\u00e3o"
	},
	bookmarks: {
		title: "Bookmarks espaciais"
	},
	parameterURL: {
		title: "Liga\u00e7\u00e3o de ferramenta"
	},
	tableofcontents: {
		title: "TOC"
	},
	legend: {
		title: "Legenda"
	},
	layerSelector: {
		title: "Selecionar camadas"
	},
	coordfinder: {
		title: "Coordenar do Finder"
	},
	agsPringing: {
		title: "Impress\u00e3o AGS"
	},
	addThis: {
		title: "Ferramenta de bookmarking social"
	},
	agolmapFinder: {
		title: "ArcGIS Online Web Mapa do Finder"
	},
	drawSymbolChooser: {
		title: "Escolha um s\u00edmbolo para colocar no mapa ..."
	},
	redliningStyleProperties: {
		title: "Desenhe Propriedades"
	}
});