define({
	europeanCommission: "Comisia European\u0103",
	lang: "ro",
	headerTitle: "Portalul European De Date",
	betaPageLink: "ro/what-we-do",
	legalNotice: "Aviz juridic",
	legalNoticeLink: "ro/legal-notice",
	cookies: "Cookie-urile",
	cookiesLink: "ro/cookies",
	contact: "Contact",
	search: "C\u0103utare",
	legalDisclaimer: "Declinarea r\u0103spunderii juridice",
	routing: {
		title: "Rutare"
	},
	streetview: {
		title: "Street View"
	},
	mapdesk: {
		title: "Harta birou"
	},
	overviewmap: {
		title: "Hart\u0103 general\u0103"
	},
	mapflow: {
		title: "Harta Flow"
	},
	followme: {
		title: "Urma\u021bi-M\u0103"
	},
	magnifier: {
		title: "Lup\u0103"
	},
	gallery: {
		title: "Galerie"
	},
	resultcenter: {
		title: "Rezultatul Center"
	},
	measurement: {
		title: "Instrumente de m\u0103surare"
	},
	bookmarks: {
		title: "Marcaje spa\u021biale"
	},
	parameterURL: {
		title: "Link Instrumentul"
	},
	tableofcontents: {
		title: "TOC"
	},
	legend: {
		title: "Legend\u0103"
	},
	layerSelector: {
		title: "Selectare straturi"
	},
	coordfinder: {
		title: "Coordonarea Finder"
	},
	agsPringing: {
		title: "AGS imprimare"
	},
	addThis: {
		title: "Instrumentul Social marcare"
	},
	agolmapFinder: {
		title: "ArcGIS Online Web Map Finder"
	},
	drawSymbolChooser: {
		title: "Alege un simbol pentru a plasa pe hart\u0103 ..."
	},
	redliningStyleProperties: {
		title: "Egal Propriet\u0103\u021bi"
	}
});