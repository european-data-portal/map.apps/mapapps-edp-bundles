define({
	europeanCommission: "Il-Kummissjoni Ewropea",
	lang: "mt",
	headerTitle: "Portal tad-Data Ewropew",
	betaPageLink: "mt/what-we-do",
	legalNotice: "Avvi\u017c legali",
	legalNoticeLink: "mt/legal-notice",
	cookies: "Cookies",
	cookiesLink: "mt/cookies",
	contact: "Kuntatt",
	search: "Fittex",
	legalDisclaimer: "Rinunzja Legali",
	routing: {
		title: "Rotot"
	},
	streetview: {
		title: "Street View"
	},
	mapdesk: {
		title: "Desk Map"
	},
	overviewmap: {
		title: "\u0126arsa \u0121enerali Map"
	},
	mapflow: {
		title: "Mappa Flow"
	},
	followme: {
		title: "Follow Me"
	},
	magnifier: {
		title: "Lenti"
	},
	gallery: {
		title: "Gallerija"
	},
	resultcenter: {
		title: "Ri\u017cultat Center"
	},
	measurement: {
		title: "Kejl G\u0127odod"
	},
	bookmarks: {
		title: "Bookmarks spazjali"
	},
	parameterURL: {
		title: "Link Tool"
	},
	tableofcontents: {
		title: "TOC"
	},
	legend: {
		title: "Le\u0121\u0121enda"
	},
	layerSelector: {
		title: "Ag\u0127\u017Cel is-saffi"
	},
	coordfinder: {
		title: "Tikkoordina Finder"
	},
	agsPringing: {
		title: "AGS Stampar"
	},
	addThis: {
		title: "Social Bookmarking Tool"
	},
	agolmapFinder: {
		title: "ArcGIS Online Web Mappa Finder"
	},
	drawSymbolChooser: {
		title: "Ag\u0127\u017cel simbolu g\u0127all-post fuq il-mappa ..."
	},
	redliningStyleProperties: {
		title: "Properties I\u0121bed"
	}
});