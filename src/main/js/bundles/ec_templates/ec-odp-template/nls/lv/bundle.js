define({
	europeanCommission: "Eiropas Komisija",
	lang: "lv",
	headerTitle: "EIROPAS DATU PORT\u0100LS",
	betaPageLink: "lv/what-we-do",
	legalNotice: "Juridisks pazi\u0146ojums",
	legalNoticeLink: "lv/legal-notice",
	cookies: "S\u012Bkdatnes",
	cookiesLink: "lv/cookies",
	contact: "Kontakti",
	search: "Mekl\u0113t",
	legalDisclaimer: "Juridiska atruna",
	routing: {
		title: "Mar\u0161rut\u0113\u0161anas"
	},
	streetview: {
		title: "Street View"
	},
	mapdesk: {
		title: "Karte Desk"
	},
	overviewmap: {
		title: "P\u0101rskats Karte"
	},
	mapflow: {
		title: "Karte Flow"
	},
	followme: {
		title: "Follow Me"
	},
	magnifier: {
		title: "Lupa"
	},
	gallery: {
		title: "Galerija"
	},
	resultcenter: {
		title: "Rezult\u0101ts Center"
	},
	measurement: {
		title: "M\u0113r\u012Bjumu Tools"
	},
	bookmarks: {
		title: "Telpiskie Gr\u0101matz\u012Bmes"
	},
	parameterURL: {
		title: "Link Tool"
	},
	tableofcontents: {
		title: "TOC"
	},
	legend: {
		title: "Le\u0123enda"
	},
	layerSelector: {
		title: "Atlas\u012Bt sl\u0101\u0146us"
	},
	coordfinder: {
		title: "Koordin\u0101tu Finder"
	},
	agsPringing: {
		title: "AGS Printing"
	},
	addThis: {
		title: "Soci\u0101l\u0101s Gr\u0101matz\u012bmes Tool"
	},
	agolmapFinder: {
		title: "ArcGIS Online Web Map mekl\u0113t\u0101js"
	},
	drawSymbolChooser: {
		title: "Izv\u0113lieties simbolu laist karti ..."
	},
	redliningStyleProperties: {
		title: "Z\u012bm\u0113t Rekviz\u012bti"
	}
});