define({
	europeanCommission: "\u0415\u0432\u0440\u043e\u043f\u0435\u0439\u0441\u043a\u0430 \u043a\u043e\u043c\u0438\u0441\u0438\u044f",
	lang: "bg",
	headerTitle: "\u0415\u0432\u0440\u043E\u043F\u0435\u0439\u0441\u043A\u0438 \u043F\u043E\u0440\u0442\u0430\u043B \u0437\u0430 \u0434\u0430\u043D\u043D\u0438",
	betaPageLink: "bg/what-we-do",
	legalNotice: "\u041f\u0440\u0430\u0432\u043d\u0430 \u0438\u043d\u0444\u043e\u0440\u043c\u0430\u0446\u0438\u044f",
	legalNoticeLink: "bg/legal-notice",
	cookies: "\u0411\u0438\u0441\u043a\u0432\u0438\u0442\u043a\u0438",
	cookiesLink: "bg/cookies",
	contact: "\u0417\u0430 \u043a\u043e\u043d\u0442\u0430\u043a\u0442\u0438",
	search: "\u0422\u044a\u0440\u0441\u0435\u043d\u0435",
	legalDisclaimer: "\u041E\u0442\u043A\u0430\u0437 \u043E\u0442 \u043E\u0442\u0433\u043E\u0432\u043E\u0440\u043D\u043E\u0441\u0442",
	routing: {
		title: "Routing"
	},
	streetview: {
		title: "Street View"
	},
	mapdesk: {
		title: "\u041a\u0430\u0440\u0442\u0430 Desk"
	},
	overviewmap: {
		title: "\u041f\u0440\u0435\u0433\u043b\u0435\u0434 \u041a\u0430\u0440\u0442\u0430"
	},
	mapflow: {
		title: "\u041a\u0430\u0440\u0442\u0430 Flow"
	},
	followme: {
		title: "Follow Me"
	},
	magnifier: {
		title: "\u041b\u0443\u043f\u0430"
	},
	gallery: {
		title: "\u0413\u0430\u043b\u0435\u0440\u0438\u044f"
	},
	resultcenter: {
		title: "\u0420\u0435\u0437\u0443\u043b\u0442\u0430\u0442\u0438 Center"
	},
	measurement: {
		title: "\u0418\u043d\u0441\u0442\u0440\u0443\u043c\u0435\u043d\u0442\u0438 \u0437\u0430 \u0438\u0437\u043c\u0435\u0440\u0432\u0430\u043d\u0435"
	},
	bookmarks: {
		title: "\u041f\u0440\u043e\u0441\u0442\u0440\u0430\u043d\u0441\u0442\u0432\u0435\u043d\u0438 Bookmarks"
	},
	parameterURL: {
		title: "Link Tool"
	},
	tableofcontents: {
		title: "TOC"
	},
	legend: {
		title: "\u041b\u0435\u0433\u0435\u043d\u0434\u0430"
	},
	layerSelector: {
		title: "\u0418\u0437\u0431\u043E\u0440 \u043D\u0430 \u0441\u043B\u043E\u0439"
	},
	coordfinder: {
		title: "\u041a\u043e\u043e\u0440\u0434\u0438\u043d\u0438\u0440\u0430 Finder"
	},
	agsPringing: {
		title: "AGS \u043f\u0435\u0447\u0430\u0442"
	},
	addThis: {
		title: "\u0421\u043e\u0446\u0438\u0430\u043b\u043d\u0438 \u043e\u0442\u043c\u0435\u0442\u043a\u0438 Tool"
	},
	agolmapFinder: {
		title: "ArcGIS Online Web Map Finder"
	},
	drawSymbolChooser: {
		title: "\u0418\u0437\u0431\u0435\u0440\u0435\u0442\u0435 \u0441\u0438\u043c\u0432\u043e\u043b, \u0437\u0430 \u0434\u0430 \u043f\u043e\u0441\u0442\u0430\u0432\u0438\u0442\u0435 \u043d\u0430 \u043a\u0430\u0440\u0442\u0430\u0442\u0430 ..."
	},
	redliningStyleProperties: {
		title: "Draw Properties"
	}
});