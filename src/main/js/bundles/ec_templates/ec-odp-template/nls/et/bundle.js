define({
	europeanCommission: "Euroopa Komisjon",
	lang: "et",
	headerTitle: "EUROOPA ANDMEPORTAAL",
	betaPageLink: "et/what-we-do",
	legalNotice: "\u00D5igusteave",
	legalNoticeLink: "et/legal-notice",
	cookies: "K\u00FCpsised",
	cookiesLink: "et/cookies",
	contact: "Kontakt",
	search: "Otsi",
	legalDisclaimer: "Juriidilisest vastutusest loobumine",
	routing: {
		title: "Marsruut"
	},
	streetview: {
		title: "Street View"
	},
	mapdesk: {
		title: "Kaart Desk"
	},
	overviewmap: {
		title: "\u00DClevaade Kaart"
	},
	mapflow: {
		title: "Kaart Flow"
	},
	followme: {
		title: "J\u00E4rgi mind"
	},
	magnifier: {
		title: "Luup"
	},
	gallery: {
		title: "Galerii"
	},
	resultcenter: {
		title: "Tulemus Center"
	},
	measurement: {
		title: "M\u00F5\u00F5tmine T\u00F6\u00F6riistad"
	},
	bookmarks: {
		title: "Ruumiline Bookmarks"
	},
	parameterURL: {
		title: "Link Tool"
	},
	tableofcontents: {
		title: "TOC"
	},
	legend: {
		title: "Legend"
	},
	layerSelector: {
		title: "Vali kihid"
	},
	coordfinder: {
		title: "Koordinaatide Finder"
	},
	agsPringing: {
		title: "AGS tr\u00fckkimine"
	},
	addThis: {
		title: "Sotsiaalne j\u00e4rjehoidjad Tool"
	},
	agolmapFinder: {
		title: "ArcGIS Online Web Map Finder"
	},
	drawSymbolChooser: {
		title: "Vali s\u00fcmbol panna kaardile ..."
	},
	redliningStyleProperties: {
		title: "Joonista omadused"
	}
});