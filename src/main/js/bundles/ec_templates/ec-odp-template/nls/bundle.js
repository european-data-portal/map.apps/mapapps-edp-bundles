define({
    root:
            {
                europeanCommission: "European Commission",
                lang: "en",
                headerTitle:"EUROPEAN DATA PORTAL",
                betaPageLink: "en/content/what-we-do",
                legalNotice: "Legal notice",
                legalNoticeLink: "en/legal-notice",
                cookies: "Cookies",
                cookiesLink: "en/cookies",
                contact: "Contact",
                search: "Search",
                legalDisclaimer: "Legal Disclaimer",
                "routing": {
                    "title": "Routing"
                },
                "streetview": {
                    "title": "Street View"
                },
                "mapdesk": {
                    "title": "Map Desk"
                },
                "overviewmap": {
                    "title": "Overview Map"
                },
                "mapflow": {
                    "title": "Map Flow"
                },
                "followme": {
                    "title": "Follow Me"
                },
                "magnifier": {
                    "title": "Magnifier"
                },
                "gallery": {
                    "title": "Gallery"
                },
                "resultcenter": {
                    "title": "Result Center"
                },
                "measurement": {
                    "title": "Measurement Tools"
                },
                "bookmarks": {
                    "title": "Spatial Bookmarks"
                },
                "parameterURL": {
                    "title": "Link Tool"
                },
                "tableofcontents": {
                    "title": "TOC"
                },
                "legend": {
                    "title": "Legend"
                },
                "layerSelector": {
                    "title": "Layer"
                },
                "coordfinder": {
                    "title": "Coordinate Finder"
                },
                "agsPringing": {
                    "title": "AGS Printing"
                },
                "addThis": {
                    "title": "Social Bookmarking Tool"
                },
                "agolmapFinder": {
                    "title": "ArcGIS Online Web Map Finder"
                },
                "drawSymbolChooser": {
                    "title": "Choose a symbol to place on the map..."
                },
                "redliningStyleProperties": {
                    "title": "Draw Properties"
                }
            },
    bg: true,
    cs: true,
    da: true,
    de: true,
    et: true,
    el: true,
    es: true,
    fr: true,
    ga: true,
    hr: true,
    it: true,
    lv: true,
    lt: true,
    hu: true,
    mt: true,
    nl: true,
    pl: true,
    pt: true,
    ro: true,
    sk: true,
    sl: true,
    fi: true,
    sv: true
});