define({
	europeanCommission: "Eur\u00f3pska komisia",
	lang: "sk",
	headerTitle: "Port\u00e1l Eur\u00f3pskych \u00dadajov",
	betaPageLink: "sk/what-we-do",
	legalNotice: "Pr\u00e1vne upozornenie",
	legalNoticeLink: "sk/legal-notice",
	cookies: "S\u00fabory cookie",
	cookiesLink: "sk/cookies",
	contact: "Kontakt",
	search: "Vyh\u013ead\u00e1vanie",
	legalDisclaimer: "Pr\u00e1vna dolo\u017eka",
	routing: {
		title: "Smerovanie"
	},
	streetview: {
		title: "Street View"
	},
	mapdesk: {
		title: "Mapa Desk"
	},
	overviewmap: {
		title: "Mapka"
	},
	mapflow: {
		title: "Mapa Flow"
	},
	followme: {
		title: "Follow Me"
	},
	magnifier: {
		title: "Zv\u00e4\u010d\u0161ovacie sklo"
	},
	gallery: {
		title: "Gal\u00e9ria"
	},
	resultcenter: {
		title: "V\u00fdsledok Center"
	},
	measurement: {
		title: "Meranie d\u013a\u017eky"
	},
	bookmarks: {
		title: "Priestorov\u00e9 Z\u00e1lo\u017eky"
	},
	parameterURL: {
		title: "Link Tool"
	},
	tableofcontents: {
		title: "TOC"
	},
	legend: {
		title: "Legenda"
	},
	layerSelector: {
		title: "Vybra\u0165 vrstvy"
	},
	coordfinder: {
		title: "S\u00faradn\u00edc Finder"
	},
	agsPringing: {
		title: "AGS tla\u010de"
	},
	addThis: {
		title: "Social Bookmarking Tool"
	},
	agolmapFinder: {
		title: "ArcGIS Online Web Map Finder"
	},
	drawSymbolChooser: {
		title: "Vyberte symbol uvies\u0165 na mape ..."
	},
	redliningStyleProperties: {
		title: "Rem\u00edza Vlastnosti"
	}
});