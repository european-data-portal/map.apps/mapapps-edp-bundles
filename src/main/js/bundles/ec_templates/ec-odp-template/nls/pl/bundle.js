define({
	europeanCommission: "Komisja Europejska",
	lang: "pl",
	legalNotice: "Informacja prawna",
	cookies: "Pliki cookie",
	contact: "Kontakt",
	search: "Wyszukiwanie",
	headerTitle: "Europejski Portal Danych",
	betaPageLink: "pl/what-we-do",
	legalNoticeLink: "pl/legal-notice",
	cookiesLink: "pl/cookies",
	legalDisclaimer: "Prawne ograniczenie odpowiedzialno\u015bci",
	routing: {
		title: "Routing"
	},
	streetview: {
		title: "Street View"
	},
	mapdesk: {
		title: "Biuro Mapa"
	},
	overviewmap: {
		title: "Informacje og\u00f3lne Mapa"
	},
	mapflow: {
		title: "Mapa przep\u0142ywu"
	},
	followme: {
		title: "Follow Me"
	},
	magnifier: {
		title: "Lupa"
	},
	gallery: {
		title: "Galeria"
	},
	resultcenter: {
		title: "Centrum Wynik"
	},
	measurement: {
		title: "Narz\u0119dzia pomiarowe"
	},
	bookmarks: {
		title: "Zak\u0142adki przestrzenne"
	},
	parameterURL: {
		title: "Narz\u0119dzie link"
	},
	tableofcontents: {
		title: "TOC"
	},
	legend: {
		title: "Legenda"
	},
	layerSelector: {
		title: "Wybierz poziomy"
	},
	coordfinder: {
		title: "Finder wsp\u00f3\u0142rz\u0119dnych"
	},
	agsPringing: {
		title: "AGS Drukarnie"
	},
	addThis: {
		title: "Spo\u0142eczne zak\u0142adki narz\u0119dzia"
	},
	agolmapFinder: {
		title: "ArcGIS Online Web Map Finder"
	},
	drawSymbolChooser: {
		title: "Wybierz symbol, aby umie\u015bci\u0107 na mapie ..."
	},
	redliningStyleProperties: {
		title: "Remis W\u0142a\u015bciwo\u015bci"
	}
});