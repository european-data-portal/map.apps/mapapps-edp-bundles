define([
  "dojo/text!./ec-odp-desktop.html",
  "dojo/text!./ec-odp-mobile.html",
  "dojo/i18n!./nls/bundle",
  // ensure that widgets used in template are loaded
  "dijit/layout/BorderContainer",
  "dijit/layout/ContentPane"
], function(desktop, mobile, i18n) {
  // extend i18n with locale
  if (dojoConfig.locale) {
    i18n.locale = dojoConfig.locale;
  }
  return {
    layouts: [
      {
        requiredExecutionEnvironment: ["mobile"],
        orientation: "portrait",
        templateString: mobile,
        name: "mobile_portrait"
      },
      {
        requiredExecutionEnvironment: ["mobile"],
        orientation: "landscape",
        templateString: mobile,
        name: "mobile_portrait"
      },
      {
        templateString: desktop,
        name: "ec_desktop"
      }
    ],
    i18n: [i18n]
  };
});
