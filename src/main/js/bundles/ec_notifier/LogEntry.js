define([
    'dojo/_base/declare',
    'dojo/_base/lang',
    'system/log/LogEntry'
], function (declare, lang, LogEntry) {
    var levels = lang.mixin({}, LogEntry.levels, { LOG_FATAL: -1 });
    var Entry = declare([LogEntry], {
        levels: levels,
        isError: function () {
            return this.inherited(arguments) || this.get('level') === levels.LOG_FATAL;
        }
    });
    Entry.levels = levels;
    return Entry;
});