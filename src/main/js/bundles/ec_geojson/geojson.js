/*
 Source is based on:
 https://github.com/Esri/Terraformer -> arcgis.js


 Copyright (c) 2013 Esri, Inc
 Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
define([], function() {

    // determine if polygon ring coordinates are clockwise. clockwise signifies outer ring, counter-clockwise an inner ring
    // or hole. this logic was found at http://stackoverflow.com/questions/1165647/how-to-determine-if-a-list-of-polygon-
    // points-are-in-clockwise-order
    var ringIsClockwise = function(ringToTest) {
        var total = 0, i = 0;
        var rLength = ringToTest.length;
        var pt1 = ringToTest[i];
        var pt2;
        for (var i; i < rLength - 1; i++) {
            pt2 = ringToTest[i + 1];
            total += (pt2[0] - pt1[0]) * (pt2[1] + pt1[1]);
            pt1 = pt2;
        }
        return (total >= 0);
    };

    // This function flattens holes in polygons to one array of rings
    //
    // [
    //   [ array of outer coordinates ]
    //   [ hole coordinates ]
    //   [ hole coordinates ]
    // ]
    // becomes
    // [
    //   [ array of outer coordinates ]
    //   [ hole coordinates ]
    //   [ hole coordinates ]
    // ]
    var flattenPolygonRings = function(polygon) {
        var output = [];
        var outerRing = polygon.shift();

        if (!ringIsClockwise(outerRing)) {
            outerRing.reverse();
        }

        output.push(outerRing);

        for (var i = 0; i < polygon.length; i++) {
            var hole = polygon[i];
            if (ringIsClockwise(hole)) {
                outerRing.reverse();
            }
            output.push(polygon[i]);
        }

        return output;
    };

    // This function flattens holes in multipolygons to one array of polygons
    // so
    // [
    //   [
    //     [ array of outer coordinates ]
    //     [ hole coordinates ]
    //     [ hole coordinates ]
    //   ],
    //   [
    //     [ array of outer coordinates ]
    //     [ hole coordinates ]
    //     [ hole coordinates ]
    //   ],
    // ]
    // becomes
    // [
    //   [ array of outer coordinates ]
    //   [ hole coordinates ]
    //   [ hole coordinates ]
    //   [ array of outer coordinates ]
    //   [ hole coordinates ]
    //   [ hole coordinates ]
    // ]
    var flattenMultiPolygonRings = function(rings) {
        var output = [];
        for (var i = 0; i < rings.length; i++) {
            var polygon = flattenPolygonRings(rings[i]);
            for (var x = polygon.length - 1; x >= 0; x--) {
                var ring = polygon[x];
                output.push(ring);
            }
            output.push();
        }
        return output;
    };

    var vertexIntersectsVertex = function(a1, a2, b1, b2) {
        var ua_t = (b2[0] - b1[0]) * (a1[1] - b1[1]) - (b2[1] - b1[1]) * (a1[0] - b1[0]);
        var ub_t = (a2[0] - a1[0]) * (a1[1] - b1[1]) - (a2[1] - a1[1]) * (a1[0] - b1[0]);
        var u_b = (b2[1] - b1[1]) * (a2[0] - a1[0]) - (b2[0] - b1[0]) * (a2[1] - a1[1]);

        if (u_b !== 0) {
            var ua = ua_t / u_b;
            var ub = ub_t / u_b;

            if (0 <= ua && ua <= 1 && 0 <= ub && ub <= 1) {
                return true;
            }
        }

        return false;
    };


    var arrayIntersectsArray = function(a, b) {
        for (var i = 0; i < a.length - 1; i++) {
            for (var j = 0; j < b.length - 1; j++) {
                if (vertexIntersectsVertex(a[i], a[i + 1], b[j], b[j + 1])) {
                    return true;
                }
            }
        }
        return false;
    };

    var coordinatesContainPoint = function(coordinates, point) {
        var contains = false;
        for (var i = -1, l = coordinates.length, j = l - 1; ++i < l; j = i) {
            if (((coordinates[i][1] <= point[1] && point[1] < coordinates[j][1]) ||
                    (coordinates[j][1] <= point[1] && point[1] < coordinates[i][1])) &&
                    (point[0] < (coordinates[j][0] - coordinates[i][0]) * (point[1] - coordinates[i][1]) / (coordinates[j][1] - coordinates[i][1]) + coordinates[i][0])) {
                contains = true;
            }
        }
        return contains;
    };


    var coordinatesContainCoordinates = function(outer, inner) {
        var intersects = arrayIntersectsArray(outer, inner);
        var contains = coordinatesContainPoint(outer, inner[0]);
        if (!intersects && contains) {
            return true;
        }
        return false;
    };

    // do any polygons in this array contain any other polygons in this array?
    // used for checking for holes in arcgis rings
    var convertRingsToGeoJSON = function(rings) {
        var outerRings = [];
        var holes = [];

        // for each ring
        for (var r = 0; r < rings.length; r++) {
            var ring = rings[r];

            // is this ring an outer ring? is it clockwise?
            if (ringIsClockwise(ring)) {
                var polygon = [ring];
                outerRings.push(polygon); // push to outer rings
            } else {
                holes.push(ring); // counterclockwise push to holes
            }
        }

        // while there are holes left...
        while (holes.length) {
            // pop a hole off out stack
            var hole = holes.pop();
            var matched = false;

            // loop over all outer rings and see if they contain our hole.
            for (var x = outerRings.length - 1; x >= 0; x--) {
                var outerRing = outerRings[x][0];
                if (coordinatesContainCoordinates(outerRing, hole)) {
                    // the hole is contained push it into our polygon
                    outerRings[x].push(hole);

                    // we matched the hole
                    matched = true;

                    // stop checking to see if other outer rings contian this hole
                    break;
                }
            }

            // no outer rings contain this hole turn it into and outer ring (reverse it)
            if (!matched) {
                outerRings.push([hole.reverse()]);
            }
        }

        if (outerRings.length === 1) {
            return {
                type: "Polygon",
                coordinates: outerRings[0]
            };
        } else {
            return {
                type: "MultiPolygon",
                coordinates: outerRings
            };
        }
    };

    // ArcGIS -> GeoJSON
    var parse = function(input) {
        var arcgis = input; //clone?-->JSON.parse(JSON.stringify(input));
        var geojson = {};

        if (arcgis.x && arcgis.y) {
            geojson.type = "Point";
            geojson.coordinates = [arcgis.x, arcgis.y];
        }

        if (arcgis.points) {
            geojson.type = "MultiPoint";
            geojson.coordinates = arcgis.points;
        }

        if (arcgis.paths) {
            if (arcgis.paths.length === 1) {
                geojson.type = "LineString";
                geojson.coordinates = arcgis.paths[0];
            } else {
                geojson.type = "MultiLineString";
                geojson.coordinates = arcgis.paths;
            }
        }

        if (arcgis.rings) {
            geojson = convertRingsToGeoJSON(arcgis.rings);
        }

        if (arcgis.attributes && arcgis.geometry) {
            geojson.type = "Feature";
            geojson.geometry = parse(arcgis.geometry);
            geojson.properties = arcgis.attributes;
        }

        var inputSpatialReference = (arcgis.geometry) ? arcgis.geometry.spatialReference : arcgis.spatialReference;

        //convert spatial ref if needed
        if (inputSpatialReference && inputSpatialReference.wkid !== 4326) {
            console.warn("geojson has to be crs 4326");
        }
        return geojson;
    };

    // GeoJSON -> ArcGIS
    var convert = function(input, crs) {
        crs = crs || 4326;
        var geojson = input; //clone?-->JSON.parse(JSON.stringify(input));
        var spatialReference = {wkid: crs};
        var result = {};
        var i;
        switch (geojson.type) {
            case "Point":
                result.x = geojson.coordinates[0];
                result.y = geojson.coordinates[1];
                result.spatialReference = spatialReference;
                break;
            case "MultiPoint":
                result.points = geojson.coordinates;
                result.spatialReference = spatialReference;
                break;
            case "LineString":
                result.paths = [geojson.coordinates];
                result.spatialReference = spatialReference;
                break;
            case "MultiLineString":
                result.paths = geojson.coordinates;
                result.spatialReference = spatialReference;
                break;
            case "Polygon":
                result.rings = flattenPolygonRings(geojson.coordinates);
                result.spatialReference = spatialReference;
                break;
            case "MultiPolygon":
                result.rings = flattenMultiPolygonRings(geojson.coordinates);
                result.spatialReference = spatialReference;
                break;
            case "Feature":
                result.geometry = convert(geojson.geometry, crs);
                result.attributes = geojson.properties;
                break;
            case "FeatureCollection":
                result = [];
                for (i = 0; i < geojson.features.length; i++) {
                    result.push(convert(geojson.features[i], crs));
                }
                break;
            case "GeometryCollection":
                result = [];
                for (i = 0; i < geojson.geometries.length; i++) {
                    result.push(convert(geojson.geometries[i], crs));
                }
                break;
        }
        return result;
    };

    return {
        fromGeometry: parse,
        toGeometry: convert
    };
});