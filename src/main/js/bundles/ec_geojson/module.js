define([
  "ct/Stateful",
  "./GeoJsonServiceExtensionController",
  "./GeoJsonServiceTypeFactory",
  "./GeoJsonServiceTypeRegistrator",
  "./EsriSymbolCreator",
  "./SymbolAddingGeoJsonTransformer"
], {});
