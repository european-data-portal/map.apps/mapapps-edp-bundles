define([
  "dojo/_base/declare",
  "dojo/_base/lang",
  "dojo/_base/url",
  "../Deferred",
  "ct/request"
], function(declare, lang, d_url, Deferred, request) {
  return declare([], {
    type: "GeoJSON",
    wkidMapping: null,
    _traversalHandlers: {
      Point: function(extent, x) {
        this._updateBoundingBox(extent, 0, x.coordinates);
      },
      MultiPoint: function(extent, x) {
        this._updateBoundingBox(extent, 1, x.coordinates);
      },
      LineString: function(extent, x) {
        this._updateBoundingBox(extent, 1, x.coordinates);
      },
      MultiLineString: function(extent, x) {
        this._updateBoundingBox(extent, 2, x.coordinates);
      },
      Polygon: function(extent, x) {
        this._updateBoundingBox(extent, 2, x.coordinates);
      },
      MultiPolygon: function(extent, x) {
        this._updateBoundingBox(extent, 3, x.coordinates);
      },
      GeometryCollection: function(extent, x) {
        if (!x.geometries) throw new Error(this.i18n.errors.invalidGeoJSON);
        for (var i = 0; i < x.geometries.length; ++i) {
          this._traverseGeoJSON(extent, x.geometries[i]);
        }
      },
      FeatureCollection: function(extent, x) {
        if (!x.features) throw new Error(this.i18n.errors.invalidGeoJSON);
        for (var i = 0; i < x.features.length; ++i) {
          this._traverseGeoJSON(extent, x.features[i]);
        }
      },
      Feature: function(extent, x) {
        this._traverseGeoJSON(extent, x.geometry);
      }
    },

    constructor: function(options) {
      options = options || {};
      this.wkidMapping = options.wkids;
    },

    activate: function() {
      this.i18n = this._i18n.get();
    },

    _updateBoundingBox: function(extent, dim, x) {
      if (!x) throw new Error(this.i18n.errors.invalidGeoJSON);
      if (dim === 0) {
        if (x.length < 2) throw new Error(this.i18n.errors.invalidGeoJSON);
        if (extent.xmin > x[0]) extent.xmin = x[0];
        if (extent.ymin > x[1]) extent.ymin = x[1];
        if (extent.xmax < x[0]) extent.xmax = x[0];
        if (extent.ymax < x[1]) extent.ymax = x[1];
      } else {
        for (var i = 0, l = x.length; i < l; ++i) {
          this._updateBoundingBox(extent, dim - 1, x[i]);
        }
      }
    },
    _getWKID: function(crs) {
      if (!crs) {
        return null;
      }
      if (crs.type !== "name") {
        throw new Error(this.i18n.errors.unsupportedCRSType);
      }
      var name = crs.properties.name;
      for (var key in this.wkidMapping) {
        if (name.indexOf(key) >= 0) {
          return this.wkidMapping[key];
        }
      }
      var message = this.i18n.errors.canNotParseNamedCRS;
      throw new Error(message.replace("${name}", name));
    },
    _traverseGeoJSON: function(extent, x) {
      if (!x) throw new Error(this.i18n.errors.invalidGeoJSON);
      var wkid = this._getWKID(x.crs);
      if (wkid) {
        if (
          extent.spatialReference.wkid &&
          extent.spatialReference.wkid !== wkid
        ) {
          throw new Error(this.i18n.errors.mixingOfDifferentCRS);
        } else {
          extent.spatialReference.wkid = wkid;
        }
      }
      var handlers = this._traversalHandlers;
      if (x.type && handlers[x.type]) {
        handlers[x.type].call(this, extent, x);
      } else {
        var message = this.i18n.errors.invalidGeoJSONType;
        throw new Error(message.replace("${type}", x.type));
      }
    },
    calculateExtent: function(x) {
      var extent = {
        xmin: Number.POSITIVE_INFINITY,
        ymin: Number.POSITIVE_INFINITY,
        xmax: Number.NEGATIVE_INFINITY,
        ymax: Number.NEGATIVE_INFINITY,
        spatialReference: { wkid: null }
      };
      this._traverseGeoJSON(extent, x);

      // set the default CRS
      if (!extent.spatialReference.wkid) {
        extent.spatialReference.wkid = 4326;
      }

      return extent;
    },
    getExtent: function(resource) {
      var _this = this;
      return request({ url: resource.getURL(), handleAs: "json" }).then(
        function(json) {
          resource.setExtent(_this.calculateExtent(json));
          json._serviceType = "GeoJSON";
          resource.setPayload(json);

          var url = new d_url(resource.getURL());
          if (url) {
            resource.setPayloadTitle(url.path);
          }

          return resource.getExtent();
        },
        lang.hitch(this, function(error) {
          console.error("could not access GeoJSON", error);
        })
      );
    }
  });
});
