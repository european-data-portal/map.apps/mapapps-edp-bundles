define([
  "dojo/_base/declare",
  "dojo/_base/lang",
  "../console",
  "../Deferred",
  "dojo/query",
  "ct/request"
], function(declare, lang, console, Deferred, query, request) {
  return declare([], {
    /**
     * The analyzers (injected).
     */
    analyzers: null,
    /**
     * The initial extent once loaded.
     */
    _initialExtent: null,
    /**
     * The CKAN resource once loaded.
     */
    _ckanResource: null,
    /**
     * The resource fetcher (injected).
     */
    resourceFetcher: null,
    /**
     * the URL of the service once resolved
     */
    _url: null,
    /**
     * the CKAN id of the dataset once resolved
     */
    _ckanId: null,
    /**
     * the type of the dataset once resolved
     */
    _type: null,
    i18n: null,
    /**
     * the errorMessages handler (injected)
     */
    errorMessages: null,

    constructor: function(options) {
      options = options || {};
      this._extentRestrictions = options.extentRestrictions || {};
    },
    /**
     * Activates this component.
     *
     * Immediate components can return a Deferred!
     *
     * @param componentContext the component context
     */
    activate: function(componentContext) {
      console.log("ParameterInterceptor: activate");
      this.i18n = this._i18n.get();
      return this._getQueryParameters(componentContext)
        .then(lang.hitch(this, this._onQueryParameters))
        .then(lang.hitch(this, this._onDataset))
        .then(lang.hitch(this, this._onCKANResource))
        .then(lang.hitch(this, this._onExtent))
        .then(null, lang.hitch(this, this._onLoadFailure));
    },
    deactivate: function() {
      console.log("ParameterInterceptor: deactivate");
    },

    _validateQueryParameters: function(param) {
      if (!param) {
        var e = new Error(this.i18n.errors.missingQueryParameter);
        e.userInput = true;
        throw e;
      }
      else if (!param.type) {
        var e2 = new Error(this.i18n.errors.missingTypeQueryParameter);
        e2.userInput = true;
        throw e2;
      }
      else if (!param.dataset && !param.url) {
        var e3 = new Error(this.i18n.errors.missingDatasetOrUrlQueryParameter);
        e3.userInput = true;
        throw e3;
      }
    },
    _onQueryParameters: function(param) {
      this._validateQueryParameters(param);

      this._url = param.url;
      this._ckanId = param.dataset;
      this._type = param.type;

      return {
        type: this._type,
        // for requesting the CKAN resource
        id: this._ckanId,
        // for requesting the dataset directly
        url: this._url,
        format: param.format,
        title: param.title,
        description: param.description
      };
    },
    _onDataset: function(dataset) {
      return this.resourceFetcher.fetch(dataset);
    },
    _onCKANResource: function(resource) {
      console.log("ParameterInterceptor: got CKAN resource");
      this._ckanResource = resource;
      if (resource) {
        this._url = resource.getURL();
      }
      var type = resource.getType();
      var analyzer = this._getAnalyzerForType(type);

      this._eventService.sendEvent(
        "ec/odp/ON_CKAN_RESOURCE",
        this._ckanResource
      );

      return analyzer.getExtent(resource);
    },
    _onExtent: function(extent) {
      console.log("ParameterInterceptor: resolved extent");
      this._initialExtent = extent;

      this._analyseExtent();

      this._eventService.sendEvent("ec/odp/extent/RESOLVED", {
        extent: extent
      });

      return extent;
    },
    _analyseExtent: function() {
      var unsupportedCrs = lang.hitch(this, function() {
          var error = {
            type: "notSupported",
            reason: "crs",
            url: this._url
          };
          this.errorMessages.presentError(error);
          return false;
        }),
        invalidMetadata = lang.hitch(this, function() {
          var error = {
            type: "notSupported",
            reason: "metadata",
            url: this._url
          };
          this.errorMessages.presentError(error);
          return false;
        }),
        isNumber = function(x) {
          return x !== null && x !== undefined && !isNaN(x);
        };

      var ex = this._initialExtent;

      if (!ex) {
        return invalidMetadata();
      }
      if (!ex.spatialReference || !ex.spatialReference.wkid) {
        return unsupportedCrs();
      }
      var restriction = this._extentRestrictions[ex.spatialReference.wkid];
      if (!restriction) {
        return unsupportedCrs();
      }
      if (
        (isNumber(restriction.xmax) && ex.xmax > restriction.xmax) ||
        (isNumber(restriction.ymax) && ex.ymax > restriction.ymax) ||
        (isNumber(restriction.xmin) && ex.xmin < restriction.xmin) ||
        (isNumber(restriction.ymin) && ex.ymin < restriction.ymin)
      ) {
        //out of bounds, inform the user about possible issues
        return invalidMetadata();
      }
      return true;
    },
    _onLoadFailure: function(err) {
      // set these to null
      this._initialExtent = null;
      this._ckanResource = null;
      this._failure = true;

      getErrorProperties.call(this, err).then(
        lang.hitch(this, function(reason) {
          this._errorToBePresented = reason;
          if (this._failureRequested) {
            this.isFailure();
          }
        })
      );

      //fire event for erroneous extent
      this._eventService.sendEvent("ec/odp/ON_EXTENT_ERROR");
      // swallow err to get this component into the activated state

      function getErrorProperties(err) {
        var message = this.i18n.errors.couldNotLoadDataset;

        message = message.replace("${cause}", err.message || err);

        if (this._unsupportedServiceType) {
          return Deferred.resolve(
            notSupportedServiceType(this._unsupportedServiceType)
          );
        } else if (err.userInput) {
          //probably a manually thrown error
          return Deferred.resolve(userInput(err, this._url));
        } else if (!err.response) {
          //probably a manually thrown error
          return Deferred.resolve(notSupported(err, this._url));
        } else {
          return checkForProxyError.call(this);
        }
      }

      function isCKAN(err) {
        if (err.response.data) {
          try {
            var json = JSON.parse(err.response.data);
            if (json.error && json.error.message === "Not found") {
              return true;
            }
          } catch (e) {
            console.error("could not process error JSON", e);
          }
        }
        return false;
      }
      function notSupported(err, url) {
        return {
          type: "notSupported",
          reason: "metadata",
          additionalDetails: err,
          url: url
        };
      }
      function userInput(err, url) {
        return {
          type: "userInput",
          reason: "missingParameter",
          additionalDetails: err,
          url: url
        };
      }

      function notSupportedServiceType(serviceType) {
        return {
          type: "notSupported",
          unsupportedServiceType: serviceType,
          url: ""
        };
      }

      function noProxyErrorMessage(err) {
        return {
          type: "notAvailable",
          httpCode: err.response.status,
          url: err.response.url,
          ckan: isCKAN(err)
        };
      }

      function proxyErrorMessage(proxyUrl) {
        return {
          type: "notAvailable",
          httpCode: "generic",
          url: proxyUrl,
          proxyError: true
        };
      }

      function checkForProxyError() {
        var proxyUrl = dojoConfig.esri.defaults.io.proxyUrl;
        // check if the proxy is the issue
        if (!proxyUrl) {
          return Deferred.resolve(noProxyErrorMessage(err));
        } else {
          var params = {
            url: this.resourceFetcher.getUrl(),
            timeout: this.resourceFetcher.getTimeout()
          };
          return request(params).then(
            function(body) {
              if (!body) {
                return proxyErrorMessage(proxyUrl);
              } else {
                return noProxyErrorMessage(err);
              }
            },
            lang.hitch(this, function(error) {
              if (error && error.response) {
                return noProxyErrorMessage(err);
              } else {
                console.error("could not access proxy", error);
                return proxyErrorMessage(proxyUrl);
              }
            })
          );
        }
      }
    },
    /**
     * for ec.odp.mapping.InitialExtentProvider
     */
    getInitialExtent: function() {
      return this._initialExtent;
    },
    /**
     * for ec.odp.ckan.ResourceProvider
     */
    getCKANResource: function() {
      return this._ckanResource;
    },
    /**
     * for ec.odp.mapping.InitialExtentProvider
     * @returns {Boolean} is an error occurred while loading a resource
     */
    isFailure: function() {
      if (this._errorToBePresented) {
        this.errorMessages.presentError(this._errorToBePresented);
        this._errorToBePresented = null;
      }
      this._failureRequested = true;
      return this._failure;
    },
    /**
     * Obtains the current query parameter.
     * @param {Object} componentContext the component context
     * @returns {Object}
     */
    _getQueryParameters: function(componentContext) {
      if (!componentContext) {
        return Deferred.reject(this.i18n.errors.noComponentContextProvided);
      }
      var bundleContext = componentContext.getBundleContext();
      if (!bundleContext) {
        return Deferred.reject(this.i18n.errors.noBundleContextProvided);
      }
      var parameter = bundleContext.getProperty("Application-QueryParams");
      if (!parameter) {
        return Deferred.reject(this.i18n.errors.noQueryParametersProvided);
      }
      return Deferred.resolve(parameter);
    },
    /**
     * Returns the CKAN analyzer for the type.
     * @param {String} datasetType the type, either 'WMS' or 'GeoJSON'
     * @returns {CKANAnalyzer} the analyzer
     */
    _getAnalyzerForType: function(datasetType) {
      for (var i = 0; i < this.analyzers.length; ++i) {
        if (this.analyzers[i].type === datasetType) {
          return this.analyzers[i];
        }
      }
      var message = this.i18n.errors.datasetTypeNotSupported;
      this._unsupportedServiceType = datasetType;
      throw new Error(message.replace("${type}", datasetType));
    },
    _handleExtentApplied: function(event) {
      console.log("ParameterInterceptor: extent applied");
      if (this._ckanResource) {
        this._eventService.sendEvent(
          "ec/odp/NEW_WMS_LAYER",
          this._ckanResource.getPayload()
        );
      }      
    },
    _handleTemplateAvailable: function(event) {
      console.log("ParameterInterceptor: template available");
      //call components that just have been initialised, they might
      //be interested in failures
      if (this.isFailure()) {
        this._eventService.sendEvent("ec/odp/ON_EXTENT_ERROR");
      }

      var titleSpan = query(".ec-maptitle");
      var resource = this.getCKANResource();

      if (!resource) {
        console.warn("no resource available. cannot set map title");
        return;
      }

      var title =
        resource.getTitle(this.i18n.$locale) || resource.getPayloadTitle();

      if (titleSpan && titleSpan.length > 0 && resource) {
        titleSpan.forEach(function(ts) {
          ts.innerHTML = title;
        });
      }
    },
    _handleExtentRequested: function(event) {
      console.log("ParameterInterceptor: extent requested");
      //someone requested an update on EXTENT status
      if (this._initialExtent || this.isFailure()) {
        window.setTimeout(
          lang.hitch(this, function() {
            if (this.isFailure()) {
              this._eventService.sendEvent("ec/odp/ON_EXTENT_ERROR");
            } else {
              this._eventService.sendEvent("ec/odp/extent/RESOLVED", {
                extent: this._initialExtent
              });
            }
          }),
          500
        );
      }
    },
    handleEvent: function(event) {
      switch (event.getTopic()) {
        case "ec/odp/extent/APPLIED":
          this._handleExtentApplied(event);
          break;
        case "ec/odp/template/AVAILABLE":
          this._handleTemplateAvailable(event);
          break;
        case "ec/odp/extent/REQUESTED":
          this._handleExtentRequested(event);
          break;
      }
    }
  });
});
