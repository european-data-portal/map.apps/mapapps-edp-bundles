define(["dojo/_base/declare", "dojo/_base/lang", "dojo/when"], function(
  declare,
  lang,
  when
) {
  return declare([], {
    _mapState: null,
    _srsChanger: null,
    _coordinateTransformer: null,
    _eventService: null,
    activate: function() {
      console.log("ExtentChanger: activated");
      if (this._eventService) {
        console.log("ExtentChanger: requesting extent");
        this._eventService.sendEvent("ec/odp/extent/REQUESTED");
      }
    },
    deactivate: function() {
      console.log("ExtentChanger: deactivated");
    },
    _handleExtentResolved: function(event) {
      console.log("ExtentChanger: extent resolved");
      var extent = event.getProperty("extent");
      if (!extent) {
        return;
      }
      if (!extent.spatialReference || !extent.spatialReference.wkid) {
        this._mapState.setExtent(extent);
        this._sendExtentChanged();
        return;
      }
      var oldSrs = this._mapState.getExtent().spatialReference.wkid;
      var newSrs = extent.spatialReference.wkid;

      if (newSrs === oldSrs) {
        console.log("ExtentChanger: setting extent");
        this._mapState.setExtent(extent);
        this._sendExtentChanged();
      } else {
        //handle it as a geometry to not use the map scale (which may be 0)
        extent.type = "extent";
        // to get no conflicts, first set the extent and then change the srs...
        when(this._coordinateTransformer.transform(extent, oldSrs))
          .then(
            lang.hitch(this, function(extent) {
              console.log("ExtentChanger: setting extent");
              this._mapState.setExtent(extent);
            })
          )
          .then(
            lang.hitch(this, function() {
              console.log("ExtentChanger: changing SRS");
              this._srsChanger.changeSrs(newSrs);
            })
          );
      }
    },
    _handleSrsChanged: function(event) {
      console.log("ExtentChanger: SRS changed");
      this._sendExtentChanged();
    },
    _sendExtentChanged: function() {
      if (this._eventService) {
        this._eventService.sendEvent("ec/odp/extent/APPLIED");
      }
    },
    handleEvent: function(event) {
      switch (event.getTopic()) {
        case "ec/odp/extent/RESOLVED":
          this._handleExtentResolved(event);
          break;
        case "sdi_srschanger/SRS/CHANGED":
          this._handleSrsChanged(event);
          break;
      }
    }
  });
});
