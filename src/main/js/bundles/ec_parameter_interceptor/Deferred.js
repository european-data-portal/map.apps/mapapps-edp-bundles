define([
    'dojo/Deferred',
    './util'
], function (Deferred, util) {

    function ConvenientDeferred() {
        Deferred.apply(this, arguments);

        this.reject = (function (inherited) {
            return function (error, strict) {
                if (!(error instanceof Error)) {
                    error = new Error(error);
                }
                inherited.call(this, error, strict);
            };
        })(this.reject);

        this.catch = function (fun) {
            return this.then(function (x) {
                return x;
            }, fun);
        };

        this.finally = function (fun) {
            return this.then(function (x) {
                fun(x);
                return x;
            }, function (x) {
                fun(x);
                throw x;
            });
        };
    };

    ConvenientDeferred.prototype = util.inherit(Deferred.prototype);

    ConvenientDeferred.resolve = function(value) {
        var d = new ConvenientDeferred();
        d.resolve(value);
        return d;
    };

    ConvenientDeferred.reject = function(error) {
        var d = new ConvenientDeferred();
        if (error instanceof Error) {
            d.reject(error);
        } else {
            d.reject(new Error(error));
        }
        return d;
    };

    return ConvenientDeferred;
});