define(["dojo/_base/declare"], function(declare) {
  return declare([], {
    resourceProvider: null,
    resourceHandlers: null,
    activate: function() {
      this.i18n = this._i18n.get();
      var type;
      var resource = this.resourceProvider.getCKANResource();
      if (resource) {
        type = resource.getType();
        for (var i = 0; i < this.resourceHandlers.length; ++i) {
          if (this.resourceHandlers[i].type === type) {
            try {
              this.resourceHandlers[i].createLayers(resource);
            } catch (e) {
              console.error(e);
            }
            return;
          }
        }
        // we shouldn't get here, but just in case...
        var message = this.i18n.errors.datasetTypeNotSupported;
        throw new Error(message.replace("${type}", type));
      }
    }
  });
});
