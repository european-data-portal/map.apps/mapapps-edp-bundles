define([
    "dojo/_base/declare",
    "dojo/_base/array",
    "ec_base/mapping/MappingResourceUtils",
    "ec_base/util/CommonID",
    "./DynamicLayerFactory",
    "../console"
], function (declare, d_array, MappingResourceUtils, CommonID, DynamicLayerFactory, console) {

    return declare([DynamicLayerFactory], {
        type: "WMS",
        layers: [],
        createLayers: function (resource) {
            this.origService = resource.getPayload();
            
            if (!this.origService) {
                throw new Error("The Service provided invalid WMS Capabilities");
            }
            
            this.extent = resource.getExtent();
            this.resolveLayers(this.origService.layerInfos, this.layers);
            
            if (!this.layers || this.layers.length === 0) {
                throw new Error("The Service provided WMS Capabilities with 0 layers");
            }
        },
        addLayersToMap: function (layers) {
            if (!layers || layers.length === 0) {
                console.warn("no layers provided. skipping addLayersToMap");
                return;
            }
            
            this.mapModel.getOperationalLayer().removeChildren();
            var url = this.origService.url;
            var type = 'WMS';
            var mrr = this.mappingResourceRegistry;
            var mrf = this.mappingResourceFactory;
            var dataAdded = false;
            try {
                var service = MappingResourceUtils.getServiceResource(mrr, mrf, url, type, "");
                d_array.forEach(layers, function (l) {
                    var id = l.title;
                    var serviceNode = CommonID.findIdInModel(this.mapModel, id);

                    if (!serviceNode) {
                        //we only create a layer if it´s not already there
                        var serviceMapModelNode = MappingResourceUtils
                                .addServiceMapModelNode(service, "", this.insertionNode, id);
                        serviceMapModelNode.added = true;
                        serviceMapModelNode.ignoreFullExtent = true;
                        MappingResourceUtils.addLayer(mrr, mrf, service, l, type, serviceMapModelNode);
                        dataAdded = true;
                    }
                }, this);
            } catch (err) {
                //console.error("Unable to add dataset", ex);
                var message = this.i18n.errors.unableToAddDataset;
                message = message.replace('${cause}', (err.message || err));
                //throw new Error(message);
                this.notifier.error(message);
                if (console.isDebug) {
                    console.error(message, err);
                }
            }
            this.mapModel.fireModelStructureChanged({source: this});
            if (dataAdded) {
                this.mapState.setExtent(this.extent);
            }
        },
        getLayersList: function () {
            return this.layers;
        },
        resolveLayers: function (layerInfos, targetArray) {
            d_array.forEach(layerInfos, function (l) {
                if (l.name === "" && l.subLayers) {
                    this.resolveLayers(l.subLayers, targetArray);
                } else {
                    targetArray.push({title: l.title, id: l.name});
                }
            }, this);
            return targetArray;
        }
    });
});
