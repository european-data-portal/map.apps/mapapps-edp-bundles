define([
  "dojo/_base/declare",
  "ct/mapping/mapcontent/MappingResourceFactory"
], function(declare, MappingResourceFactory) {
  return declare([], {
    mappingResourceRegistry: null,
    mapModel: null,
    mapState: null,
    insertionNode: null,
    mappingResourceFactory: null,
    insertionNodeName: "__operationallayer__",

    type: null,

    activate: function() {
      this.i18n = this._i18n.get();
      this.insertionNode = this.mapModel.getNodeById(this.insertionNodeName);
      this.mappingResourceFactory = new MappingResourceFactory();
    },

    createLayers: function() {
      console.warn("createLayers() is not yet implemented");
    }
  });
});
