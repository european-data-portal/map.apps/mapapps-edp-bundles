define([
  "dojo/_base/declare",
  "dojo/_base/array",
  "dojo/number",
  "dojo/io-query",
  "ct/array",
  "ct/mapping/layers/WMSLayer"
], function(declare, d_array, d_number, d_ioq, ct_array, WMSLayer) {
  return declare([WMSLayer], {
    getImageUrl: function(extent, width, height, callback) {
      var visibleLayers = this.visibleLayers;
      return this.inherited(arguments, [
        extent,
        width,
        height,
        function(url) {
          var original = "" + (visibleLayers || null);
          var fixed = encodeURIComponent((visibleLayers || []).join(","));
          return callback(url.replace(original, fixed));
        }
      ]);
    },
    /**
     * Returns the GetFeatureInfo url for this service and given layers.
     * @param {esri.geometry.Point} pixPoint
     * @param {esri.geometry.Extent} extent
     * @param {Number} width
     * @param {Number} height
     * @param {String[]} layerIds optinal array containing layer ids
     * @param {String} [format] format (default: text/html)
     * @return url for feature info request
     */
    getFeatureInfoUrl: function(
      pixPoint,
      extent,
      width,
      height,
      layerIds,
      format
    ) {
      var infoLayers =
        this.infoLayers && this.infoLayers.length > 0
          ? this.infoLayers
          : this.visibleLayers;
      // check if all queryable
      infoLayers = d_array.filter(
        infoLayers,
        function(name) {
          var l = this.getLayerInfo(name);
          // if layer info not found, accept it
          return !l || l.queryable;
        },
        this
      );

      if (infoLayers.length === 0) {
        // send a warning and return no url...
        console.warn(
          "WMS layer(s) '" + layerIds.join(",") + "' not queryable!"
        );
        return null;
      }

      // filter layer ids if set
      layerIds = layerIds || [];
      if (layerIds.length) {
        infoLayers = d_array.filter(
          infoLayers,
          function(name) {
            return d_array.indexOf(layerIds, name) >= 0;
          },
          this
        );
      }

      if (infoLayers.length === 0) {
        throw new Error("WMS layer(s) '" + layerIds.join(",") + "' not found!");
      }
      format = format ? format : "text/html";
      var selFormat = ct_array.arraySearchFirst(this.infoFormat, format);
      if (!selFormat) {
        throw new Error(
          "WMS does not support '" +
            format +
            "' format for getFeatureInfo operation! Supported types are: " +
            this.infoFormat.join(", ")
        );
      }

      var featureCount = this.infoFeatureCount ? this.infoFeatureCount : 10;
      var additionalParams = {
        INFO_FORMAT: selFormat,
        QUERY_LAYERS: infoLayers.join(","),
        FEATURE_COUNT: featureCount
      };
      var xParam = "X";
      var yParam = "Y";
      if (this.version === "1.3.0") {
        xParam = "I";
        yParam = "J";
      }
      // here we uning dojo round because of error in some browsers, were pixpoint is not an int!
      additionalParams[xParam] = d_number.round(pixPoint.x, 0);
      additionalParams[yParam] = d_number.round(pixPoint.y, 0);

      var additionalQueryPart = d_ioq.objectToQuery(additionalParams);

      var url;
      this.getImageUrl(extent, width, height, function(imageUrl) {
        var featureUrl = imageUrl.replace(
          /(REQUEST=)[^&]*&/i,
          "$1GetFeatureInfo&"
        );
        featureUrl = featureUrl.concat("&").concat(additionalQueryPart);
        console.debug("GetFeatureInfo ---->> " + featureUrl);
        url = featureUrl;
      });
      return url;
    }
  });
});
