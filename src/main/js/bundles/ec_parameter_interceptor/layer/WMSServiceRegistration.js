define([
  "dojo/_base/declare",
  "ct/mapping/mapcontent/ServiceTypes",
  "ct/mapping/map/EsriLayerFactory",
  "ct/mapping/map/EsriService",
  "./WMSLayer"
], function(declare, ServiceTypes, EsriLayerFactory, EsriService, WMSLayer) {
  var factories = EsriLayerFactory.globalServiceFactories;
  var count = 0;
  var type = ServiceTypes.WMS;
  var oldFactory = null;
  var factory = {
    create: function(node, url) {
      return new EsriService({
        mapModelNode: node,
        reverseEnabledLayers: true,
        createEsriLayer: function() {
          return new WMSLayer(url, node.get("options") || {});
        }
      });
    }
  };
  return declare([], {
    activate: function() {
      if (++count > 1) return;
      oldFactory = factories[type];
      factories[type] = factory;
    },
    deactivate: function() {
      if (count === 0 || --count > 0) return;
      factories[type] = oldFactory;
      oldFactory = null;
    }
  });
});
