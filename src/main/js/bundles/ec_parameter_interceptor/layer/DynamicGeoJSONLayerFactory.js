define(["dojo/_base/declare", "./DynamicLayerFactory"], function(
  declare,
  DynamicLayerFactory
) {
  return declare([DynamicLayerFactory], {
    type: "GeoJSON",
    createLayers: function(resource) {
      try {
        this.geoJsonController.load(resource);
      } catch (err) {
        throw new Error(this.i18n.errors.unableToAddDataset);
      }
    }
  });
});
