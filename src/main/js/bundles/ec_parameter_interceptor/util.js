define([], function () {

    var inherit = (function () {
        if (typeof Object.create === 'function') {
            return Object.create;
        } else {
            var Temp = function () {
            };
            return function (prototype) {
                if (arguments.length > 1) {
                    throw Error('Second argument not supported');
                }
                if (typeof prototype !== 'object') {
                    throw TypeError('Argument must be an object');
                }
                Temp.prototype = prototype;
                var result = new Temp();
                Temp.prototype = null;
                return result;
            };
        }
    })();

    var isArray = (function () {
        if (typeof Array.isArray === 'function') {
            return Array.isArray;
        } else {
            var toString = Object.prototype.toString;
            return function (x) {
                return toString.call(x) === '[object Array]';
            };
        }
    })();

    var encodeQuery = function (url, param) {
        var idx = url.indexOf('?');
        // remove old query string
        if (idx >= 0) {
            url = url.substring(0, idx);
        }
        var encoded = [];
        var key, value;
        for (key in param) {
            if (!param.hasOwnProperty(key)) {
                continue;
            }
            key = encodeURIComponent(key);
            value = param[key];
            if (value === null) {
                encoded.push(key);
            } else {
                if (isArray(value)) {
                    value = value.join(',');
                }
                value = encodeURIComponent(value);
                encoded.push(key + '=' + value);
            }
        }
        console.log(url+param);
        return url + '?' + encoded.join('&');
    };

    var decodeQuery = function (url) {
        console.log(url);
        var idx = url.indexOf('?');
        if (idx < 0 || idx === url.length - 1) {
            return {};
        }
        var query = url.substr(idx + 1).split('&');
        var i, key, value;
        var params = {};

        for (var i = 0; i < query.length; ++i) {
            idx = query[i].indexOf('=');
            if (idx < 0) {
                key = query[i];
                value = null;
            } else {
                key = query[i].substr(0, idx);
                value = query[i].substr(idx + 1);
            }
            key = key ? decodeURIComponent(key) : key;
            value = value ? decodeURIComponent(value) : value;
            if (params.hasOwnProperty(key)) {
                if (isArray(params[key])) {
                    params[key].push(value);
                } else {
                    params[key] = [params[key], value];
                }
            } else {
                params[key] = value;
            }
        }
        return params;
    };

    return  {
        isArray: isArray,
        inherit: inherit,
        encodeQuery: encodeQuery,
        decodeQuery: decodeQuery
    };
});