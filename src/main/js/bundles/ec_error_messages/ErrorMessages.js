define([
  "dojo/_base/declare",
  "./ErrorMessagesWidget",
  "dojo/_base/lang",
  "./Utils",
  "dojo/i18n",
  "dojo/i18n!esri/nls/jsapi"
], function(declare, ErrorMessagesWidget, d_lang, Utils, i18n, tileBundle) {
  return declare([], {
    overlayUtils: null,

    constructor: function() {},
    activate: function() {
      var props = this._properties || {};
      this._errorInfo = props._errorInfo || "";
      this._errorDetail = props._errorDetail || "";
      this._suppressTileErrors = props._suppressTileErrors;
    },
    _getTicketMessages: function() {
      if (!this._ticketMessages) {
        var general = i18n.getLocalization("ec_i18n", "bundle", "en");
        if (general && Object.getPrototypeOf(general)) {
          var proto = Object.getPrototypeOf(general);

          this._ticketMessages = proto.ec_error_messages;
        } else {
          this._ticketMessages = this._i18n.get();
        }
      }
      return this._ticketMessages;
    },
    presentError: function(error) {
      var ticketMessages = this._getTicketMessages();
      if (!error || error.type === "userInput") {
        // do not present an error on missing parameters
        // --> that should not happen when coming from portal
        return;
      }

      error.httpCode = error.httpCode || "generic";

      var gen = this._i18n.get().httpIssues[error.httpCode];
      if (error.ckan) {
        gen = "CKAN: " + gen;
      }

      var detailed;
      if (error.unsupportedServiceType) {
        detailed =
          this._i18n.get().general["unsupportedServiceType"] +
          ": " +
          error.unsupportedServiceType;
      } else {
        detailed = this._i18n.get().detailedErrors[error.reason];
      }

      var params = {
        type: error.type,
        detailed: detailed,
        moreDetailed: error.additionalDetails ? error.additionalDetails : "",
        general: this._i18n.get().general,
        reason: error.unsupportedServiceType
          ? "unsupportedServiceType"
          : error.reason,
        http: gen,
        httpCode: error.httpCode,
        url: error.proxyUrl ? error.url : this.removeProxy(error.url),
        overlayUtils: this.overlayUtils,
        ticketMessages: ticketMessages,
        ckanResource: this._ckanResource,
        showContactProvider: error.httpCode >= 400
      };

      var ui = new ErrorMessagesWidget(params);
      Utils.showErrorMessagesWindow(ui);
    },
    removeProxy: function(url) {
      if (dojoConfig.esri.defaults.io.proxyUrl && url) {
        return url.replace(dojoConfig.esri.defaults.io.proxyUrl + "?", "");
      }
      return url;
    },
    handleEvent: function(e) {
      switch (e.getTopic()) {
        case "ec/odp/ON_CKAN_RESOURCE":
          console.info("received CKAN resource");
          this._ckanResource = e.getProperties();
          break;
        case "ct/map/NODE_UPDATE_ERROR":
          var node = e.getProperty("mapModelNode");
          var error = e.getProperty("error");
          var msg = (error && error.toString()) || "";
          if (
            (this._suppressTileErrors && this._isTileLoadError(msg)) ||
            (error && error.canceled)
          ) {
            break;
          }
          var url =
            (node && node.service && node.service.serviceUrl) ||
            "no URL available";
          this.presentError({
            type: "notAvailable",
            httpCode: "generic",
            url: url
          });
          break;
      }
      return true;
    },
    _isTileLoadError: function(msg) {
      var i18ned =
        d_lang.getObject("layers.tiled.tileError", false, tileBundle) ||
        "Unable to load tile";
      return (
        msg.indexOf("Unable to load tile") >= 0 || msg.indexOf(i18ned) >= 0
      );
    }
  });
});
