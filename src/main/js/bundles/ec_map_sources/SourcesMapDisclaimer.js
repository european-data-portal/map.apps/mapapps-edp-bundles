define(["dojo/_base/declare",
    "dojo/dom-construct",
    "dojo/query",
    "dojo/html",
    "dojo/_base/window"],
        function (declare, domConstruct, query, html, win) {
            return declare([],
                    {
                        overlayUtils: null,
                        _attribution: "Administrative boundaries: © EuroGeographics © UN-FAO © Turkstat, Cartography: Eurostat - GISCO, 2014;  ",
                        
                        activate: function () {
                            this._injectSources();
                        },
                        
                        _injectSources: function() {
                            var sources = query('.esriAttributionLastItem');
                            if (sources !== null && sources.length === 1) {
                                html.set(sources[0], this._attribution);
                            }
                            else {
                                var esriControlsBR = domConstruct.create('div', {
                                    class: 'esriControlsBR ec_source_attribution',
                                    style: 'bottom: 50px; left: 10px; display: inline;'
                                }, win.body());
                                
                                var esriAttribution = domConstruct.create('span', {
                                    class: 'esriAttribution'
                                }, esriControlsBR);
                                
                                var esriAttributionList = domConstruct.create('span', {
                                    class: 'esriAttributionList'
                                }, esriAttribution);
                                
                                var esriAttributionLastItem = domConstruct.create('span', {
                                    class: 'esriAttributionLastItem'
                                }, esriAttributionList);
                                
                                html.set(esriAttributionLastItem, this._attribution);
                            }
                        },
                        
                        handleEvent: function(event) {
                            this._injectSources();
                        }
                    });
        });
