define({
	contentviewer : {
		bundleName : "Viewer Content",
		bundleDescription : "Visualizza diversi tipi di contenuti.",
		ui : {
			unknownContentError : "Il contenuto \u00e8 sconosciuto.",
			graphicinfotool : {
				title : "Informazioni elemento",
				desc : "Informazioni elemento"
			},
			content : {
				defaultTitle : "Elemento",
				grid : {
					detailView : "Mostra dettagli",
					key : "Propriet\u00e0",
					value : "Valore"
				},
				customTemplate : {
					detailView : "Mostra dettagli"
				},
				attachment : {
					noAttachmentSupport : "Questo strato non supporta gli allegati",
					detailView : "Mostra dettagli"
				},
				AGSDetail : {
					title : "Vedi dettagli",
					print : "Stampa",
					serviceMetadata : "Metadati di servizio",
					serviceURL : "URL",
					serviceCopyrights : "Copyrights",
					serviceTitle : "Titolo",
					pager : {
						pageSizeLabel : "Caratteristica ${currentPage} di ${endPage}"
					},
					key : "Propriet\u00e0",
					value : "Valore",
					detailView : "Mostra dettagli"
				}
			}
		}
	},
	coordinatetransformer : {
		bundleName : "Coordinare trasformatore",
		bundleDescription : "--Funzionalit\u00e0 di base-- La coordinata trasformatore trasforma geometrie da un sistema di coordinate a un altro."
	},
	featureinfo : {
		bundleName : "FeatureInfo",
		bundleDescription : "FeatureInfo visualizza le informazioni sulle caratteristiche di strati attivi.",
		ui : {
			featureInfo : {
				noQueryLayersFound : "Nessun livello ricercabile disponibile!",
				contentInfoWindowTitle : "Identificare",
				noResultsFound : "Nessun risultato trovato.",
				loadingInfoText : "Interrogazione livello ${layerName} (${layerIndex} di ${layersTotal}).",
				featureInfoTool : "Identificare",
				layer : "Strato",
				feature : "Oggetto"
			},
			wms : {
				emptyResult : "Nessun risultato trovato sul layer '${layerTitle}' WMS."
			}
		}
	},
	geometryservice : {
		bundleName : "GeometryService",
		bundleDescription : "Il bundle offre un servizio di geometria centrale."
	},
	geojson : {
		bundleName : "GeoJSON",
		bundleDescription : "Questo pacchetto fornisce un servizio di conversione, in grado di elaborare GeoJSON e noto testo (WKT)."
	},
	infoviewer : {
		bundleName : "Information Display",
		bundleDescription : "Il display messaggio mostra all'utente informazioni sostanziali su un luogo sulla mappa in una finestra.",
		ui : {
			title : "Info Viewer",
			tools : {
				closeInfoWindowMapdeskTool : "Vicino",
				closeInfoWindowMapTool : "Vicino",
				focusMapTool : "Centro Mappa",
				attachToGeorefTool : "Allegare alla posizione",
				mainActivationTool : "Informazioni sulla posizione"
			}
		}
	},
	languagetoggler : {
		bundleName : "Switcher Lingua",
		bundleDescription : "Attraverso la lingua dell'interfaccia lingua switcher pu\u00f2 essere modificato.",
		ui : {
			title : "Lingua"
		}
	},
	map : {
		bundleName : "Mappa",
		bundleDescription : "Il bundle Map gestisce la mappa principale e tutte le informazioni contenuti cartografici. Il cliente pu\u00f2 astrarre i servizi utilizzati in un modello gerarchico mappa in modo che possano essere visualizzati in vari modi per l'utente.",
		ui : {
			nodeUpdateError : {
				info : "Aggiornamento del servizio '${title}' non riuscita! Errore: ${errorMsg}",
				detail : "Aggiornamento del servizio '${title}' non riuscita! Errore: ${url} ${errorMsg} - dettaglio: ${error}"
			},
			sliderLabels : {
				country : "Paese",
				region : "Regione",
				town : "Citt\u00e0"
			}
		},
		drawTooltips : {
			addPoint : "Clicca sulla mappa",
			addMultipoint : "Clicca per iniziare",
			finishLabel : "Finitura"
		}
	},
	mobiletoc : {
		bundleName : "Controllo di livello mobile",
		bundleDescription : "Questo pacchetto fornisce un controllo di livello mobile ready",
		tool : {
			title : "Mappa contenuti",
			tooltip : "Attiva / Disattiva mappa contenuti"
		},
		ui : {
			basemaps : "Mappe di base",
			operationalLayers : "Livelli Argomenti",
			switchUI : {
				noTitle : "no title",
				leftLabel : "Su",
				rightLabel : "Spento"
			}
		}
	},
	mobileview : {
		bundleName : "Palmari View",
		bundleDescription : "Contiene classi speciali che fissano alcune questioni riguardanti dojox.mobile"
	},
	notifier : {
		bundleName : "Notifiche",
		bundleDescription : "Tutti i messaggi di stato, di avanzamento o di errore vengono visualizzati per l'utente in un messaggio pop-up in modo che sia chiaro che cosa sta accadendo nella domanda.",
		ui : {
			title : "Notifier",
			tooltips : {
				close : "Chiudi messaggio",
				glue : "Allega messaggio"
			}
		}
	},
	parametermanager : {
		bundleName : "La gestione dei parametri",
		bundleDescription : "Il parametro Manager \u00e8 responsabile di delegare i parametri dall'URL ai componenti in base all'avvio.",
		ui : {
			encoderBtn : "Strumento Link",
			encoderBtnTooltip : "Strumento Link",
			sendMail : "E-mail",
			refresh : "Rinfrescare",
			linkBoxTitle : "Link URL",
			size : "Dimensioni (Pixel)",
			codeBoxTitle : "Codice da incorporare in HTML",
			qrCode : "QRCode",
			mailBody : "${url}",
			mailSubject : "Guardate a volte questa carta!",
			options : {
				small : "piccolo (480 x 320)",
				medium : "medio (640 x 480)",
				large : "grande (1280 x 1024)",
				custom : "definita dall'utente"
			}
		}
	},
	printing : {
		bundleName : "Stampa",
		bundleDescription : "Questo pacchetto fornisce uno strumento di stampa per stampare la mappa.",
		tool : {
			title : "Stampa",
			tooltip : "Stampa",
			back : "Indietro"
		},
		resultWin : {
			title : "Stampa"
		}
	},
	qrcode : {
		bundleName : "QRCode",
		bundleDescription : "Questo pacchetto fornisce un servizio che pu\u00f2 essere utilizzato per creare codici QR.",
		errorMessage : "La generazione di codice QR non ha avuto successo."
	},
	splashscreen : {
		bundleName : "Schermata iniziale",
		bundleDescription : "La schermata iniziale visualizza una barra di avanzamento, mentre l'applicazione viene avviata.",
		loadTitle : "Avviare l'applicazione '{appName}'",
		loadBundle : "{percent}% - {startedBundles}/{maxBundlesToStart} - carica {name}"
	},
	system : {
		bundleName : "Sistema",
		bundleDescription : "--Funzionalit\u00e0 di base-- Il sistema descrive le funzionalit\u00e0 di base di base (scheletro compilabile) di map.apps."
	},
	templatelayout : {
		bundleName : "Vista layout",
		bundleDescription : "--Funzionalit\u00e0 di base-- il layout del modello implementa la disposizione di tutti gli elementi dell'interfaccia utente basati su modelli predefiniti."
	},
	templates : {
		bundleName : "Vista",
		bundleDescription : "---Funzionalit\u00e0 di base-- Vista sono utilizzati dal layout del modello.",
		templates : {
			desktop : {
				title : "Desktop",
				desc : "Un layout del desktop-oriented."
			},
			modern : {
				title : "Moderno",
				desc : "Una moderna struttura di stile."
			},
			minimal : {
				title : "Minimo",
				desc : "Un layout minimalista"
			}
		},
		ui : {
			selectorLabelTitle : "Vista"
		}
	},
	themes : {
		bundleName : "Stile",
		bundleDescription : "--Funzionalit\u00e0 di base-- Il bundle Temi gestisce tutte le informazioni CSS come i colori, gli stili dei caratteri, immagini di sfondo, ecc Gli utenti possono passare da differenti sguardi degli elementi dell'interfaccia utente, selezionando diversi stili.",
		themes : {
			pure : {
				desc : "'Puro' stile Le map.apps."
			},
			night : {
				desc : "I map.apps 'notte' stile."
			}
		},
		ui : {
			selectorLabelTitle : "Stile"
		}
	},
	windowmanager : {
		bundleName : "Finestra Gestione",
		bundleDescription : "--Funzionalit\u00e0 di base-- Il window manager \u00e8 responsabile della gestione di tutte le finestre di dialogo.",
		ui : {
			defaultWindowTitle : "Finestra",
			closeBtn : {
				title : "Vicino"
			},
			minimizeBtn : {
				title : "Ridurre al minimo"
			},
			maximizeBtn : {
				title : "Massimizzare"
			},
			restoreBtn : {
				title : "Ripristinare"
			},
			opacityBtn : {
				title : "Cambia la trasparenza"
			},
			collapseBtn : {
				title : "Hide content"
			},
			loading : {
				title : "Attendere prego!",
				message : "Caricamento In Corso ..."
			},
			okcancel : {
				title : "Domanda",
				okButton : "OK",
				cancelButton : "Cancellare"
			}
		}
	},
	ec_legend : {
		bundleName : "Leggenda",
		bundleDescription : "This bundle provides the Esri legend.",
		tool : {
			title : "Leggenda",
			tooltip : "Leggenda"
		}
	},
	ec_parameter_interceptor : {
		bundleName : "ec-parameter-interceptor",
		bundleDescription : "ec-parameter-interceptor",
		errors : {
			missingQueryParameter : "Missing query parameters",
			missingTypeQueryParameter : "Missing type query parameter",
			missingDatasetOrUrlQueryParameter : "Missing dataset or url query parameter",
			noComponentContextProvided : "No componentContext provided",
			noBundleContextProvided : "No bundleContext provided",
			noQueryParametersProvided : "No query parameters provided",
			datasetTypeNotSupported : "The type '${type}' is not supported",
			invalidGeoJSON : "Invalid GeoJSON",
			unsupportedCRSType : "Only named CRS are supported",
			canNotParseNamedCRS : "Can not parse named CRS to WKID: ${name}",
			mixingOfDifferentCRS : "Mixing of different CRS is not supported",
			invalidGeoJSONType : "Unknown GeoJSON type attribute: ${type}",
			typeNotFoundInResource : "No appropriate format for ${type} found in resource",
			couldNotLoadDataset : "Could not load dataset: ${cause}",
			unableToAddDataset : "Unable to add dataset: ${cause}",
			unableToParseCapabilities : "Unable to parse WMS Capabilities response",
			canNotDetermineUrl : "Could not determine the download URL of the resource"
		}
	},
	ec_feature_info : {
		tool : {
			title : "Informazioni dettagliate",
			tooltip : "Informazioni dettagliate"
		}
	},
	ec_map_loading_screen : {
		bundleName : "ec-map-loading-screen",
		bundleDescription : "ec-map-loading-screen",
		messages : {
			initializingMap : "Caricamento della cartina..."
		}
	},
	ec_wms_layer_selector : {
		bundleName : "ec-wms-layer-selector",
		bundleDescription : "ec-wms-layer-selector",
		ui : {
			windowTitle : "Seleziona livelli",
			hint : "Il servizio WMS selezionato contiene ${countToProvideSelection} livelli, si prega di selezionare tutti i livelli da mostrare sulla cartina.",
			selectAll : "Seleziona tutto",
			closeButton : "OK"
		},
		tool : {
			title : "Livelli",
			tooltip : "Livelli"
		}
	},
	ec_error_messages : {
		bundleName : "ec-error-messages",
		bundleDescription : "ec-error-messages",
		general : {
			serviceNotAvailable : "Il servizio richiesto non \u00e8 disponibile",
			serviceNotSupported : "Il servizio richiesto non \u00e8 supportato",
			invalidResource : "Questo URL contiene una risorsa non valida",
			unsupportedResource : "Questo URL contiene una risorsa non supportata",
			errorSource : "\u00c8 stata determinata la seguente fonte di errore",
			requestedUrl : "URL richiesto",
			unsupportedServiceType : "Il servizio richiesto non \u00e8 supportato",
			technicalSupport : "Se il problema persiste, contattare l\u2019assistenza tecnica",
			technicalSupportCreateTicket : "Contattare l\u2019assistenza tecnica",
			weWillContactProvider : "Contatteremo il fornitore del servizio e segnaleremo il problema.",
			closeButton : "OK"
		},
		detailedErrors : {
			crs : "Il servizio non fornisce un sistema supportato di riferimento coordinato",
			encoding : "Il servizio non fornisce un sistema di codificazione dei caratteri supportato",
			metadata : "Il documento dei metadati del servizio \u00e8 errato",
			fileFormat : "\u00c8 stato ricevuto un formato di dati non valido al dato URL",
			dataRequest : "Il sistema ha ricevuto una risposta non valida durante la ricerca dei dati del servizio. Le cause possono essere, ad esempio: errore interno del server, configurazione errata del servizio, un comportamento non standard-non conforme del servizio, ecc."
		},
		httpIssues : {
			303 : "Il risultato della ricerca si pu\u00f2 trovare su un altro URI",
			305 : "La risorsa richiesta \u00e8 disponibile solo mediante un proxy",
			400 : "Il server non pu\u00f2 elaborare la richiesta a causa di un errore da parte del cliente",
			401 : "La risorsa richiesta richiede un\u2019autorizzazione",
			403 : "La risorsa richiesta non \u00e8 aperta al pubblico",
			404 : "La risorsa richiesta non \u00e8 stata trovata",
			405 : "La risorsa \u00e8 stata richiesta mediante un metodo HTTP non autorizzato",
			406 : "\u00c8 stato richiesto un contenuto non accettabile dalla risorsa",
			407 : "La risorsa richiede un\u2019autenticazione preventiva presso un servizio proxy",
			500 : "La risorsa non \u00e8 disponibile a causa di un errore interno del server",
			501 : "La risorsa non \u00e8 disponibile a causa di un metodo di richiesta non riconosciuto",
			502 : "La risorsa non \u00e8 disponibile a causa di un gateway erroneamente configurato",
			503 : "La risorsa non \u00e8 al momento disponibile a causa di un sovraccarico o di attivit\u00e0 di manutenzione",
			504 : "La risorsa non \u00e8 disponibile a causa di un gateway erroneamente configurato o non disponibile",
			505 : "La risorsa non supporta la versione http utilizzata",
			generic : "Si \u00e8 verificato un errore del server durante la richiesta della risorsa"
		}
	},
	ec_user_tutorial : {
		bundleName : "ec-user-tutorial",
		bundleDescription : "ec-user-tutorial",
		externalResourceDisclaimer : {
			introduction : "Questo visualizzatore di cartine accede a servizi esterni e mostra i loro dati. La manutenzione di questi servizi esterni non \u00e8 garantita dalla Commissione europea e quindi non abbiamo alcuna influenza sulla loro disponibilit\u00e0/stabilit\u00e0. Potrebbero verificarsi i problemi seguenti:",
			rotatingCircle : "Un cerchio rotante sulla cartina indica che il visualizzatore sta aspettando una risposta da una fonte di dati. Questo potrebbe essere dovuto da un server non disponibile o a problemi di rete.",
			layerZoomLevel : "Alcune livelli potrebbero non essere visibili a dati livelli di ingrandimento. Questo \u00e8 dovuto a restrizioni fissate dal fornitore esterno del servizio. Potrebbe occorrere ingrandire o ridurre l\u2019immagine per visualizzare i dati del livello.",
			layerPanning : "I dati provenienti da un server esterno potrebbero trovarsi al di fuori dell\u2019attuale visualizzazione della cartina. Questo visualizzatore di cartine cerca automaticamente di identificare la zona coperta. In alcuni casi i metadati sono per\u00f2 incompleti o errati, e quindi si dovr\u00e0 navigare sull\u2019area presente.",
			detailInformation : "Se si richiedono informazioni dettagliate per una caratteristica selezionata, questo sar\u00e0 gestito da una richiesta specifica al server della cartina. Se la risposta a questa richiesta \u00e8 mal formulata o vuota (per es. a causa di un errore del server), non potranno essere visualizzate informazioni e apparir\u00e0 una finestra vuota.",
			technicalSupport : "Se si riscontrano altri problemi, si prega di contattare l\u2019assistenza tecnica:",
			technicalSupportContactLink : "Contattare l\u2019assistenza tecnica",
			donotshow : "Non mostrare pi\u00f9 questa schermata di benvenuto.",
			mapLoading : "Caricamento della cartina...",
			mapScaling : "Le carte grafiche usate in questa applicazione sono fornite dall\u2019ufficio statistico dell\u2019Unione europea (Eurostat). Queste cartine sono attualmente disponibili con una risoluzione di fino a 1:50.000. Sono attualmente in preparazione cartine con una migliore risoluzione (per es. per visualizzare i dati all\u2019interno di una citt\u00e0). Pertanto, alcune fonti di dati potrebbero non essere ancora visualizzate in modo ottimale su una cartina."
		},
		legalNotice : "Le denominazioni utilizzate e la presentazione di materiale su questa cartina non implicano l\u2019espressione di alcuna opinione da parte dell\u2019Unione europea riguardo la situazione legale di alcun paese, territorio, citt\u00e0 o regione o delle sue autorit\u00e0 o riguardante la delimitazione delle sue frontiere e dei suoi confini. Kosovo*: la denominazione \u00e8 imparziale per quanto riguarda le posizioni sullo status ed \u00e8 in linea con la UNSCR 1244/1999 e con l\u2019opinione dell\u2019ICJ sulla dichiarazione di indipendenza del Kosovo. Palestina*: la denominazione non deve essere interpretata come un riconoscimento dello Stato della Palestina e non pregiudica le singole posizioni degli Stati membri sulla questione.",
		legalNoticeHeader : "Avviso legale",
		tutorial : {
			welcome : "Benvenuto sul visualizzatore delle cartine del Portale europeo dei dati",
			familiarise : "Questa piccola introduzione ha lo scopo di farti familiarizzare con gli elementi e le funzionalit\u00e0 del visualizzatore delle cartine.",
			navigation : "Clicca e tieni premuto il tasto sinistro del mouse per spostarti sulla cartina.",
			zoom : "Questi tasti cambiano il livello di zoom sulla cartina. In alternativa puoi usare la rotellina del mouse.",
			features : "Altre funzionalit\u00e0 sono disponibili tramite questi tasti. Servono ad attivare o disattivare una data funzionalit\u00e0.",
			legend : "Puoi usare la legenda per esaminare i livelli della cartina disponibili e attivare o disattivare la loro visualizzazione. I nomi sono ripresi direttamente dal servizio accessibile dall\u2019esterno.",
			transparency : "\u00c8 possibile anche regolare la trasparenza di un livello.",
			featureinfo : "\u00c8 possibile esaminare alcuni dati pi\u00f9 dettagliatamente. Quando si attiva questa funzione, \u00e8 possibile cliccare sulla cartina per richiedere ulteriori informazioni.",
			okButton : "OK",
			closeButton : "Chiudi",
			next : "Successivo",
			back : "Precedente",
			skip : "Salta",
			done : "Fatto"
		}
	}
});