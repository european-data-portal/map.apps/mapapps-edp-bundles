var https = require('http');
fs = require('fs');

var languages = {
    bg: true,
    cs: true,
    da: true,
    de: true,
    et: true,
    el: true,
    es: true,
    fr: true,
    ga: true,
    hr: true,
    it: true,
    lv: true,
    lt: true,
    hu: true,
    mt: true,
    nl: true,
    pl: true,
    pt: true,
    ro: true,
    sk: true,
    sl: true,
    fi: true,
    sv: true
};


var url = 'http://www.europeandataportal.eu/mapapps/resources/jsregistry/root/ec_i18n/3.1.4/nls/';

function callAndFetch(ln) {
    console.log('Fetching ' + ln);
    https.get(url + ln + '/bundle.js#', function(res) {
        res.setEncoding('utf8');
        var body = '';
        res.on('data', data => {
            body += data;
        });
        res.on('end', function() {
            fs.writeFile('./' + ln + '/bundle.js', body, function (err) {
                if (err) {
                    return console.log(err);
                }
                console.log('Wrote :' + ln);
            });
        });
    });
}

for (var ln in languages) {
    if (languages.hasOwnProperty(ln)) {
        callAndFetch(ln);
    }
}