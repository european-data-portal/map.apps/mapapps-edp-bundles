define({
	contentviewer : {
		bundleName : "Content Viewer",
		bundleDescription : "Ein Viewer f\u00fcr unterschiedliche Arten von Inhalten.",
		ui : {
			unknownContentError : "Der Inhalt ist unbekannt.",
			graphicinfotool : {
				title : "Element Info",
				desc : "Element Info"
			},
			content : {
				defaultTitle : "Element",
				grid : {
					detailView : "Zeige Details",
					key : "Attribut",
					value : "Wert"
				},
				customTemplate : {
					detailView : "Zeige Details"
				},
				attachment : {
					noAttachmentSupport : "Diese Ebene hat keine Unterst\u00fctzung f\u00fcr Anh\u00e4nge",
					detailView : "Zeige Details"
				},
				AGSDetail : {
					title : "Detailansicht",
					print : "Drucken",
					serviceMetadata : "Service Metadaten",
					serviceURL : "URL",
					serviceCopyrights : "Copyrights",
					serviceTitle : "Titel",
					pager : {
						pageSizeLabel : "Geoobjekt ${currentPage} von ${endPage}"
					},
					key : "Attribut",
					value : "Wert",
					detailView : "Zeige Details"
				}
			}
		}
	},
	coordinatetransformer : {
		bundleName : "Koordinaten Transformator",
		bundleDescription : "--KERNFUNKTIONALIT\u00c4T-- Der Koordinaten Transformator transformiert Geometrien von einem Koordinatensystem in ein anderes."
	},
	featureinfo : {
		bundleName : "FeatureInfo",
		bundleDescription : "FeatureInfo zeigt FeatureInfos f\u00fcr alle aktivierten Layer an.",
		ui : {
			featureInfo : {
				noQueryLayersFound : "Keine durchsuchbaren Layer verf\u00fcgbar!",
				contentInfoWindowTitle : "Identifizieren",
				noResultsFound : "Keine Ergebnisse gefunden.",
				loadingInfoText : "Layer ${layerName} wird abgerufen (${layerIndex} von ${layersTotal}).",
				featureInfoTool : "Identifizieren",
				layer : "Ebene",
				feature : "Objekt"
			},
			wms : {
				emptyResult : "Der WMS-Layer '${layerTitle}' lieferte keine Ergebnisse."
			}
		}
	},
	geometryservice : {
		bundleName : "GeometryService",
		bundleDescription : "Das Modul bietet einen zentralen geometry service an."
	},
	geojson : {
		bundleName : "GeoJson",
		bundleDescription : "Dieses Bundle stellt einen Konvertierungsdienst bereit, der geojson and well-known text (wkt) verarbeiten kann."
	},
	infoviewer : {
		bundleName : "Informationsanzeige",
		bundleDescription : "Die Informationsanzeige zeigt dem Anwender inhaltliche Informationen zu einem Ort auf der Karte in einem Fenster.",
		ui : {
			title : "Info Viewer",
			tools : {
				closeInfoWindowMapdeskTool : "Schlie\u00dfen",
				closeInfoWindowMapTool : "Schlie\u00dfen",
				focusMapTool : "In Karte zentrieren",
				attachToGeorefTool : "An Position anheften",
				mainActivationTool : "Ortsinfo"
			}
		}
	},
	languagetoggler : {
		bundleName : "Sprachumschalter",
		bundleDescription : "Durch den Sprachumschalter kann die Sprache der Benutzeroberfl\u00e4che gewechselt werden.",
		ui : {
			title : "Sprache"
		}
	},
	map : {
		bundleName : "Karte",
		bundleDescription : "Das Karten Bundle stellt die Hauptkarte und alle Karteninhaltsinformationen zur Verf\u00fcgung.",
		ui : {
			nodeUpdateError : {
				info : "Aktualisierung des Dienstes '${title}' fehlgeschlagen! Fehler: ${errorMsg}",
				detail : "Aktualisierung des Dienstes '${title}' fehlgeschlagen! Fehler: ${url} ${errorMsg} - Detail: ${error}"
			},
			sliderLabels : {
				country : "Land",
				region : "Region",
				town : "Stadt"
			}
		},
		drawTooltips : {
			addPoint : "In die Karte klicken",
			addMultipoint : "In die Karte klicken um zu beginnen",
			finishLabel : "Beenden"
		}
	},
	mobiletoc : {
		bundleName : "Mobile Ebenensteuerung",
		bundleDescription : "Dieses Bundle stellt eine mobile Ebenensteuerung bereit",
		tool : {
			title : "Karteninhalt",
			tooltip : "Karteninhalt ein-/ausblenden"
		},
		ui : {
			basemaps : "Hintergrundkarten",
			operationalLayers : "Themenebenen",
			switchUI : {
				noTitle : "kein Titel",
				leftLabel : "An",
				rightLabel : "Aus"
			}
		}
	},
	mobileview : {
		bundleName : "Mobile View",
		bundleDescription : "Enthaelt Klassen, um einige Probleme mit dojox.mobile zu beheben."
	},
	notifier : {
		bundleName : "Benachrichtigungen",
		bundleDescription : "Alle Status-, Fortschritts- oder Fehlermeldungen werden dem Anwender in einer Popup Meldung angezeigt, so dass klar ist, was gerade in der Anwendung passiert.",
		ui : {
			title : "Notifier",
			tooltips : {
				close : "Nachricht schlie\u00dfen",
				glue : "Nachricht anheften"
			}
		}
	},
	parametermanager : {
		bundleName : "Parameter Verwaltung",
		bundleDescription : "Die Parameter Verwaltung ist beim Starten der Anwendung f\u00fcr die Weitergabe der Parameter aus der URL an die entsprechenden Komponenten verantwortlich.",
		ui : {
			encoderBtn : "Link Werkzeug",
			encoderBtnTooltip : "Link Werkzeug",
			sendMail : "E-Mail",
			refresh : "Aktualisieren",
			linkBoxTitle : "Link URL",
			size : "Gr\u00f6\u00dfe (Pixel)",
			codeBoxTitle : "Code zum Einbinden in HTML",
			qrCode : "QRCode",
			mailBody : "${url}",
			mailSubject : "Schau dir mal diese Karte an!",
			options : {
				small : "klein (480 x 320)",
				medium : "mittel (640 x 480)",
				large : "gro\u00df (1280 x 1024)",
				custom : "benutzerdefiniert"
			}
		}
	},
	printing : {
		bundleName : "Drucken",
		bundleDescription : "Stellt ein Druck-Werkzeug bereit um die Karte zu drucken",
		tool : {
			title : "Drucken",
			tooltip : "Drucken",
			back : "Zur\u00fcck"
		},
		resultWin : {
			title : "Drucken"
		}
	},
	qrcode : {
		bundleName : "QRCode",
		bundleDescription : "Dieses Bundle stellt einen Dienst zur Verf\u00fcgung um QR Codes zu erzeugen.",
		errorMessage : "QR Code Generierung war nicht erfolgreich."
	},
	splashscreen : {
		bundleName : "Startbildschirm",
		bundleDescription : "Der Startbildschirm zeigt einen Fortschrittsbalken w\u00e4hrend die Anwendung gestartet wird.",
		loadTitle : "Starte Anwendung '{appName}'",
		loadBundle : "{percent}% - {startedBundles}/{maxBundlesToStart} - Lade {name}"
	},
	system : {
		bundleName : "System",
		bundleDescription : "--KERNFUNKTIONALIT\u00c4T-- Das System beschreibt die Kernfunktionen von map.apps."
	},
	templatelayout : {
		bundleName : "Ansichts-Layout",
		bundleDescription : "--KERNFUNKTIONALIT\u00c4T-- Das Ansichts-Layout ordnet s\u00e4mtliche Elemente der Benutzeroberfl\u00e4che entsprechend vordefinierter Einstellungen an."
	},
	templates : {
		bundleName : "Ansicht",
		bundleDescription : "--KERNFUNKTIONALIT\u00c4T-- Die hier konfigurierten Ansichten werden vom Ansichts-Layout verwendet.",
		templates : {
			desktop : {
				title : "Desktop",
				desc : "Ein Desktop orientiertes Layout."
			},
			modern : {
				title : "Modern",
				desc : "Ein modernes stylisches Layout."
			},
			minimal : {
				title : "Minimal",
				desc : "Ein minimalistisches Layout"
			}
		},
		ui : {
			selectorLabelTitle : "Ansicht"
		}
	},
	themes : {
		bundleName : "Farbschemas",
		bundleDescription : "--KERNFUNKTIONALIT\u00c4T-- Dieses Bundle stellt alle CSS Informationen bereit, wie z.B: Farben.",
		themes : {
			pure : {
				desc : "Das map.apps 'pure' Farbschema."
			},
			night : {
				desc : "Das map.apps 'night' Farbschema."
			}
		},
		ui : {
			selectorLabelTitle : "Farbschema"
		}
	},
	windowmanager : {
		bundleName : "Fenster Verwaltung",
		bundleDescription : "--KERNFUNKTIONALIT\u00c4T-- Die Fenster Verwaltung ist f\u00fcr die Verwaltung aller Dialog-Fenster verantwortlich.",
		ui : {
			defaultWindowTitle : "Fenster",
			closeBtn : {
				title : "Schlie\u00dfen"
			},
			minimizeBtn : {
				title : "Verkleinern"
			},
			maximizeBtn : {
				title : "Maximieren"
			},
			restoreBtn : {
				title : "Wiederherstellen"
			},
			opacityBtn : {
				title : "Transparenz umschalten"
			},
			collapseBtn : {
				title : "Inhalt verstecken"
			},
			loading : {
				title : "Bitte warten!",
				message : "Lade..."
			},
			okcancel : {
				title : "Frage",
				okButton : "OK",
				cancelButton : "Abbrechen"
			}
		}
	},
	ec_legend : {
		bundleName : "Legende",
		bundleDescription : "Dieses Bundle stellt die Esri Legende zur Verf\u00fcgung.",
		tool : {
			title : "Legende",
			tooltip : "Legende"
		}
	},
	ec_parameter_interceptor : {
		bundleName : "ec-parameter-interceptor",
		bundleDescription : "ec-parameter-interceptor",
		errors : {
			missingQueryParameter : "Missing query parameters",
			missingTypeQueryParameter : "Missing type query parameter",
			missingDatasetOrUrlQueryParameter : "Missing dataset or url query parameter",
			noComponentContextProvided : "No componentContext provided",
			noBundleContextProvided : "No bundleContext provided",
			noQueryParametersProvided : "No query parameters provided",
			datasetTypeNotSupported : "The type '${type}' is not supported",
			invalidGeoJSON : "Invalid GeoJSON",
			unsupportedCRSType : "Only named CRS are supported",
			canNotParseNamedCRS : "Can not parse named CRS to WKID: ${name}",
			mixingOfDifferentCRS : "Mixing of different CRS is not supported",
			invalidGeoJSONType : "Unknown GeoJSON type attribute: ${type}",
			typeNotFoundInResource : "No appropriate format for ${type} found in resource",
			couldNotLoadDataset : "Could not load dataset: ${cause}",
			unableToAddDataset : "Unable to add dataset: ${cause}",
			unableToParseCapabilities : "Unable to parse WMS Capabilities response",
			canNotDetermineUrl : "Could not determine the download URL of the resource"
		}
	},
	ec_feature_info : {
		tool : {
			title : "Feature Info",
			tooltip : "Feature Info"
		}
	},
	ec_map_loading_screen : {
		bundleName : "ec-map-loading-screen",
		bundleDescription : "ec-map-loading-screen",
		messages : {
			initializingMap : "Initialisiere Karte. Bitte warten!"
		}
	},
	ec_wms_layer_selector : {
		bundleName : "ec-wms-layer-selector",
		bundleDescription : "ec-wms-layer-selector",
		ui : {
			windowTitle : "Layer ausw\u00e4hlen",
			hint : "Der ausgew\u00e4hlte WMS enh\u00e4lt ${countToProvideSelection} Layer, bitte w\u00e4hlen Sie aus, welche Layer in der Karte geladen werden sollen.",
			selectAll : "Alle ausw\u00e4hlen",
			closeButton : "OK"
		},
		tool : {
			title : "Layer",
			tooltip : "Layer"
		}
	},
	ec_error_messages : {
		bundleName : "ec-error-messages",
		bundleDescription : "ec-error-messages",
		general : {
			serviceNotAvailable : "Der angeforderte Dienst ist nicht verf\u00fcgbar",
			serviceNotSupported : "Der angeforderte Dienst wird nicht unterst\u00fctzt",
			invalidResource : "Eine ung\u00fcltige Ressource wurde unter der angegebenen URL gefunden",
			unsupportedResource : "Eine nicht unterst\u00fctzte Ressource wurde unter der angegebenen URL gefunden",
			errorSource : "Die folgende Fehlerquelle wurde festgestellt",
			requestedUrl : "Angeforderte URL",
			technicalSupport : "Besteht das Problem weiterhin in der Zukunft, kontaktieren Sie bitte den technischen Support",
			unsupportedServiceType : "Der angeforderte Dienst wird nicht unterst\u00fctzt",
			technicalSupportCreateTicket : "Support-Ticket erstellen",
			weWillContactProvider : "Wir werden den Anbieter des Dienstes kontaktieren und den Fehler melden.",
			closeButton : "OK"
		},
		detailedErrors : {
			crs : "Der Dienst bietet keine unterst\u00fctzten Koordinatenreferenzsysteme",
			encoding : "Der Dienst bietet keine unterst\u00fctzten Zeichenkodierungen",
			metadata : "Das Metadaten-Dokument des Dienstes ist fehlerhaft",
			fileFormat : "Eine ung\u00fcltiges Datenformat wurde unter der angegebenen URL gefunden",
			dataRequest : "W\u00e4hrend der Abfrage der Daten des Dienstes wurde eine ung\u00fcltige Antwort empfangen. Gr\u00fcnde daf\u00fcr k\u00f6nnen zum Beispiel sein: interner Serverfehler, falsche Konfiguration des Dienstes, Nicht-Standard-konformes Verhalten des Dienstes, etc."
		},
		httpIssues : {
			303 : "Die Antwort auf die Anfrage kann unter einem anderen URI gefunden werden",
			305 : "Die angeforderte Ressource steht nur \u00fcber einen Proxy zur Verf\u00fcgung",
			400 : "Der Server kann oder m\u00f6chte die Anforderung aufgrund eines Clientfehlers nicht bearbeiten",
			401 : "Die angeforderte Ressource erfordert Autorisation",
			403 : "Die angeforderte Ressource wird nicht f\u00fcr die \u00d6ffentlichkeit bereitgestellt",
			404 : "Die angeforderte Ressource wurde nicht gefunden",
			405 : "Die Ressource wurde durch ein nicht erlaubte HTTP-Methode angefordert",
			406 : "Ein nicht akzeptabler Inhalt wurden von der Ressource angefordert",
			407 : "Die Ressource erfordert eine vorherige Authentifizierung bei einem Proxy-Dienst",
			500 : "Die Ressource ist aufgrund eines internen Server-Fehlers nicht verf\u00fcgbar",
			501 : "Die Ressource ist aufgrund eines nicht anerkannten Anforderungsmethode nicht verf\u00fcgbar",
			502 : "Die Ressource ist aufgrund eines fehlerhaft konfigurierten Gateway nicht verf\u00fcgbar",
			503 : "Die Ressource ist derzeit wegen \u00dcberlastung oder Wartung nicht verf\u00fcgbar",
			504 : "Die Ressource ist aufgrund eines fehlerhaft konfigurierten oder nicht mehr reagierendem Gateway nicht verf\u00fcgbar",
			505 : "Die Ressource unterst\u00fctzt die angegebene HTTP-Version nicht",
			generic : "Ein Server-Fehler ist w\u00e4hrend des Anforderns der angegebenen Ressource aufgetreten"
		}
	},
	ec_user_tutorial : {
		bundleName : "ec-user-tutorial",
		bundleDescription : "ec-user-tutorial",
		externalResourceDisclaimer : {
			introduction : "Dieser Map Viewer greift auf externe Dienste zu und zeigt deren Daten an. Diese externen Dienste werden nicht von der Europ\u00e4ischen Kommission betrieben und daher haben wir keinen Einfluss auf ihre Verf\u00fcgbarkeit/Stabilit\u00e4t. Die folgenden Probleme k\u00f6nnen auftreten:",
			rotatingCircle : "Ein rotierender Kreis in der Kartendarstellung zeigt an, da\u00df der Viewer auf eine Antwort von einer Datenquelle wartet. Dies kann durch einen nicht verf\u00fcgbaren Server oder Netzwerkprobleme verursacht werden",
			layerZoomLevel : "Einige Ebenen k\u00f6nnen bei bestimmten Zoomstufen nicht angezeigt werden. Dies ergibt sich aus Einschr\u00e4nkungen durch den externen Dienstleister. M\u00f6glicherweise m\u00fcssen Sie, um die Daten der Ebene anzuzeigen, den Kartenausschnitt vergr\u00f6\u00dfern oder verkleinern",
			layerPanning : "Daten von einem externen Server k\u00f6nnen au\u00dferhalb der aktuellen Kartenansicht liegen. Dieser Map Viewer versucht, die abgedeckte Fl\u00e4che automatisch zu identifizieren. Doch in einigen F\u00e4llen sind die Metadaten nicht vollst\u00e4ndig oder fehlerhaft, so dass Sie den aktuellen Kartenausschnitt anpassen m\u00fcssen.",
			detailInformation : "Wenn Sie zus\u00e4tzliche Informationen f\u00fcr ein ausgew\u00e4hltes Objekt anfordern, wird dies durch eine spezielle Anfrage an den Kartenserver behandelt. Wenn die Antwort auf diese Anfrage fehlerhaft oder leer ist (z.B. wegen eines Serverfehlers) werden keine Informationen angezeigt und ein leeres Fenster wird angezeigt.",
			technicalSupport : "Wenn Sie auf andere Probleme sto\u00dfen, z\u00f6gern Sie nicht den technischen Support zu kontaktieren:",
			technicalSupportContactLink : "Technischen Support kontaktieren",
			donotshow : "Dieses Fenster nicht nochmals anzeigen.",
			mapLoading : "Map loading...",
			mapScaling : "Die in dieser Anwendung verwendeten Hintergrundkarten werden durch das Statistische Amt der Europ\u00e4ischen Union (Eurostat) bereitgestellt. Diese Karten sind in einer Aufl\u00f6sung von bis zu 1:50.000 verf\u00fcgbar. An einer feineren Aufl\u00f6sung, um beispielsweise Daten innerhalb von St\u00e4dten zu visualisieren, wird zurzeit gearbeitet. Daher k\u00f6nnen einzelne Datenquellen noch nicht mit einer optimalen Hintergrundkarte angezeigt werden."
		},
		legalNotice : "Die in dieser Karte verwendeten Bezeichnungen und die Darstellungsform geben nicht die Auffassung der Europ\u00e4ischen Union zur Rechtsstellung von Staaten, Hoheitsgebieten, St\u00e4dten oder Gebieten bzw. deren Beh\u00f6rden oder zum Verlauf ihrer Grenzen wieder. Das Kosovo*: Diese Bezeichnung ber\u00fchrt nicht die Standpunkte zum Status und steht im Einklang mit der Resolution 1244/1999 des VN-Sicherheitsrates und dem Gutachten des Internationalen Gerichtshofs zur Unabh\u00e4ngigkeitserkl\u00e4rung des Kosovos. Pal\u00e4stina*: Diese Bezeichnung ist nicht als Anerkennung eines Staates Pal\u00e4stina auszulegen und l\u00e4sst die Standpunkte der einzelnen Mitgliedstaaten zu dieser Frage unber\u00fchrt.",
		legalNoticeHeader : "Rechtlicher Hinweis",
		tutorial : {
			welcome : "Willkommen im Map Viewer des European Data Portal",
			familiarise : "Diese kleine Einf\u00fchrung hat das Ziel Sie mit den Elementen und Funktionalit\u00e4t des Map Viewer vertraut zu machen.",
			navigation : "Klicken und halten Sie die linke Maustaste, um die Karte zu verschieben.",
			zoom : "Mit diesen Tasten \u00e4ndern Sie den Zoomfaktor auf der Karte. Alternativ k\u00f6nnen Sie das Mausrad verwenden, um den Zoom einzustellen.",
			features : "Zus\u00e4tzliche Funktionalit\u00e4t wird durch diese Buttons zur Verf\u00fcgung gestellt. Sie fungieren als Schalter und aktivieren oder deaktivieren die angegebene Funktionalit\u00e4t.",
			legend : "Die Legende kann verwendet werden, um zu sehen, welche Kartenebenen zur Verf\u00fcgung stehen und um das Anzeigen dieser auf der Karte zu aktivieren oder deaktivieren. Die Namen werden direkt aus dem externen Dienst \u00fcbernommen.",
			transparency : "Sie k\u00f6nnen auch die Transparenz einer Ebene einstellen.",
			featureinfo : "Einige Daten k\u00f6nnen n\u00e4her untersucht werden. Wenn diese Funktion aktiviert ist, k\u00f6nnen Sie auf die Karte klicken, um weitere Informationen abzufragen.",
			okButton : "OK",
			closeButton : "Schliessen",
			next : "Vor",
			back : "Zur\u00fcck",
			skip : "Abbrechen",
			done : "Fertig"
		}
	}
});
