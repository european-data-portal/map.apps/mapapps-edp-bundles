define({
	contentviewer : {
		bundleName : "Content Viewer",
		bundleDescription : "Displays different kinds of content.",
		ui : {
			unknownContentError : "The content is unknown.",
			graphicinfotool : {
				title : "Item Info",
				desc : "Item Info"
			},
			content : {
				defaultTitle : "Item",
				grid : {
					detailView : "Show details",
					key : "Property",
					value : "Value"
				},
				customTemplate : {
					detailView : "Show details"
				},
				attachment : {
					noAttachmentSupport : "This layer does not offer attachment support",
					detailView : "Open detail view"
				},
				AGSDetail : {
					title : "Detail View",
					print : "Print",
					serviceMetadata : "Service metadata",
					serviceURL : "URL",
					serviceCopyrights : "Copyrights",
					serviceTitle : "Title",
					pager : {
						pageSizeLabel : "Feature ${currentPage} of ${endPage}"
					},
					key : "Property",
					value : "Value",
					detailView : "Show details"
				}
			}
		}
	},
	coordinatetransformer : {
		bundleName : "Coordinate Transformer",
		bundleDescription : "--CORE FUNCTIONALITY-- The Coordinate Transformer transforms geometries from a source coordinate system to a target coordinate system."
	},
	featureinfo : {
		bundleName : "FeatureInfo",
		bundleDescription : "FeatureInfo displays information on features for active layers.",
		ui : {
			featureInfo : {
				noQueryLayersFound : "No queryable layers found!",
				contentInfoWindowTitle : "Identify",
				noResultsFound : "No results found.",
				loadingInfoText : "Querying layer ${layerName} (${layerIndex} of ${layersTotal}).",
				featureInfoTool : "Identify",
				layer : "Layer",
				feature : "Feature"
			},
			wms : {
				emptyResult : "No results found on the WMS layer '${layerTitle}'."
			}
		}
	},
	geometryservice : {
		bundleName : "GeometryService",
		bundleDescription : "The bundle provides a central geometry service."
	},
	geojson : {
		bundleName : "GeoJson",
		bundleDescription : "This bundle provides a conversion service for geojson and well-known text formats"
	},
	infoviewer : {
		bundleName : "Info Viewer",
		bundleDescription : "The Info Viewer shows content information for a location in a moveable and resizable window.",
		ui : {
			title : "Info Viewer",
			tools : {
				closeInfoWindowMapdeskTool : "Close",
				closeInfoWindowMapTool : "Close",
				focusMapTool : "Center in Map",
				attachToGeorefTool : "Attach to position",
				mainActivationTool : "Location Information"
			}
		}
	},
	languagetoggler : {
		bundleName : "Language Toggler",
		bundleDescription : "The language of the user interface can be switched by a language toggler.",
		ui : {
			title : "Language"
		}
	},
	map : {
		bundleName : "Map",
		bundleDescription : "The Map bundle manages the main map and all map content information. The client can abstract the services used in a hierarchical Map model so that they can be displayed in various ways to the user.",
		ui : {
			nodeUpdateError : {
				info : "Update of service '${title}' failed! Msg: ${errorMsg}",
				detail : "Update of service '${title}' failed! Msg: ${url} ${errorMsg} - Details: ${error}"
			},
			sliderLabels : {
				country : "Country",
				region : "Region",
				town : "Town"
			}
		},
		drawTooltips : {
			addPoint : "Click in the map",
			addMultipoint : "Click to start",
			finishLabel : "Finish"
		}
	},
	mobiletoc : {
		bundleName : "Mobile TOC",
		bundleDescription : "This bundle provides a mobile table of contents",
		tool : {
			title : "Map Content",
			tooltip : "Turn on/off Map Content"
		},
		ui : {
			basemaps : "Base maps",
			operationalLayers : "Operational layers",
			switchUI : {
				noTitle : "no title",
				leftLabel : "On",
				rightLabel : "Off"
			}
		}
	},
	mobileview : {
		bundleName : "Mobile View",
		bundleDescription : "Contains special classes that fix some issues concerning dojox.mobile"
	},
	notifier : {
		bundleName : "Notifier",
		bundleDescription : "All messages on status, progress, errors and warnings are shown in a popup message so that a user can understand what is going on in the application.",
		ui : {
			title : "Notifier",
			tooltips : {
				close : "Close this message",
				glue : "Pin this message"
			}
		}
	},
	parametermanager : {
		bundleName : "Parameter Manager",
		bundleDescription : "The Parameter Manager is responsible to delegate parameters from the URL to the according components on startup.",
		ui : {
			encoderBtn : "Link tool",
			encoderBtnTooltip : "Link tool",
			sendMail : "EMail",
			refresh : "Refresh",
			linkBoxTitle : "Link URL",
			size : "Size (Pixels)",
			codeBoxTitle : "Code to embed in HTML",
			qrCode : "QRCode",
			mailBody : "${url}",
			mailSubject : "Check out this map!",
			options : {
				small : "small (480 x 320)",
				medium : "medium (640 x 480)",
				large : "large (1280 x 1024)",
				custom : "custom"
			}
		}
	},
	printing : {
		bundleName : "Print",
		bundleDescription : "This bundle provides a print tool to print the map.",
		tool : {
			title : "Print",
			tooltip : "Print",
			back : "Back"
		},
		resultWin : {
			title : "Print"
		}
	},
	qrcode : {
		bundleName : "QRCode",
		bundleDescription : "This bundle provides a service that can be used to create QR codes.",
		errorMessage : "QR Code can not be generated."
	},
	splashscreen : {
		bundleName : "Splash Screen",
		bundleDescription : "The Splash Screen shows a progress bar and a logo (e.g. the map.apps logo) and/or a text while starting the application.",
		loadTitle : "Starting Application '{appName}'",
		loadBundle : "{percent}% - {startedBundles}/{maxBundlesToStart} - Load {name} "
	},
	system : {
		bundleName : "System",
		bundleDescription : "--CORE FUNCTIONALITY-- The System describes the basic core functionality (compilable skeleton) of map.apps."
	},
	templatelayout : {
		bundleName : "Template Layout",
		bundleDescription : "--CORE FUNCTIONALITY-- The Template Layout implements the arrangement of all UI elements based on predefined templates."
	},
	templates : {
		bundleName : "View",
		bundleDescription : "--CORE FUNCTIONALITY-- Views are used by the Template Layout.",
		templates : {
			desktop : {
				title : "Desktop",
				desc : "The desktop style layout"
			},
			modern : {
				title : "Modern",
				desc : "A modern layout"
			},
			minimal : {
				title : "Minimal",
				desc : "A minimal layout"
			}
		},
		ui : {
			selectorLabelTitle : "View"
		}
	},
	themes : {
		bundleName : "Style",
		bundleDescription : "--CORE FUNCTIONALITY-- The Themes bundle manages all CSS information like colors, font styles, background images etc. Users can switch between different looks of the user interface elements by selecting different styles.",
		themes : {
			pure : {
				desc : "The map.apps 'pure' style."
			},
			night : {
				desc : "The map.apps 'night' style."
			}
		},
		ui : {
			selectorLabelTitle : "Style"
		}
	},
	windowmanager : {
		bundleName : "Window Manager",
		bundleDescription : "--CORE FUNCTIONALITY-- The Window Manager is responsible to manage all floating windows.",
		ui : {
			defaultWindowTitle : "Window",
			closeBtn : {
				title : "Close"
			},
			minimizeBtn : {
				title : "Minimize"
			},
			maximizeBtn : {
				title : "Maximize"
			},
			restoreBtn : {
				title : "Restore"
			},
			opacityBtn : {
				title : "Change transparency"
			},
			collapseBtn : {
				title : "Hide content"
			},
			loading : {
				title : "Please wait!",
				message : "Loading..."
			},
			okcancel : {
				title : "Question",
				okButton : "OK",
				cancelButton : "Cancel"
			}
		}
	},
	ec_legend : {
		bundleName : "Selite",
		bundleDescription : "This bundle provides the Esri legend.",
		tool : {
			title : "Selite",
			tooltip : "Selite"
		}
	},
	ec_parameter_interceptor : {
		bundleName : "ec-parameter-interceptor",
		bundleDescription : "ec-parameter-interceptor",
		errors : {
			missingQueryParameter : "Missing query parameters",
			missingTypeQueryParameter : "Missing type query parameter",
			missingDatasetOrUrlQueryParameter : "Missing dataset or url query parameter",
			noComponentContextProvided : "No componentContext provided",
			noBundleContextProvided : "No bundleContext provided",
			noQueryParametersProvided : "No query parameters provided",
			datasetTypeNotSupported : "The type '${type}' is not supported",
			invalidGeoJSON : "Invalid GeoJSON",
			unsupportedCRSType : "Only named CRS are supported",
			canNotParseNamedCRS : "Can not parse named CRS to WKID: ${name}",
			mixingOfDifferentCRS : "Mixing of different CRS is not supported",
			invalidGeoJSONType : "Unknown GeoJSON type attribute: ${type}",
			typeNotFoundInResource : "No appropriate format for ${type} found in resource",
			couldNotLoadDataset : "Could not load dataset: ${cause}",
			unableToAddDataset : "Unable to add dataset: ${cause}",
			unableToParseCapabilities : "Unable to parse WMS Capabilities response",
			canNotDetermineUrl : "Could not determine the download URL of the resource"
		}
	},
	ec_feature_info : {
		tool : {
			title : "Kohteen tiedot",
			tooltip : "Kohteen tiedot"
		}
	},
	ec_map_loading_screen : {
		bundleName : "ec-map-loading-screen",
		bundleDescription : "ec-map-loading-screen",
		messages : {
			initializingMap : "Karttaa ladataan..."
		}
	},
	ec_wms_layer_selector : {
		bundleName : "ec-wms-layer-selector",
		bundleDescription : "ec-wms-layer-selector",
		ui : {
			windowTitle : "Valitse kaikki",
			hint : "Valittu WMS-palvelu sis\u00E4lt\u00E4\u00E4 ${countToProvideSelection} kerroksen, valitse kaikki kerrokset, joiden tulee n\u00E4ky\u00E4 kartalla.",
			selectAll : "Valitse kaikki",
			closeButton : "OK"
		},
		tool : {
			title : "Kerros",
			tooltip : "Kerros"
		}
	},
	ec_error_messages : {
		bundleName : "ec-error-messages",
		bundleDescription : "ec-error-messages",
		general : {
			serviceNotAvailable : "Pyydetty palvelu ei ole k\u00E4ytett\u00E4viss\u00E4",
			serviceNotSupported : "Pyydetty palvelu ei tueta",
			invalidResource : "Annetusta verkko-osoitteesta l\u00F6ytyi virheellinen resurssi",
			unsupportedResource : "Annetusta verkko-osoitteesta l\u00F6ytyi resurssi, jota ei tueta",
			errorSource : "Seuraava virhel\u00E4hde m\u00E4\u00E4riteltiin",
			requestedUrl : "Pyydetty URL",
			unsupportedServiceType : "Tarjotun palvelun ei tueta",
			technicalSupport : "Jos ongelma jatkuu, ota yhteytt\u00E4 tekniseen tukeen",
			technicalSupportCreateTicket : "Create support ticket",
			weWillContactProvider : "Otamme yhteytt\u00E4 palveluntarjoajaan ja raportoimme ongelman.",
			closeButton : "OK"
		},
		detailedErrors : {
			crs : "Palvelu ei tarjoa tuettua koordinaattij\u00E4rjestelm\u00E4\u00E4",
			encoding : "Palvelu ei tarjoa tuettua merkkikoodausta",
			metadata : "Palvelun metadata-asiakirja on virheellinen",
			fileFormat : "Annetusta verkko-osoitteesta l\u00F6ytyi virheellinen tietomuoto",
			dataRequest : "Palvelun datan haussa saatiin virheellinen vastaus. Mahdollisia syit\u00E4 voivat olla esimerkiksi sis\u00E4inen palvelinvirhe, palvelun v\u00E4\u00E4r\u00E4 kokoonpano, palvelun standardista poikkeava k\u00E4ytt\u00E4ytyminen jne."
		},
		httpIssues : {
			303 : "Vastaus pyynt\u00F6\u00F6n l\u00F6ytyy toisen URI:n alta",
			305 : "Pyydetty resurssi on k\u00E4ytett\u00E4viss\u00E4 vain v\u00E4lityspalvelimen kautta",
			400 : "Palvelin ei pysty tai voi prosessoida pyynt\u00F6\u00E4 havaitun asiakasvirheen takia",
			401 : "Pyydetty resurssi vaati oikeuksien vahvistamisen",
			403 : "Pyydetty resurssi ei ole julkisesti k\u00E4ytett\u00E4viss\u00E4",
			404 : "Pyydetty\u00E4 resurssia ei l\u00F6ydy",
			405 : "Resurssia pyydettiin HTTP-metodin kautta, joka ei ole sallittu",
			406 : "Resurssilta pyydettiin sis\u00E4lt\u00F6\u00E4, joka ei ole hyv\u00E4ksytt\u00E4v\u00E4\u00E4",
			407 : "Resurssi vaatii k\u00E4ytt\u00F6oikeuden tarkistamisen etuk\u00E4teen v\u00E4lityspalvelussa",
			500 : "Resurssi ei ole k\u00E4ytett\u00E4viss\u00E4 sis\u00E4isen palvelinvirheen takia",
			501 : "Resurssi ei ole k\u00E4ytett\u00E4viss\u00E4 tunnistamattoman pyynt\u00F6tavan takia",
			502 : "Resurssi ei ole k\u00E4ytett\u00E4viss\u00E4 virheellisesti m\u00E4\u00E4ritetyn yhdysk\u00E4yt\u00E4v\u00E4n takia",
			503 : "Resurssi ei ole nyt k\u00E4ytett\u00E4viss\u00E4 ylikuormituksen tai huollon takia",
			504 : "Resurssi ei ole k\u00E4ytett\u00E4viss\u00E4 virheellisesti m\u00E4\u00E4ritetyn yhdysk\u00E4yt\u00E4v\u00E4n takia tai yhdysk\u00E4yt\u00E4v\u00E4 ei vastaa",
			505 : "Resurssi ei tue t\u00E4t\u00E4 HTTP-versiota",
			generic : "T\u00E4t\u00E4 resurssia pyydett\u00E4ess\u00E4 tapahtui palvelinvirhe"
		}
	},
	ec_user_tutorial : {
		bundleName : "ec-user-tutorial",
		bundleDescription : "ec-user-tutorial",
		externalResourceDisclaimer : {
			introduction : "Karttakatseluohjelma k\u00E4ytt\u00E4\u00E4 ulkoisia palveluja ja n\u00E4ytt\u00E4\u00E4 niiden datan. Euroopan komissio ei yll\u00E4pid\u00E4 n\u00E4it\u00E4 ulkoisia palveluja, emmek\u00E4 siten pysty vaikuttamaan niiden k\u00E4ytett\u00E4vyyteen/luotettavuuteen. Seuraavia ongelmia saattaa ilmet\u00E4:",
			rotatingCircle : "Py\u00F6riv\u00E4 ympyr\u00E4 karttan\u00E4kym\u00E4ss\u00E4 osoittaa, ett\u00E4 katseluohjelma odottaa vastausta datal\u00E4hteest\u00E4. Syyn\u00E4 voi olla, ettei palvelin ole k\u00E4ytett\u00E4viss\u00E4 tai verkossa on ongelmia.",
			layerZoomLevel : "Jotkut kerrosl\u00E4hteet eiv\u00E4t ehk\u00E4 n\u00E4y tietyill\u00E4 zoomaustasoilla. T\u00E4m\u00E4 johtuu ulkoisen palveluntarjoajan asettamista rajoituksista. Sinun tarvitsee ehk\u00E4 l\u00E4hent\u00E4\u00E4 tai loitontaa n\u00E4hd\u00E4ksesi kerroksen datan.",
			layerPanning : "Ulkoiselta palvelulta k\u00E4ytetty data saattaa sijaita nykyisen karttan\u00E4kym\u00E4n ulkopuolella. T\u00E4m\u00E4 karttakatseluohjelma yritt\u00E4\u00E4 automaattisesti tunnistaa katetun alueen. Joissakin tapauksissa metadata on puutteellinen tai virheellinen, jolloin on paneroitava oikeaan laajuuteen.",
			detailInformation : "Jos pyyd\u00E4t tarkkaa tietoa valitulle kohteelle, se hoidetaan erikoispyynn\u00F6ll\u00E4 karttapalvelimelle. Jos vastaus t\u00E4h\u00E4n pyynt\u00F6\u00F6n on v\u00E4\u00E4rin muotoiltu tai tyhj\u00E4 (esim. palvelinvirheen takia), tietoa ei voi n\u00E4ytt\u00E4\u00E4 ja ikkuna n\u00E4kyy tyhj\u00E4n\u00E4.",
			mapScaling : "Euroopan unionin tilastotoimisto (Eurostat) tarjoaa t\u00E4m\u00E4n sovelluksen pohjakartat. Kartat ovat t\u00E4ll\u00E4 hetkell\u00E4 saatavana korkeintaan resoluutiossa 1:50 000. Yksityiskohtaisemmat pohjakartat ovat ty\u00F6n alla (esim. tukemaan datan visualisointia kaupunkialueella). N\u00E4in ollen tiettyj\u00E4 datal\u00E4hteit\u00E4 ei ehk\u00E4 viel\u00E4 n\u00E4ytet\u00E4 optimaalisella taustakartalla.",
			technicalSupport : "Jos kohtaat muita ongelmia, \u00E4l\u00E4 ep\u00E4r\u00F6i ottaa yhteytt\u00E4 tekniseen tukeen:",
			technicalSupportContactLink : "Ota yhteytt\u00E4 tekniseen tukeen",
			mapLoading : "Karttaa ladataan...",
			donotshow : "\u00C4l\u00E4 n\u00E4yt\u00E4 t\u00E4t\u00E4 tervetulon\u00E4ytt\u00F6\u00E4 uudestaan."
		},
		legalNotice : "T\u00E4ss\u00E4 kartassa k\u00E4ytetyt nime\u00E4miset ja aineiston esitys eiv\u00E4t viittaa mink\u00E4\u00E4nlaiseen mielipiteen ilmaisuun Euroopan unionin taholta koskien mink\u00E4\u00E4n maan, territorion, kaupungin tai alueen tai sen viranomaisten laillista statusta, tai sen rajojen m\u00E4\u00E4r\u00E4\u00E4mist\u00E4. Kosovo*: T\u00E4m\u00E4 nime\u00E4minen ei vaikuta asemaa koskeviin kantoihin ja on YK:n turvallisuusneuvoston p\u00E4\u00E4t\u00F6slauselman 1244/1999 ja Kansainv\u00E4lisen tuomioistuimen Kosovon itsen\u00E4isyysjulistuksesta antaman lausunnon mukainen. Palestiina*: T\u00E4t\u00E4 nime\u00E4mist\u00E4 ei tule tulkita Palestiinan valtion tunnustamiseksi eik\u00E4 se rajoita yksitt\u00E4isten j\u00E4senvaltioiden kantoja t\u00E4t\u00E4 asiaa koskien.",
		legalNoticeHeader : "Oikeudellinen huomautus",
		tutorial : {
			welcome : "Tervetuloa Euroopan dataportaalin kartta karttakatseluohjelmaan!",
			familiarise : "T\u00E4m\u00E4n lyhyen johdannon tarkoitus on tutustuttaa sinut karttakatseluohjelman elementteihin ja toiminnallisuuteen.",
			navigation : "Napsauta ja pid\u00E4 hiiren vasenta n\u00E4pp\u00E4int\u00E4 alhaalla panoroidaksesi karttaa. ",
			zoom : "N\u00E4m\u00E4 painikkeet muuttavat kartan zoomaustasoa. Voit my\u00F6s k\u00E4ytt\u00E4\u00E4 hiiren kiekkopainiketta zoomauksen s\u00E4\u00E4t\u00E4miseen.",
			features : "Lis\u00E4toiminnallisuutta l\u00F6ytyy n\u00E4iden painikkeiden takaa. Ne toimivat vaihtopainikkeina ja ottavat toiminnon k\u00E4ytt\u00F6\u00F6n tai poistavat sen k\u00E4yt\u00F6st\u00E4.",
			legend : "Selitteen avulla voi tutkia k\u00E4yt\u00F6ss\u00E4 olevia karttakerroksia ja ottaa k\u00E4ytt\u00F6\u00F6n tai poistaa k\u00E4yt\u00F6st\u00E4 niiden n\u00E4yt\u00F6n kartalla. Nimet on johdettu suoraan ulkoisesti k\u00E4ytetyst\u00E4 palvelusta.",
			transparency : "Voit my\u00F6s s\u00E4\u00E4t\u00E4\u00E4 kerroksen l\u00E4pin\u00E4kyvyyden.",
			featureinfo : "Joitakin tietoja voi tutkia viel\u00E4 yksityiskohtaisemmin. Kun kohde on aktivoitu, voit napsauttaa karttaa lis\u00E4tietojen kyselemiseksi.",
			done : "Valmis",
			okButton : "OK",
			closeButton : "Sulje",
			skip : "Ohita",
			next : "Seuraava",
			back : "Edellinen"
		}
	}
});