#!/usr/bin/env node


function createBundleFiles(defaultLang, additionalLangs, bundles, callback) {
    var fs = require('fs'), opts = {
        encoding: 'utf8'
    };

    function define(what) {
        return 'define(' + JSON.stringify(what, null, 2) + ');';
    }

    function errorHandler(err){
        if (err) { callback(err); }
    }


    function readDirectory(lang, callback) {
        var i18n = {};
        bundles.forEach(function (bundle) {
            var file = lang + '/bundles/' + bundle + '.json';
            var content = fs.readFileSync(file, opts);
            try {
                i18n[bundle] = JSON.parse(content);
            } catch (err) {
                callback(err);
            }
        });
        
        callback(null, i18n);
    }

    readDirectory(defaultLang, function (err, content) {
        if (err) {
            callback(err);
        } else {
            content = {
                root: content
            };
            additionalLangs.forEach(function (lang) {
                content[lang] = true;
            });
            console.info('INFO: Writing bundle.js');
            fs.writeFile('bundle.js', define(content), opts, errorHandler);
        }
    });

    additionalLangs.forEach(function (lang) {
        readDirectory(lang, function (err, content) {
            if (err) {
                callback(err);
            } else {
                console.info('INFO: Writing ' + lang + '/bundle.js');
                fs.writeFile(lang + '/bundle.js', define(content), opts, errorHandler);
            }
        });
    });

}


createBundleFiles('en', [
//    'bg',
//    'cs',
//    'da',
    'de',
//    'el',
    'es',
//    'et',
//    'fi',
    'fr',
//    'ga',
//    'hr',
//    'hu',
    'it',
//    'lt',
//    'lv',
//    'mt',
//    'nl',
    'pl'
//    'pt',
//    'ro',
//    'sk',
//    'sl',
//    'sv'
], [
//  'aceeditor',
//  'addthis',
//  'addthis-config',
//  'agolauthn',
//  'agolmapfinder',
//  'agolmapfinder-config',
//  'agsprinting',
//  'agsprinting-config',
//  'agssearch',
//  'agssearch-config',
//  'appautorefresh',
//  'appmanagement',
//  'appsoverview',
//  'appsstore',
//  'authentication',
//  'basemapgallery',
//  'basemaptoggler',
//  'bookmarks',
//  'bookmarks-config',
//  'bundlemanagement',
//  'bundletools',
//  'compositesearch',
//  'console',
    'contentviewer',
//  'contentviewer-config',
//  'coordinatefinder',
    'coordinatetransformer',
//  'coordinateviewer',
//  'coordinateviewer-config',
//  'custominfo',
//  'custominfo-config',
//  'dataform',
//  'editing',
//  'editing-config',
    'featureinfo',
//  'filesaver',
//  'followme',
//  'forcelogin',
    'geometryservice',
//  'geonames',
//  'geonames-config',
    'geojson',
//  'georss',
//  'googlemaps',
//  'gpsgate',
//  'infoservice',
    'infoviewer',
//  'integration',
//  'joinaddress',
    'languagetoggler',
//  'legend',
//  'legend-config',
//  'locateme',
//  'locateme-config',
//  'locationfinder',
//  'locationfinder-config',
//  'locator',
//  'locator-config',
//  'magnifier',
//  'magnifier-config',
    'map',
//  'map-config',
//  'mapdesk',
//  'mapflow',
//  'mapflow-config',
//  'maptip',
//  'measurement',
//  'measurement-config',
    'mobiletoc',
    'mobileview',
//  'navigationtoolset',
    'notifier',
//  'omnisearch',
//  'omnisearch-config',
//  'overviewmap',
//  'overviewmap-config',
    'parametermanager',
//  'parametermanager-config',
    'printing',
//  'printing-config',
    'qrcode',
//  'ractive',
//  'redlining',
//  'redlining-config',
//  'remotechannels',
//  'reportmanagement',
//  'reportstore',
//  'reporttool',
//  'reporttool-config',
//  'resultcenter',
//  'routing',
//  'routing-config',
//  'scalebar',
//  'scalebar-config',
//  'selection',
    'splashscreen',
//  'streetview',
//  'streetview-config',
    'system',
//  'tableofcontents',
    'templatelayout',
    'templates',
//  'templates-config',
    'themes',
//  'themes-config',
//  'tilejson',
//  'toolrules',
//  'toolset',
//  'toolset-config',
//  'undoredo',
//  'webmap',
//  'webmap-config',
//  'webtiled',
    'windowmanager',
//  'wizard',
    'ec_legend',
    'ec_parameter_interceptor',
    'ec_feature_info',
    'ec_map_loading_screen',
    'ec_wms_layer_selector',
    'ec_error_messages',
    'ec_user_tutorial'
], function(err) {
    console.error(err);
});