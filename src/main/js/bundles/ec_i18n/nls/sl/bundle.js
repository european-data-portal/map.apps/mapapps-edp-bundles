define({
	contentviewer : {
		bundleName : "Viewer vsebina",
		bundleDescription : "Prikazuje razli\u010dne vrste vsebin.",
		ui : {
			unknownContentError : "Vsebina ni znana.",
			graphicinfotool : {
				title : "Podatki element",
				desc : "Podatki element"
			},
			content : {
				defaultTitle : "Element",
				grid : {
					detailView : "Prika\u017ei podrobnosti",
					key : "Nepremi\u010dnine",
					value : "Vrednost"
				},
				customTemplate : {
					detailView : "Prika\u017ei podrobnosti"
				},
				attachment : {
					noAttachmentSupport : "Ta plast ne podpira priklju\u010dki",
					detailView : "Prika\u017ei podrobnosti"
				},
				AGSDetail : {
					title : "Poglej podrobnosti",
					print : "Natisni",
					serviceMetadata : "Storitev metapodatki",
					serviceURL : "URL",
					serviceCopyrights : "Avtorske pravice",
					serviceTitle : "Naslov",
					pager : {
						pageSizeLabel : "Funkcija ${currentPage} od ${endPage}"
					},
					key : "Nepremi\u010dnine",
					value : "Vrednost",
					detailView : "Prika\u017ei podrobnosti"
				}
			}
		}
	},
	featureinfo : {
		bundleName : "FeatureInfo",
		bundleDescription : "FeatureInfo prika\u017ee informacije o mo\u017enosti za aktivne plasti.",
		ui : {
			featureInfo : {
				noQueryLayersFound : "Ne iskati plasti voljo!",
				contentInfoWindowTitle : "Prepoznajte",
				noResultsFound : "Ni najdenih rezultatov.",
				loadingInfoText : "Poizvedovanje plasti ${layerName} (${layerIndex} za ${layersTotal}).",
				featureInfoTool : "Prepoznajte",
				layer : "Layer",
				feature : "Predmet"
			},
			wms : {
				emptyResult : 'Ni najdenih na WMS sloja "${layerTitle}" rezultatov.'
			}
		}
	},
	geometryservice : {
		bundleName : "GeometryService",
		bundleDescription : "Snop zagotavlja osrednjo geometrije storitev."
	},
	coordinatetransformer : {
		bundleName : "Usklajuje transformator",
		bundleDescription : "--OSNOVNO FUNKCIONALNOST-- Usklajuje transformatorja transformira geometrije iz enega koordinatnega sistema v drugega."
	},
	geojson : {
		bundleName : "GeoJSON",
		bundleDescription : "Ta paket zagotavlja storitev pretvorbe, ki se lahko obdelajo GeoJSON in znano besedilo (Wkt)."
	},
	infoviewer : {
		bundleName : "Prikaz informacij",
		bundleDescription : "Na zaslonu se izpi\u0161e uporabni\u0161ko vsebinsko podatke o kraju, na karti v oknu.",
		ui : {
			title : "Info Viewer",
			tools : {
				closeInfoWindowMapdeskTool : "Zapri",
				closeInfoWindowMapTool : "Zapri",
				focusMapTool : "Sredi\u0161\u010de na zemljevidu",
				attachToGeorefTool : "Pritrdite na polo\u017eaju",
				mainActivationTool : "Lokacija Informacije"
			}
		}
	},
	languagetoggler : {
		bundleName : "Jezik preklopnik",
		bundleDescription : "Prek vmesnika jeziku jezik preklopnik je mogo\u010de spremeniti.",
		ui : {
			title : "Language"
		}
	},
	map : {
		bundleName : "Map",
		bundleDescription : "Snop Map upravlja glavno zemljevid in vse informacije map vsebine. Stranka lahko povzetek storitve, ki se uporabljajo v hierarhi\u010dni model zemljevidov, tako da se lahko prika\u017ee na razli\u010dne na\u010dine za uporabnika.",
		ui : {
			nodeUpdateError : {
				info : 'Posodobitev storitev "${title}" ni uspelo! Napaka: ${errorMsg}',
				detail : 'Posodobitev storitev "${title}" ni uspelo! Napaka: ${url} ${errorMsg} - Podrobnosti: ${error}'
			},
			sliderLabels : {
				country : "Dr\u017eava",
				region : "Regija",
				town : "Kraj"
			}
		},
		drawTooltips : {
			addPoint : "Kliknite na zemljevid",
			addMultipoint : "Kliknite za za\u010detek",
			finishLabel : "Kon\u010da"
		}
	},
	mobiletoc : {
		bundleName : "Nadzor mobilni raven",
		bundleDescription : "Ta paket zagotavlja krmilnik mobilni raven pripravljen",
		tool : {
			title : "Zemljevid vsebina",
			tooltip : "Vklop / izklop Zemljevid vsebine"
		},
		ui : {
			basemaps : "Navadne karte",
			operationalLayers : "Ravni Teme",
			switchUI : {
				noTitle : "brez naslova",
				leftLabel : "Na",
				rightLabel : "Off"
			}
		}
	},
	mobileview : {
		bundleName : "Mobilni pogled",
		bundleDescription : "Vsebuje posebne razrede, ki pritrjujejo nekaj vpra\u0161anj v zvezi z dojox.mobile"
	},
	parametermanager : {
		bundleName : "Ravnanje parameter",
		bundleDescription : "Parameter Manager je odgovoren, da prenese parametre iz URL-ja za po komponent za zagon.",
		ui : {
			encoderBtn : "Link orodje",
			encoderBtnTooltip : "Link orodje",
			sendMail : "E-mail",
			refresh : "Osve\u017eitev",
			linkBoxTitle : "Link URL",
			size : "Velikost (pike)",
			codeBoxTitle : "Koda vlagati v HTML",
			qrCode : "QRCode",
			mailBody : "${url}",
			mailSubject : "Poglej \u010dasih to kartico!",
			options : {
				small : "majhna (480 x 320)",
				medium : "srednja (640 x 480)",
				large : "velika (1280 x 1024)",
				custom : "uporabni\u0161ko definirane"
			}
		}
	},
	notifier : {
		bundleName : "Obvestila",
		bundleDescription : "Vsa stanja, napredek ali sporo\u010dila o napakah so prikazana uporabniku v sporo\u010dilu pop-up, tako da je jasno, kaj se dogaja v uporabi.",
		ui : {
			title : "Notifier",
			tooltips : {
				close : "Zapri Sporo\u010dilo",
				glue : "Prilo\u017eite sporo\u010dilo"
			}
		}
	},
	printing : {
		bundleName : "Natisni",
		bundleDescription : "Ta paket zagotavlja tiskanja orodje za tiskanje zemljevida.",
		tool : {
			title : "Natisni",
			tooltip : "Natisni",
			back : "Nazaj"
		},
		resultWin : {
			title : "Natisni"
		}
	},
	qrcode : {
		bundleName : "QRCode",
		bundleDescription : "Ta paket zagotavlja storitev, ki se lahko uporablja za ustvarjanje QR kode.",
		errorMessage : "Koda QR generacija ni bila uspe\u0161na."
	},
	splashscreen : {
		bundleName : "Doma\u010di zaslon",
		bundleDescription : "Za\u010detnem zaslonu se prika\u017ee vrstica napredka, medtem ko je vloga za\u010delo.",
		loadTitle : 'Za\u017eenite aplikacijo "{appName}"',
		loadBundle : "{percent}% - {startedBundles}/{maxBundlesToStart} - Polnjenje {name}"
	},
	templatelayout : {
		bundleName : "Poglej Layout",
		bundleDescription : "--OSNOVNO FUNKCIONALNOST-- Template Layout izvaja ureditev vseh elementi uporabni\u0161kega vmesnika, ki temelji na vnaprej dolo\u010denih predlog."
	},
	system : {
		bundleName : "Sistem",
		bundleDescription : "--OSNOVNO FUNKCIONALNOST-- Sistem opisuje osnovno osnovno funkcionalnost (mogo\u010de prevesti skelet) z map.apps."
	},
	templates : {
		bundleName : "Poglej",
		bundleDescription : "--OSNOVNO FUNKCIONALNOST-- Views jih Postavitev Predloga uporablja.",
		templates : {
			desktop : {
				title : "Desktop",
				desc : "Postavitev desktop slog"
			},
			modern : {
				title : "Modern",
				desc : "Moderen layout."
			},
			minimal : {
				title : "Minimalna",
				desc : "Minimalisti\u010dno postavitev"
			}
		},
		ui : {
			selectorLabelTitle : "Poglej"
		}
	},
	themes : {
		bundleName : "Style",
		bundleDescription : "--OSNOVNO FUNKCIONALNOST-- Teme snop upravlja vse informacije, CSS, kot so barve, pisave stilov, slik ozadja itd Uporabniki lahko preklapljate med razli\u010dnimi izgled elementov uporabni\u0161kega vmesnika z izbiro razli\u010dnih stilov.",
		themes : {
			pure : {
				desc : '"\u010cista" slog map.apps.'
			},
			night : {
				desc : 'Map.apps "no\u010d" slog.'
			}
		},
		ui : {
			selectorLabelTitle : "Style"
		}
	},
	ec_legend : {
		bundleName : "Legend",
		bundleDescription : "Ta paket zagotavlja ESRI legendo.",
		tool : {
			title : "Legend",
			tooltip : "Legend"
		}
	},
	windowmanager : {
		bundleName : "Okno za upravljanje",
		bundleDescription : "---OSNOVNO FUNKCIONALNOST-- Upravljalnik oken je odgovoren za upravljanje vseh pogovorno okno.",
		ui : {
			defaultWindowTitle : "Okno",
			closeBtn : {
				title : "Zapri"
			},
			minimizeBtn : {
				title : "\u010cim bolj zmanj\u0161ati"
			},
			maximizeBtn : {
				title : "Maksimiranje"
			},
			restoreBtn : {
				title : "Obnovitev"
			},
			opacityBtn : {
				title : "Spremeni preglednost"
			},
			collapseBtn : {
				title : "Skrij vsebino"
			},
			loading : {
				title : "Prosimo po\u010dakajte!",
				message : "Nalaganje ..."
			},
			okcancel : {
				title : "Vpra\u0161anje",
				okButton : "Ok",
				cancelButton : "Prekli\u010di"
			}
		}
	},
	ec_parameter_interceptor : {
		bundleName : "ec-parameter-interceptor",
		bundleDescription : "ec-parameter-interceptor",
		errors : {
			missingQueryParameter : "Missing query parameters",
			missingTypeQueryParameter : "Missing type query parameter",
			missingDatasetOrUrlQueryParameter : "Missing dataset or url query parameter",
			noComponentContextProvided : "No componentContext provided",
			noBundleContextProvided : "No bundleContext provided",
			noQueryParametersProvided : "No query parameters provided",
			datasetTypeNotSupported : "The type '${type}' is not supported",
			invalidGeoJSON : "Invalid GeoJSON",
			unsupportedCRSType : "Only named CRS are supported",
			canNotParseNamedCRS : "Can not parse named CRS to WKID: ${name}",
			mixingOfDifferentCRS : "Mixing of different CRS is not supported",
			invalidGeoJSONType : "Unknown GeoJSON type attribute: ${type}",
			typeNotFoundInResource : "No appropriate format for ${type} found in resource",
			couldNotLoadDataset : "Could not load dataset: ${cause}",
			unableToAddDataset : "Unable to add dataset: ${cause}",
			unableToParseCapabilities : "Unable to parse WMS Capabilities response",
			canNotDetermineUrl : "Could not determine the download URL of the resource"
		}
	},
	ec_feature_info : {
		tool : {
			title : "Informacije o funkciji",
			tooltip : "Informacije o funkciji"
		}
	},
	ec_map_loading_screen : {
		bundleName : "ec-map-loading-screen",
		bundleDescription : "ec-map-loading-screen",
		messages : {
			initializingMap : "Zemljevid se inicializira. Prosimo, po\u010Dakajte!"
		}
	},
	ec_wms_layer_selector : {
		bundleName : "ec-wms-layer-selector",
		bundleDescription : "ec-wms-layer-selector",
		ui : {
			windowTitle : "Izberi sloje",
			hint : "Izbrana storitev WMS vsebuje ${countToProvideSelection} sloj(-a/-e/-ev). Izberite vse sloje, ki se morajo prikazati na zemljevidu.",
			selectAll : "Izberi vse",
			closeButton : "V REDU"
		},
		tool : {
			title : "Sloj",
			tooltip : "Sloj"
		}
	},
	ec_error_messages : {
		bundleName : "ec-error-messages",
		bundleDescription : "ec-error-messages",
		general : {
			serviceNotAvailable : "Zahtevana storitev ni na voljo",
			serviceNotSupported : "Zahtevana storitev ni podprta",
			invalidResource : "Na tem naslovu URL je bil odkrit neveljaven vir",
			unsupportedResource : "Na tem naslovu URL je bil odkrit nepodprt vir",
			errorSource : "Ugotovljen je bil naslednji vir napake",
			requestedUrl : "Zahtevani URL",
			unsupportedServiceType : "The provided service type is not supported",
			technicalSupport : "\u010Ce te\u017Eava ni odpravljena, se obrnite na tehni\u010Dno podporo",
			technicalSupportCreateTicket : "Create support ticket",
			weWillContactProvider : "We will contact the provider of the service and report the issue.",
			closeButton : "V REDU"
		},
		detailedErrors : {
			crs : "Storitev ne omogo\u010Da podprtega usklajenega referen\u010Dnega sistema",
			encoding : "Storitev ne omogo\u010Da podprtega kodiranja znakov",
			metadata : "Dokument storitve z metapodatki vsebuje napake",
			fileFormat : "Na tem naslovu URL je bila prejeta neveljavna oblika zapisa podatkov",
			dataRequest : "Med poizvedbo po podatkih storitve je bil prejet neveljaven odgovor. Razlogi za to so lahko na primer: notranja napaka na stre\u017Eniku, napa\u010Dna konfiguracija storitve, s standardi neusklajeno vedenje storitve, itd."
		},
		httpIssues : {
			303 : "Odgovor na zahtevo je na voljo na drugem URI",
			305 : "Zahtevani vir je na voljo samo preko posredni\u0161kega stre\u017Enika",
			400 : "Stre\u017Enik ne more ali ne \u017Eeli obdelati zahteve zaradi domnevne napake pri odjemalcu",
			401 : "Zahtevani vir je treba avtorizirati",
			403 : "Zahtevani vir ni javno na voljo",
			404 : "Zahtevanega vira ni bilo mogo\u010De najti",
			405 : "Vir je bil zahtevan z nedovoljeno metodo HTTP",
			406 : "Zahtevana je bila vsebina iz vira, ki ni sprejemljiva",
			407 : "Vir zahteva vnaprej\u0161njo avtentifikacijo v posredni\u0161ki storitvi",
			500 : "Vir ni na voljo zaradi notranje napake na stre\u017Eniku",
			501 : "Vir ni na voljo zaradi neznanega na\u010Dina vlo\u017Eitve zahteve",
			502 : "Vir ni na voljo zaradi napa\u010Dno konfiguriranega prehoda",
			503 : "Vir trenutno ni na voljo zaradi preobremenitve ali vzdr\u017Eevanja",
			504 : "Vir ni na voljo zaradi napa\u010Dno konfiguriranega ali neodzivnega prehoda",
			505 : "Vir ne podpira dane razli\u010Dice HTTP",
			generic : "pri zahtevi danega vira je pri\u0161lo do napake na stre\u017Eniku"
		}
	},
	ec_user_tutorial : {
		bundleName : "ec-user-tutorial",
		bundleDescription : "ec-user-tutorial",
		externalResourceDisclaimer : {
			introduction : "Ta pregledovalnik zemljevida ima dostop do zunanjih storitev in prikazuje njihove podatke. Evropska komisija teh zunanjih storitev ne vzdr\u017Euje , zato nimamo nikakr\u0161nega vpliva na njihovo razpolo\u017Eljivost/stalnost. Pojavijo se lahko naslednje te\u017Eave:",
			rotatingCircle : "Vrte\u010Di se krog v pogledu zemljevida pomeni, da pregledovalnik \u010Daka na odgovor vira podatkov. Vzrok za to je lahko nedostopen stre\u017Enik ali te\u017Eave v omre\u017Eju.",
			layerZoomLevel : "Nekateri viri slojev se morda ne prika\u017Eejo na dolo\u010Denih ravneh pove\u010Dave. To je posledica omejitev, ki jih dolo\u010Di ponudnik zunanje storitve. Morda boste morali prikaz pove\u010Dati ali zmanj\u0161ati, da se prika\u017Eejo podatki v sloju.",
			layerPanning : "Podatki, do katerih je bil dostop mogo\u010D iz zunanjega stre\u017Enika, so lahko zunaj trenutnega pogleda zemljevida. Ta pregledovalnik zemljevida posku\u0161a samodejno identificirati zajeto obmo\u010Dje. Kljub temu so metapodatki v nekaterih primerih nepopolni ali napa\u010Dni, zato se morate z mi\u0161ko premikati do dejanskega obsega.",
			detailInformation : "\u010De \u017Eelite podrobne informacije za izbrano funkcijo, se to obravnava na podlagi posebnega zahtevka, ki ga po\u0161ljete v stre\u017Enik zemljevida. \u010Ce je odgovor na ta zahtevek nepravilno oblikovan ali prazen (npr. zaradi napake na stre\u017Eniku), se informacije ne morejo prikazati in pojavi se prazno okno.",
			mapScaling : "Osnovne zemljevide, ki se uporabljajo v tej aplikaciji, je prispeval Statisti\u010Dni urad Evropske unije (Eurostat). Ti zemljevidi so trenutno na voljo v lo\u010Dljivosti do najve\u010D 1:50\u00A0000. Natan\u010Dnej\u0161i osnovni zemljevidi so v pripravi (npr. da bi se podprla vizualizacija podatkov znotraj mesta). Nekateri viri podatkov se zato morda ne bodo \u0161e prikazali z optimalno prilagojenim zemljevidom ozadja.",
			technicalSupport : "\u010Ce naletite na druge te\u017Eave, se obrnite na tehni\u010Dno podporo:",
			technicalSupportContactLink : "Kontaktirajte tehni\u010Dno podporo",
			mapLoading : "Zemljevid se nalaga ...",
			donotshow : "Ne prika\u017Ei ve\u010D tega uvodnega zaslona."
		},
		legalNotice : "Uporabljena imenovanja in predstavitev gradiva na tem zemljevidu ne izra\u017Ea kakr\u0161nega koli stali\u0161\u010Da Evropske unije v zvezi s pravnim statusom dr\u017Eav, ozemelj, mest ali obmo\u010Dij oziroma njihovih organov oblasti ali v zvezi z njihovo razmejitvijo. Kosovo*: to imenovanje ne posega v stali\u0161\u010Da do statusa in je v skladu z RVSZN 1244/1999 ter mnenjem Meddr\u017Eavnega sodi\u0161\u010Da o razglasitvi neodvisnosti Kosova. Palestina*: to imenovanje ne pomeni priznanja dr\u017Eave Palestine in ne posega v stali\u0161\u010Da posameznih dr\u017Eav \u010Dlanic o tem vpra\u0161anju.",
		legalNoticeHeader : "Izjava o omejitvi odgovornosti",
		tutorial : {
			welcome : "Dobrodo\u0161li v pregledovalniku zemljevida na Evropskem podatkovnem portalu",
			familiarise : "Cilj tega kratkega uvoda je, da vas seznani z elementi in funkcijami pregledovalnika zemljevida.",
			navigation : "Za pomikanje po zemljevidu kliknite in dr\u017Eite levi gumb na mi\u0161ki.",
			zoom : "Ti gumbi spreminjajo raven pove\u010Dave na zemljevidu. Za prilagoditev pove\u010Dave lahko uporabite tudi kole\u0161\u010Dek na mi\u0161ki.",
			features : "Dodatne funkcije so na voljo z uporabo teh gumbov. Ti delujejo kot preklopna stikala ter omogo\u010Dajo ali onemogo\u010Dajo dolo\u010Deno funkcijo.",
			legend : "Uporabite lahko legendo, da bi pregledali razpolo\u017Eljive sloje zemljevida ter omogo\u010Dili ali onemogo\u010Dili njihov trenutni prikaz na zemljevidu. Imena so neposredno izpeljana iz storitve z zunanjim dostopom.",
			transparency : "Prilagodite lahko tudi prosojnost sloja.",
			featureinfo : "Nekatere podatke je mogo\u010De pregledati tudi podrobneje. Ko je ta funkcija aktivirana, za dodatne informacije lahko kliknete na zemljevid.",
			done : "Kon\u010Dano",
			okButton : "V REDU",
			closeButton : "Zapri",
			skip : "Izpusti",
			next : "Naprej",
			back : "Nazaj"
		}
	}
});