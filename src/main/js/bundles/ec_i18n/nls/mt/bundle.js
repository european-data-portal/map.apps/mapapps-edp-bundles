define({
	contentviewer : {
		bundleName : "Content Viewer",
		bundleDescription : "Displays different kinds of content.",
		ui : {
			unknownContentError : "The content is unknown.",
			graphicinfotool : {
				title : "Item Info",
				desc : "Item Info"
			},
			content : {
				defaultTitle : "Item",
				grid : {
					detailView : "Show details",
					key : "Property",
					value : "Value"
				},
				customTemplate : {
					detailView : "Show details"
				},
				attachment : {
					noAttachmentSupport : "This layer does not offer attachment support",
					detailView : "Open detail view"
				},
				AGSDetail : {
					title : "Detail View",
					print : "Print",
					serviceMetadata : "Service metadata",
					serviceURL : "URL",
					serviceCopyrights : "Copyrights",
					serviceTitle : "Title",
					pager : {
						pageSizeLabel : "Feature ${currentPage} of ${endPage}"
					},
					key : "Property",
					value : "Value",
					detailView : "Show details"
				}
			}
		}
	},
	coordinatetransformer : {
		bundleName : "Coordinate Transformer",
		bundleDescription : "--CORE FUNCTIONALITY-- The Coordinate Transformer transforms geometries from a source coordinate system to a target coordinate system."
	},
	featureinfo : {
		bundleName : "FeatureInfo",
		bundleDescription : "FeatureInfo displays information on features for active layers.",
		ui : {
			featureInfo : {
				noQueryLayersFound : "No queryable layers found!",
				contentInfoWindowTitle : "Identify",
				noResultsFound : "No results found.",
				loadingInfoText : "Querying layer ${layerName} (${layerIndex} of ${layersTotal}).",
				featureInfoTool : "Identify",
				layer : "Layer",
				feature : "Feature"
			},
			wms : {
				emptyResult : "No results found on the WMS layer '${layerTitle}'."
			}
		}
	},
	geometryservice : {
		bundleName : "GeometryService",
		bundleDescription : "The bundle provides a central geometry service."
	},
	geojson : {
		bundleName : "GeoJson",
		bundleDescription : "This bundle provides a conversion service for geojson and well-known text formats"
	},
	infoviewer : {
		bundleName : "Info Viewer",
		bundleDescription : "The Info Viewer shows content information for a location in a moveable and resizable window.",
		ui : {
			title : "Info Viewer",
			tools : {
				closeInfoWindowMapdeskTool : "Close",
				closeInfoWindowMapTool : "Close",
				focusMapTool : "Center in Map",
				attachToGeorefTool : "Attach to position",
				mainActivationTool : "Location Information"
			}
		}
	},
	languagetoggler : {
		bundleName : "Language Toggler",
		bundleDescription : "The language of the user interface can be switched by a language toggler.",
		ui : {
			title : "Language"
		}
	},
	map : {
		bundleName : "Map",
		bundleDescription : "The Map bundle manages the main map and all map content information. The client can abstract the services used in a hierarchical Map model so that they can be displayed in various ways to the user.",
		ui : {
			nodeUpdateError : {
				info : "Update of service '${title}' failed! Msg: ${errorMsg}",
				detail : "Update of service '${title}' failed! Msg: ${url} ${errorMsg} - Details: ${error}"
			},
			sliderLabels : {
				country : "Country",
				region : "Region",
				town : "Town"
			}
		},
		drawTooltips : {
			addPoint : "Click in the map",
			addMultipoint : "Click to start",
			finishLabel : "Finish"
		}
	},
	mobiletoc : {
		bundleName : "Mobile TOC",
		bundleDescription : "This bundle provides a mobile table of contents",
		tool : {
			title : "Map Content",
			tooltip : "Turn on/off Map Content"
		},
		ui : {
			basemaps : "Base maps",
			operationalLayers : "Operational layers",
			switchUI : {
				noTitle : "no title",
				leftLabel : "On",
				rightLabel : "Off"
			}
		}
	},
	mobileview : {
		bundleName : "Mobile View",
		bundleDescription : "Contains special classes that fix some issues concerning dojox.mobile"
	},
	notifier : {
		bundleName : "Notifier",
		bundleDescription : "All messages on status, progress, errors and warnings are shown in a popup message so that a user can understand what is going on in the application.",
		ui : {
			title : "Notifier",
			tooltips : {
				close : "Close this message",
				glue : "Pin this message"
			}
		}
	},
	parametermanager : {
		bundleName : "Parameter Manager",
		bundleDescription : "The Parameter Manager is responsible to delegate parameters from the URL to the according components on startup.",
		ui : {
			encoderBtn : "Link tool",
			encoderBtnTooltip : "Link tool",
			sendMail : "EMail",
			refresh : "Refresh",
			linkBoxTitle : "Link URL",
			size : "Size (Pixels)",
			codeBoxTitle : "Code to embed in HTML",
			qrCode : "QRCode",
			mailBody : "${url}",
			mailSubject : "Check out this map!",
			options : {
				small : "small (480 x 320)",
				medium : "medium (640 x 480)",
				large : "large (1280 x 1024)",
				custom : "custom"
			}
		}
	},
	printing : {
		bundleName : "Print",
		bundleDescription : "This bundle provides a print tool to print the map.",
		tool : {
			title : "Print",
			tooltip : "Print",
			back : "Back"
		},
		resultWin : {
			title : "Print"
		}
	},
	qrcode : {
		bundleName : "QRCode",
		bundleDescription : "This bundle provides a service that can be used to create QR codes.",
		errorMessage : "QR Code can not be generated."
	},
	splashscreen : {
		bundleName : "Splash Screen",
		bundleDescription : "The Splash Screen shows a progress bar and a logo (e.g. the map.apps logo) and/or a text while starting the application.",
		loadTitle : "Starting Application '{appName}'",
		loadBundle : "{percent}% - {startedBundles}/{maxBundlesToStart} - Load {name} "
	},
	system : {
		bundleName : "System",
		bundleDescription : "--CORE FUNCTIONALITY-- The System describes the basic core functionality (compilable skeleton) of map.apps."
	},
	templatelayout : {
		bundleName : "Template Layout",
		bundleDescription : "--CORE FUNCTIONALITY-- The Template Layout implements the arrangement of all UI elements based on predefined templates."
	},
	templates : {
		bundleName : "View",
		bundleDescription : "--CORE FUNCTIONALITY-- Views are used by the Template Layout.",
		templates : {
			desktop : {
				title : "Desktop",
				desc : "The desktop style layout"
			},
			modern : {
				title : "Modern",
				desc : "A modern layout"
			},
			minimal : {
				title : "Minimal",
				desc : "A minimal layout"
			}
		},
		ui : {
			selectorLabelTitle : "View"
		}
	},
	themes : {
		bundleName : "Style",
		bundleDescription : "--CORE FUNCTIONALITY-- The Themes bundle manages all CSS information like colors, font styles, background images etc. Users can switch between different looks of the user interface elements by selecting different styles.",
		themes : {
			pure : {
				desc : "The map.apps 'pure' style."
			},
			night : {
				desc : "The map.apps 'night' style."
			}
		},
		ui : {
			selectorLabelTitle : "Style"
		}
	},
	windowmanager : {
		bundleName : "Window Manager",
		bundleDescription : "--CORE FUNCTIONALITY-- The Window Manager is responsible to manage all floating windows.",
		ui : {
			defaultWindowTitle : "Window",
			closeBtn : {
				title : "Close"
			},
			minimizeBtn : {
				title : "Minimize"
			},
			maximizeBtn : {
				title : "Maximize"
			},
			restoreBtn : {
				title : "Restore"
			},
			opacityBtn : {
				title : "Change transparency"
			},
			collapseBtn : {
				title : "Hide content"
			},
			loading : {
				title : "Please wait!",
				message : "Loading..."
			},
			okcancel : {
				title : "Question",
				okButton : "OK",
				cancelButton : "Cancel"
			}
		}
	},
	ec_legend : {
		bundleName : "Le\u0121\u0121enda",
		bundleDescription : "This bundle provides the Esri legend.",
		tool : {
			title : "Le\u0121\u0121enda",
			tooltip : "Le\u0121\u0121enda"
		}
	},
	ec_parameter_interceptor : {
		bundleName : "ec-parameter-interceptor",
		bundleDescription : "ec-parameter-interceptor",
		errors : {
			missingQueryParameter : "Missing query parameters",
			missingTypeQueryParameter : "Missing type query parameter",
			missingDatasetOrUrlQueryParameter : "Missing dataset or url query parameter",
			noComponentContextProvided : "No componentContext provided",
			noBundleContextProvided : "No bundleContext provided",
			noQueryParametersProvided : "No query parameters provided",
			datasetTypeNotSupported : "The type '${type}' is not supported",
			invalidGeoJSON : "Invalid GeoJSON",
			unsupportedCRSType : "Only named CRS are supported",
			canNotParseNamedCRS : "Can not parse named CRS to WKID: ${name}",
			mixingOfDifferentCRS : "Mixing of different CRS is not supported",
			invalidGeoJSONType : "Unknown GeoJSON type attribute: ${type}",
			typeNotFoundInResource : "No appropriate format for ${type} found in resource",
			couldNotLoadDataset : "Could not load dataset: ${cause}",
			unableToAddDataset : "Unable to add dataset: ${cause}",
			unableToParseCapabilities : "Unable to parse WMS Capabilities response",
			canNotDetermineUrl : "Could not determine the download URL of the resource"
		}
	},
	ec_feature_info : {
		tool : {
			title : "Informazzjoni dwar il-Karatteristika",
			tooltip : "Informazzjoni dwar il-Karatteristika"
		}
	},
	ec_map_loading_screen : {
		bundleName : "ec-map-loading-screen",
		bundleDescription : "ec-map-loading-screen",
		messages : {
			initializingMap : "Il-mappa qed tillowdja..."
		}
	},
	ec_wms_layer_selector : {
		bundleName : "ec-wms-layer-selector",
		bundleDescription : "ec-wms-layer-selector",
		ui : {
			windowTitle : "Ag\u0127\u017Cel kollox",
			hint : "Is-servizz tad-WMS mag\u0127\u017Cul fih ${countToProvideSelection} saff, jekk jog\u0127\u0121bok ag\u0127\u017Cel is-saffi kollha li g\u0127andhom jidhru fil-mappa.",
			selectAll : "Ag\u0127\u017Cel kollox",
			closeButton : "OK"
		},
		tool : {
			title : "Saff",
			tooltip : "Saff"
		}
	},
	ec_error_messages : {
		bundleName : "ec-error-messages",
		bundleDescription : "ec-error-messages",
		general : {
			serviceNotAvailable : "Is-servizz mitlub mhuwiex disponibbli",
			serviceNotSupported : "Is-servizz mitlub ma hija sostnuta.",
			invalidResource : "Instabet ri\u017Corsa invalida fl-URL mog\u0127ti",
			unsupportedResource : "Instabet ri\u017Corsa mhux appo\u0121\u0121jata fl-URL mog\u0127ti",
			errorSource : "Is-sors ta' \u017Cball li \u0121ej \u0121ie ddeterminat",
			requestedUrl : "URL mitlub",
			unsupportedServiceType : "The provided service type is not supported",
			technicalSupport : "Jekk il-problema tibqa' g\u0127addejja fil-futur, jekk jog\u0127\u0121bok ikkuntattja l-appo\u0121\u0121 tekniku",
			technicalSupportCreateTicket : "Create support ticket",
			weWillContactProvider : "A\u0127na se nikkuntattjaw il-fornitur tas-servizz u nirrappurtaw il-kwistjoni.",
			closeButton : "OK"
		},
		detailedErrors : {
			crs : "Is-servizz ma jipprovdix sistema ta' referenza kkoordinata u appo\u0121\u0121jata",
			encoding : "Is-servizz ma jipprovdix kodifikar tal-karattru appo\u0121\u0121jat",
			metadata : "Id-dokument tal-metadata tas-servizz huwa \u017Cbaljat",
			fileFormat : "\u0121ie ri\u010Bevut format tad-data invalidu fl-URL mog\u0127ti",
			dataRequest : "Filwaqt li ti\u0121i vverifikata d-data tas-servizz, \u0121ie ri\u010Bevut rispons invalidu. Ra\u0121unijiet g\u0127al dan jista' jkun, pere\u017Cempju: \u017Cball tas-server intern, konfigurazzjoni \u0127a\u017Cina tas-servizz, im\u0121iba konformi mhux standard tas-servizz, e\u010B\u010B."
		},
		httpIssues : {
			303 : "Ir-rispons g\u0127at-talba jista' jinstab ta\u0127t URL ie\u0127or",
			305 : "Ir-ri\u017Corsa mitluba hija disponibbli biss permezz ta' proxy",
			400 : "Is-server ma jistax jew mhux se jippro\u010Bessa t-talba min\u0127abba \u017Cball per\u010Bepit tal-klijent",
			401 : "Ir-ri\u017Corsa mitluba kellha b\u017Conn awtorizzazzjoni",
			403 : "Ir-ri\u017Corsa mitluba mhijiex pubblikament disponibbli",
			404 : "Ir-ri\u017Corsa mitluba ma setg\u0127etx tinstab",
			405 : "Ir-ri\u017Corsa ntalbet permezz ta' metodu HTTP mhux permess",
			406 : "Intalab kontenut mhux a\u010B\u010Bettabbli mir-ri\u017Corsa",
			407 : "Ir-ri\u017Corsa te\u0127tie\u0121 awtentikazzjoni minn qabel f'servizz proxy",
			500 : "Ir-ri\u017Corsa mhijiex disponibbli min\u0127abba \u017Cball intern tas-server",
			501 : "Ir-ri\u017Corsa mhijiex disponibbli min\u0127abba metodu ta' talba mhux rikonoxxut",
			502 : "Ir-ri\u017Corsa mhijiex disponibbli min\u0127abba gateway ikkonfigurata b'mod \u017Cbaljat",
			503 : "Attwalment ir-ri\u017Corsa mhijiex disponibbli min\u0127abba overload jew manutenzjoni",
			504 : "Ir-ri\u017Corsa mhijiex disponibbli min\u0127abba gateway ikkonfigurata b'mod \u017Cbaljat jew gateway li mhix tirrispondi",
			505 : "Ir-ri\u017Corsa ma tappo\u0121\u0121jax il-ver\u017Cjoni HTTP mog\u0127tija",
			generic : "Sar \u017Cball tas-server fil-mument li ntalbet ir-ri\u017Corsa mog\u0127tija"
		}
	},
	ec_user_tutorial : {
		bundleName : "ec-user-tutorial",
		bundleDescription : "ec-user-tutorial",
		externalResourceDisclaimer : {
			introduction : "Dan il-vi\u017Cwalizzatur tal-mappa ja\u010B\u010Bessa s-servizzi esterni u juri d-data tag\u0127hom. Dawn is-servizzi esterni mhumiex mi\u017Cmuma mill-Kummissjoni Ewropea u g\u0127alhekk a\u0127na ma g\u0127andna l-ebda influwenza fuq id-disponibbilt\u00E0/stabbilt\u00E0 tag\u0127hom. Il-kwistjonijiet li \u0121ejjin jistg\u0127u jse\u0127\u0127u:",
			rotatingCircle : "\u010Birku li jdur fid-dehra tal-mappa jindika li l-vi\u017Cwalizzatur qed jistenna g\u0127al rispons minn sors tad-data. Dan jista' jkun kkaw\u017Cat minn server mhux disponibbli jew minn kwistjonijiet tan-netwerk.",
			layerZoomLevel : "Xi sorsi tas-saff jistg\u0127u ma jidhrux f'\u010Berti livelli ta' \u017Cum. Dan jirri\u017Culta minn restrizzjonijiet stabbiliti mill-fornitur tas-servizz estern. Inti tista' te\u0127tie\u0121 tti\u017C\u017Cumja 'l \u0121ewwa jew 'il barra sabiex tara d-data tas-saff.",
			layerPanning : "Id-data a\u010B\u010Bessata minn server estern tista' tinstab barra d-dehra tal-mappa attwali. Dan il-vi\u017Cwalizzatur tal-mappa jipprova jidentifika awtomatikament i\u017C-\u017Cona koperta. I\u017Cda xorta wa\u0127da, f'xi ka\u017Cijiet il-metadata mhijiex kompleta jew hija \u017Cbaljata u b'hekk ikollok tiddregja sal-limitu attwali.",
			detailInformation : "Jekk titlob informazzjoni ddettaljata g\u0127al karatteristika mag\u0127\u017Cula dan se ji\u0121i ttrattat minn talba spe\u010Bifika tas-server tal-mappa. Jekk ir-rispons g\u0127at-talba huwa ffurmat b'mod \u0127a\u017Cin jew huwa vojt (pere\u017Cempju, min\u0127abba \u017Cball tas-server) ma tista' tinwera l-ebda informazzjoni u se tidher tieqa vojta.",
			mapScaling : "Il-mapep ta' ba\u017Ci u\u017Cati f'din l-applikazzjoni huma pprovduti mill-Uffi\u010B\u010Bju tal-Istatistika tal-Unjoni Ewropea (Eurostat). Dawn il-mapep huma attwalment disponibbli f're\u017Coluzzjoni ta' mhux aktar minn 1:50.000. Aktar mapep ta' ba\u017Ci fini huma attwalment fi t\u0127ejjija (pere\u017Cempju, bix jappo\u0121\u0121jaw il-vi\u017Cwalizzazzjoni tad-data fi \u0127dan belt). B'hekk, \u010Berti sorsi ta' data jistg\u0127u jkunu g\u0127adhom ma ntwerewx b'mappa ta' sfond l-aktar xierqa.",
			technicalSupport : "Jekk tiltaqa' ma'  xi problemi o\u0127ra tiddejjaqx tikkuntattja l-appo\u0121\u0121 tekniku:",
			technicalSupportContactLink : "Ikkuntattja l-appo\u0121\u0121 tekniku",
			mapLoading : "Il-mappa qed tillowdja...",
			donotshow : "Ter\u0121ax turi dan l-iskrin ta' mer\u0127ba."
		},
		legalNotice : "Id-denominazzjonijiet im\u0127addma u l-pre\u017Centazzjoni tal-materjal fuq din il-mappa ma jimplikawx l-espressjoni ta' xi opinjoni tkun li tkun min-na\u0127a tal-Unjoni Ewropea li tikkon\u010Berna l-istatus legali ta' xi pajji\u017C, territorju, belt jew \u017Cona jew tal-awtoritajiet tag\u0127ha, jew tikkon\u010Berna d-delimitazzjoni tal-fruntieri jew konfini tag\u0127ha. Kosovo *: Din id-denominazzjoni  hija ming\u0127ajr pre\u0121udizzju g\u0127al po\u017Cizzjonijiet dwar l-istatus, u hija konformi mal-UNSCR 1244/1999 u l-Opinjoni QI\u0121 dwar id-dikjarazzjoni ta' indipendenza ta' Kosovo. Palestina*: Din id-denominazzjoni ma g\u0127andhiex ti\u0121i interpretata b\u0127ala rikonoxximent ta' Stat tal-Palestina u hija ming\u0127ajr pre\u0121udizzju g\u0127all-po\u017Cizzjonijiet indivdwali tal-Istati Membri dwar din il-kwistjoni.",
		legalNoticeHeader : "Avvi\u017C legali",
		tutorial : {
			welcome : "Mer\u0127ba fil-vi\u017Cwalizzatur tal-mappa tal-Portal tad-Data Ewropew!",
			familiarise : "Din l-introduzzjoni qasira g\u0127andha l-g\u0127an li tiffamiljarizzak bl-elementi u l-funzjonalit\u00E0 tal-vi\u017Cwalizzatur tal-mappa.",
			navigation : "Ikklikkja u \u017Comm mag\u0127fusa l-buttuna tax-xellug tal-mouse biex tiddreggja l-mappa.",
			zoom : "Dawn il-buttuni jbiddlu l-livell ta' \u017Cum fuq il-mappa. B'mod alternattiv inti tista' tu\u017Ca r-rota tal-mouse biex ta\u0121\u0121usta \u017C-\u017Cum.",
			features : "Funzjonalit\u00E0 addizzjonali hija disponibbli permezz ta' dawn il-buttuni. Dawn ja\u0121ixxu b\u0127ala toggles u jippermettu jew jiddi\u017Cattivaw il-funzjonalit\u00E0 mog\u0127tija.",
			legend : "Il-le\u0121\u0121enda tista' tintu\u017Ca biex te\u017Camina s-saffi tal-mappa disponibbli u tippermetti jew tiddi\u017Cattiva d-displej attwali tag\u0127hom fuq il-mappa. L-ismijiet huma derivati direttament mis-servizz a\u010B\u010Bessat b'mod estern.",
			transparency : "Inti tista' wkoll ta\u0121\u0121usta t-trasparenza tas-saff.",
			featureinfo : "Xi data tista' ti\u0121i e\u017Caminata anke f'aktar dettall. Meta din il-karatteristika hija attivata inti tista' tikklikkja fuq il-mappa biex issaqsi informazzjoni addizzjonali.",
			done : "Lest",
			okButton : "OK",
			closeButton : "Ag\u0127laq",
			skip : "Aqbe\u017C",
			next : "Li jmiss",
			back : "Pre\u010Bedenti"
		}
	}
});