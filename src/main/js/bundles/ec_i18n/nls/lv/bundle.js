define({
	contentviewer : {
		bundleName : "Saturs Viewer",
		bundleDescription : "Par\u0101da da\u017e\u0101da veida saturu.",
		ui : {
			unknownContentError : "Saturs nav zin\u0101ms.",
			graphicinfotool : {
				title : "Elementas informacija",
				desc : "Elementas informacija"
			},
			content : {
				defaultTitle : "Elements",
				grid : {
					detailView : "R\u0101d\u012bt inform\u0101ciju",
					key : "\u012apa\u0161ums",
					value : "V\u0113rt\u012bba"
				},
				customTemplate : {
					detailView : "R\u0101d\u012bt inform\u0101ciju"
				},
				attachment : {
					noAttachmentSupport : "\u0160is sl\u0101nis neatbalsta pielikumus",
					detailView : "R\u0101d\u012bt inform\u0101ciju"
				},
				AGSDetail : {
					title : "Skat\u012bt deta\u013cas",
					print : "Druk\u0101t",
					serviceMetadata : "Pakalpojums metadati",
					serviceURL : "URL",
					serviceCopyrights : "Autorties\u012bbas",
					serviceTitle : "Virsraksts",
					pager : {
						pageSizeLabel : "Iez\u012bme ${currentPage} no ${endPage}"
					},
					key : "\u012apa\u0161ums",
					value : "V\u0113rt\u012bba",
					detailView : "R\u0101d\u012bt inform\u0101ciju"
				}
			}
		}
	},
	coordinatetransformer : {
		bundleName : "Koordin\u0101tu transformators",
		bundleDescription : "--Pagrindin\u0117 funkcija-- Koordin\u0113t transformators p\u0101rveido \u0123eometriju no vienas koordin\u0101tu sist\u0113mas uz otru."
	},
	featureinfo : {
		bundleName : "FeatureInfo",
		bundleDescription : "FeatureInfo par\u0101da inform\u0101ciju par funkcij\u0101m akt\u012bvo sl\u0101\u0146iem.",
		ui : {
			featureInfo : {
				noQueryLayersFound : "N\u0113 mekl\u0113\u0161anas sluoksniai pieejama!",
				contentInfoWindowTitle : "Identific\u0113t",
				noResultsFound : "Nekas nav atrasts.",
				loadingInfoText : "Pieprasa sl\u0101nis ${layerName} (${layerIndex} no ${layersTotal}).",
				featureInfoTool : "Identific\u0113t",
				layer : "Sl\u0101nis",
				feature : "Objekts"
			},
			wms : {
				emptyResult : 'Nav atrasti WMS sl\u0101\u0146a "${layerTitle}" rezult\u0101ti.'
			}
		}
	},
	geometryservice : {
		bundleName : "GeometryService",
		bundleDescription : "Sai\u0161\u0137a nodro\u0161ina centr\u0101lo \u0123eometrijas pakalpojumu."
	},
	geojson : {
		bundleName : "GeoJSON",
		bundleDescription : "\u0160aj\u0101 komplekt\u0101 sniedz konvert\u0113\u0161anas pakalpojumu, kas var apstr\u0101d\u0101t GeoJSON un labi zin\u0101mo tekstu (WKT)."
	},
	languagetoggler : {
		bundleName : "Valoda komutatoru",
		bundleDescription : "Ar valodas komutatoru interfeisa valodu var main\u012bt.",
		ui : {
			title : "Valoda"
		}
	},
	mobiletoc : {
		bundleName : "Mobils l\u012bme\u0146a kontrole",
		bundleDescription : "\u0160aj\u0101 komplekt\u0101 nodro\u0161ina mobilo l\u012bme\u0146a regulators gatavs",
		tool : {
			title : "Karte saturs",
			tooltip : "Iesl\u0113gt / izsl\u0113gt kartes saturu"
		},
		ui : {
			basemaps : "B\u0101zes kartes",
			operationalLayers : "T\u0113mas l\u012bme\u0146i",
			switchUI : {
				noTitle : "bez nosaukuma",
				leftLabel : "Par",
				rightLabel : "No"
			}
		}
	},
	map : {
		bundleName : "Karte",
		bundleDescription : "Karte sai\u0161\u0137a p\u0101rvalda galveno karti un visu karti satura inform\u0101ciju. Klients var abstrakti izmantotie pakalpojumi hierarhisk\u0101 Karte modeli, lai vi\u0146i var par\u0101d\u012bt da\u017e\u0101dos veidos, lai lietot\u0101jam.",
		ui : {
			nodeUpdateError : {
				info : 'Atjaunin\u0101\u0161ana pakalpojuma "${title}" nav! K\u013c\u016bda: ${errorMsg}',
				detail : 'Atjaunin\u0101\u0161ana pakalpojuma "${title}" nav! K\u013c\u016bda: ${url} ${errorMsg} - Deta\u013cas: ${error}'
			},
			sliderLabels : {
				country : "Valsts",
				region : "Apgabals",
				town : "Pils\u0113ta"
			}
		},
		drawTooltips : {
			addPoint : "Klik\u0161\u0137iniet uz kartes",
			addMultipoint : "Noklik\u0161\u0137iniet, lai s\u0101ktu",
			finishLabel : "Apdare"
		}
	},
	infoviewer : {
		bundleName : "Inform\u0101cijas displejs",
		bundleDescription : "Zi\u0146u displejs r\u0101da lietot\u0101ja b\u016btisku inform\u0101ciju par vietu uz kartes log\u0101.",
		ui : {
			title : "Info Viewer",
			tools : {
				closeInfoWindowMapdeskTool : "Aizv\u0113rt",
				closeInfoWindowMapTool : "Aizv\u0113rt",
				focusMapTool : "Centrs karte",
				attachToGeorefTool : "Pievienot st\u0101vokl\u012b",
				mainActivationTool : "Inform\u0101ciju par atra\u0161an\u0101s vietu"
			}
		}
	},
	mobileview : {
		bundleName : "Mobilo View",
		bundleDescription : "Satur \u012bpa\u0161as nodarb\u012bbas, ka noteikt da\u017eus jaut\u0101jumus par dojox.mobile"
	},
	notifier : {
		bundleName : "Pazi\u0146ojumi",
		bundleDescription : "Visi statuss, progresa vai k\u013c\u016bdu zi\u0146ojumi tiek par\u0101d\u012bti lietot\u0101jam k\u0101d\u0101 pop-up zi\u0146u, lai ir skaidrs, kas notiek pieteikum\u0101.",
		ui : {
			title : "Notifier",
			tooltips : {
				close : "Aizv\u0113rt \u0161o zi\u0146u",
				glue : "Pievienojiet zi\u0146u"
			}
		}
	},
	parametermanager : {
		bundleName : "Parametrs apstr\u0101de",
		bundleDescription : "Parametrs p\u0101rvald\u012bt\u0101js ir atbild\u012bgs dele\u0123\u0113t parametrus no URL uz atbilsto\u0161i sast\u0101vda\u013cas starta.",
		ui : {
			encoderBtn : "Link instruments",
			encoderBtnTooltip : "Link instruments",
			sendMail : "E-pasts",
			refresh : "Atsvaidzin\u0101t",
			linkBoxTitle : "Link URL",
			size : "Izm\u0113rs (pikse\u013ci)",
			codeBoxTitle : "Kods iegult HTML",
			qrCode : "QRCode",
			mailBody : "${url}",
			mailSubject : "Paskaties pie reizes \u0161o karti!",
			options : {
				small : "mazs (480 x 320)",
				medium : "vid\u0113js (640 x 480)",
				large : "liels (1280 x 1024)",
				custom : "lietot\u0101ja defin\u0113tu"
			}
		}
	},
	printing : {
		bundleName : "Druk\u0101t",
		bundleDescription : "\u0160aj\u0101 komplekt\u0101 pied\u0101v\u0101 drukas r\u012bku, lai druk\u0101tu karti.",
		tool : {
			title : "Druk\u0101t",
			tooltip : "Druk\u0101t",
			back : "Atpaka\u013c"
		},
		resultWin : {
			title : "Druk\u0101t"
		}
	},
	qrcode : {
		bundleName : "QRCode",
		bundleDescription : "\u0160aj\u0101 komplekt\u0101 sniedz pakalpojumu, kas var izmantot, lai izveidotu QR kodus.",
		errorMessage : "QR kods paaudze nebija veiksm\u012bga."
	},
	splashscreen : {
		bundleName : "S\u0101kuma ekr\u0101ns",
		bundleDescription : "S\u0101kuma ekr\u0101ns r\u0101da progresa josla, kam\u0113r tiek start\u0113ta.",
		loadTitle : 'S\u0101kt pieteikumu "{appName}"',
		loadBundle : "{percent}% - {startedBundles}/{maxBundlesToStart} - Uzl\u0101des {name}"
	},
	system : {
		bundleName : "Sist\u0113ma",
		bundleDescription : "--Pagrindin\u0117 funkcija-- System apraksta pamata pamatfunkcijas (compilable skelets) no map.apps."
	},
	templatelayout : {
		bundleName : "View Layout",
		bundleDescription : "--Pagrindin\u0117 funkcija-- Template Layout \u012bsteno vieno\u0161an\u0101s visu lietot\u0101ja interfeisa elementus, pamatojoties uz iepriek\u0161 veidnes."
	},
	templates : {
		bundleName : "Skats",
		bundleDescription : "--Pagrindin\u0117 funkcija-- Views izmanto Veid\u0146u izk\u0101rtojums.",
		templates : {
			desktop : {
				title : "Desktop",
				desc : "Desktop orient\u0113ta izk\u0101rtojums."
			},
			modern : {
				title : "M\u016bsdienu",
				desc : "Moderns stil\u012bgs izk\u0101rtojums."
			},
			minimal : {
				title : "Minim\u0101ls",
				desc : "Minim\u0101lisma izk\u0101rtojums"
			}
		},
		ui : {
			selectorLabelTitle : "Skats"
		}
	},
	themes : {
		bundleName : "Stils",
		bundleDescription : "--Pagrindin\u0117 funkcija-- T\u0113mas sai\u0161\u0137a p\u0101rvalda visu CSS inform\u0101ciju, piem\u0113ram, kr\u0101su, fontu stiliem, fona att\u0113lus utt Lietot\u0101ji var p\u0101rsl\u0113gties starp da\u017e\u0101diem izskat\u0101s no lietot\u0101ja interfeisa elementus, izv\u0113loties da\u017e\u0101dus stilus.",
		themes : {
			pure : {
				desc : 'Map.apps "t\u012brs" stils.'
			},
			night : {
				desc : 'Map.apps "nakts" stils.'
			}
		},
		ui : {
			selectorLabelTitle : "Stils"
		}
	},
	windowmanager : {
		bundleName : "Vad\u012bba logs",
		bundleDescription : "--Pagrindin\u0117 funkcija-- Logu p\u0101rvaldnieks ir atbild\u012bgs par visas dialoglogu.",
		ui : {
			defaultWindowTitle : "Logs",
			closeBtn : {
				title : "Aizv\u0113rt"
			},
			minimizeBtn : {
				title : "Minimiz\u0113t"
			},
			maximizeBtn : {
				title : "Maksimiz\u0113t"
			},
			restoreBtn : {
				title : "Atjaunot"
			},
			opacityBtn : {
				title : "Main\u012bt p\u0101rredzam\u012bba"
			},
			collapseBtn : {
				title : "Sl\u0113pt saturs"
			},
			loading : {
				title : "L\u016bdzu, uzgaidiet!",
				message : "Iekrau\u0161ana ..."
			},
			okcancel : {
				title : "Jaut\u0101jums",
				okButton : "Labi",
				cancelButton : "Atcelt"
			}
		}
	},
	ec_legend : {
		bundleName : "Le\u0123enda",
		bundleDescription : "\u0160aj\u0101 komplekt\u0101 nodro\u0161ina ESRI le\u0123enda.",
		tool : {
			title : "Le\u0123enda",
			tooltip : "Le\u0123enda"
		}
	},
	ec_parameter_interceptor : {
		bundleName : "ec-parameter-interceptor",
		bundleDescription : "ec-parameter-interceptor",
		errors : {
			missingQueryParameter : "Missing query parameters",
			missingTypeQueryParameter : "Missing type query parameter",
			missingDatasetOrUrlQueryParameter : "Missing dataset or url query parameter",
			noComponentContextProvided : "No componentContext provided",
			noBundleContextProvided : "No bundleContext provided",
			noQueryParametersProvided : "No query parameters provided",
			datasetTypeNotSupported : "The type '${type}' is not supported",
			invalidGeoJSON : "Invalid GeoJSON",
			unsupportedCRSType : "Only named CRS are supported",
			canNotParseNamedCRS : "Can not parse named CRS to WKID: ${name}",
			mixingOfDifferentCRS : "Mixing of different CRS is not supported",
			invalidGeoJSONType : "Unknown GeoJSON type attribute: ${type}",
			typeNotFoundInResource : "No appropriate format for ${type} found in resource",
			couldNotLoadDataset : "Could not load dataset: ${cause}",
			unableToAddDataset : "Unable to add dataset: ${cause}",
			unableToParseCapabilities : "Unable to parse WMS Capabilities response",
			canNotDetermineUrl : "Could not determine the download URL of the resource"
		}
	},
	ec_feature_info : {
		tool : {
			title : "Funkcijas",
			tooltip : "Funkcijas"
		}
	},
	ec_map_loading_screen : {
		bundleName : "ec-map-loading-screen",
		bundleDescription : "ec-map-loading-screen",
		messages : {
			initializingMap : "Inicializ\u0113 karti. L\u016Bdzu, uzgaidiet!"
		}
	},
	ec_wms_layer_selector : {
		bundleName : "ec-wms-layer-selector",
		bundleDescription : "ec-wms-layer-selector",
		ui : {
			windowTitle : "Atlas\u012Bt sl\u0101\u0146us",
			hint : "Atlas\u012Btais WMS pakalpojums satur ${countToProvideSelection} sl\u0101ni(-\u0146us); l\u016Bdzu, atlasiet visus sl\u0101\u0146us, kuri ir j\u0101par\u0101da kart\u0113.",
			selectAll : "Atlas\u012Bt visus",
			closeButton : "Labi"
		},
		tool : {
			title : "Sl\u0101nis",
			tooltip : "Sl\u0101nis"
		}
	},
	ec_error_messages : {
		bundleName : "ec-error-messages",
		bundleDescription : "ec-error-messages",
		general : {
			serviceNotAvailable : "Piepras\u012Btais pakalpojums nav pieejams",
			serviceNotSupported : "Piepras\u012Btais pakalpojums nav atbalst\u012Bts",
			invalidResource : "Nor\u0101d\u012Btaj\u0101 URL tika atrasts neder\u012Bgs resurss",
			unsupportedResource : "Nor\u0101d\u012Btaj\u0101 URL tika atrasts neatbalst\u012Bts resurss",
			errorSource : "Tika noteikts \u0161\u0101ds k\u013C\u016Bdas avots",
			requestedUrl : "Piepras\u012Btais URL",
			unsupportedServiceType : "The provided service type is not supported",
			technicalSupport : "Ja probl\u0113ma atk\u0101rtojas, l\u016Bdzu, sazinieties ar tehnisk\u0101 atbalsta dienestu",
			technicalSupportCreateTicket : "Create support ticket",
			weWillContactProvider : "We will contact the provider of the service and report the issue.",
			closeButton : "Labi"
		},
		detailedErrors : {
			crs : "Pakalpojums nenodro\u0161ina atbalst\u012Btu koordin\u0101tu atskaites sist\u0113mu",
			encoding : "Pakalpojums nenodro\u0161ina atbalst\u012Btu rakstz\u012Bmju kod\u0113\u0161anu",
			metadata : "Pakalpojuma metadatu dokuments ir k\u013C\u016Bdains",
			fileFormat : "Nor\u0101d\u012Btaj\u0101 URL tika sa\u0146emts neder\u012Bgs datu form\u0101ts",
			dataRequest : "Pieprasot pakalpojuma datus, tika sa\u0146emta neder\u012Bga atbilde. Iemesli tam var b\u016Bt, piem\u0113ram, \u0161\u0101di: iek\u0161\u0113ja servera k\u013C\u016Bda, nepareiza pakalpojuma konfigur\u0101cija, standartam neatbilsto\u0161a pakalpojuma darb\u012Bba u.\u00A0c."
		},
		httpIssues : {
			303 : "Atbildi uz piepras\u012Bjumu var atrast pie cita URI",
			305 : "Piepras\u012Btais resurss ir pieejams tikai caur starpniekserveri",
			400 : "Serveris nevar apstr\u0101d\u0101t piepras\u012Bjumu, jo uzskata to par klienta k\u013C\u016Bdu",
			401 : "Piepras\u012Btais resurss piepras\u012Bja autoriz\u0101ciju",
			403 : "Piepras\u012Btais resurss nav publiski pieejams",
			404 : "Piepras\u012Bto resursu nevar\u0113ja atrast",
			405 : "Resursu piepras\u012Bja ar neat\u013Cautu HTTP metodi",
			406 : "No resursa piepras\u012Bja nepie\u0146emamu saturu",
			407 : "Resurss pieprasa iepriek\u0161\u0113ju autentifik\u0101ciju starpniekservera pakalpojum\u0101",
			500 : "Resurss nav pieejams iek\u0161\u0113jas servera k\u013C\u016Bdas d\u0113\u013C",
			501 : "Resurss nav pieejams neatpaz\u012Btas piepras\u012Bjuma metodes d\u0113\u013C",
			502 : "Resurss nav pieejams k\u013C\u016Bdaini konfigur\u0113tas v\u0101rtejas d\u0113\u013C",
			503 : "Resurss pa\u0161laik nav pieejams p\u0101rslodzes vai uztur\u0113\u0161anas d\u0113\u013C",
			504 : "Resurss nav pieejams k\u013C\u016Bdaini konfigur\u0113tas vai neatbildo\u0161as v\u0101rtejas d\u0113\u013C",
			505 : "Resurss neatbalsta attiec\u012Bgo HTTP versiju",
			generic : "Pieprasot resursu, notika servera k\u013C\u016Bda"
		}
	},
	ec_user_tutorial : {
		bundleName : "ec-user-tutorial",
		bundleDescription : "ec-user-tutorial",
		externalResourceDisclaimer : {
			introduction : "\u0160is kar\u0161u skat\u012Bt\u0101js piek\u013C\u016Bst \u0101r\u0113jiem pakalpojumiem un par\u0101da to datus. \u0160os \u0101r\u0113jos pakalpojumus neuztur Eiropas Komisija, un t\u0101p\u0113c m\u0113s nevaram ietekm\u0113t to pieejam\u012Bbu/stabilit\u0101ti. Var rasties \u0161\u0101das probl\u0113mas:",
			rotatingCircle : "Rot\u0113jo\u0161s aplis kartes skat\u0101 par\u0101da, ka skat\u012Bt\u0101js gaida atbildi no datu avota. To var izrais\u012Bt nepieejams serveris vai t\u012Bkla probl\u0113mas.",
			layerZoomLevel : "Da\u017Ei sl\u0101\u0146u avoti var netikt par\u0101d\u012Bti noteiktos t\u0101lummai\u0146as l\u012Bme\u0146os. To izraisa \u0101r\u0113j\u0101 pakalpojuma sniedz\u0113ja noteikti ierobe\u017Eojumi. Jums var n\u0101kties tuvin\u0101t vai t\u0101lin\u0101t, lai par\u0101d\u012Btu sl\u0101\u0146a datus.",
			layerPanning : "Dati, kuriem piek\u013C\u016Bst no \u0101r\u0113ja servera, var atrasties \u0101rpus pa\u0161reiz\u0113j\u0101 kartes skata. \u0160is kar\u0161u skat\u012Bt\u0101js cen\u0161as autom\u0101tiski identific\u0113t nosegto teritoriju. Tom\u0113r da\u017Eos gad\u012Bjumos metadati ir nepiln\u012Bgi vai k\u013C\u016Bdaini, t\u0101p\u0113c jums karte ir j\u0101b\u012Bda l\u012Bdz faktiskajam p\u0101rkl\u0101jumam.",
			detailInformation : "Ja j\u016Bs piepras\u0101t detaliz\u0113tu inform\u0101ciju par atlas\u012Bto funkciju, to apstr\u0101d\u0101s \u012Bpa\u0161s piepras\u012Bjums kar\u0161u serverim. Ja atbilde uz \u0161o piepras\u012Bjumu ir nepareizi veidota vai tuk\u0161a (piem\u0113ram, servera k\u013C\u016Bdas d\u0113\u013C), tad nevar par\u0101d\u012Bt inform\u0101ciju, un par\u0101d\u012Bsies tuk\u0161s logs.",
			mapScaling : "\u0160aj\u0101 lietotn\u0113 izmantot\u0101s pamatkartes nodro\u0161ina Eiropas Savien\u012Bbas statistikas birojs (Eurostat). \u0160\u012Bs kartes pa\u0161laik ir pieejamas ar iz\u0161\u0137irtsp\u0113ju l\u012Bdz 1:50\u00A0000. Pa\u0161laik tiek sagatavotas smalk\u0101kas pamatkartes (piem\u0113ram, lai atbalst\u012Btu datu vizualiz\u0101ciju pils\u0113t\u0101). T\u0101p\u0113c da\u017Ei datu avoti v\u0113l var netikt par\u0101d\u012Bti ar optim\u0101li piem\u0113rotu fona karti.",
			technicalSupport : "Ja saskaraties ar cit\u0101m probl\u0113m\u0101m, aicin\u0101m sazin\u0101ties ar tehnisk\u0101 atbalsta dienestu:",
			technicalSupportContactLink : "Sazin\u0101ties ar tehnisk\u0101 atbalsta dienestu",
			mapLoading : "Karte tiek iel\u0101d\u0113ta...",
			donotshow : "Vairs ner\u0101d\u012Bt \u0161o sveiciena ekr\u0101nu."
		},
		legalNotice : "\u0160aj\u0101 kart\u0113 izmantotie apz\u012Bm\u0113jumi un materi\u0101lu att\u0113lojums nek\u0101d\u0101 veid\u0101 nepau\u017E Eiropas Savien\u012Bbas nost\u0101ju par valstu, teritoriju, pils\u0113tu vai to p\u0101rvaldes iest\u0101\u017Eu juridisko statusu vai robe\u017Eu noteik\u0161anu. Kosova*: \u0160is apz\u012Bm\u0113jums nek\u0101di neietekm\u0113 nost\u0101jas attiec\u012Bb\u0101 uz statusu un atbilst ANO DP Rezol\u016Bcijai\u00A01244/1999 un Starptautisk\u0101s Tiesas atzinumam par Kosovas neatkar\u012Bbas pasludin\u0101\u0161anu. Palest\u012Bna*: \u0160is apz\u012Bm\u0113jums nav iztulkojams k\u0101 Palest\u012Bnas valsts atz\u012B\u0161ana un tas nek\u0101di neietekm\u0113 dal\u012Bbvalstu atsevi\u0161\u0137\u0101s nost\u0101jas \u0161aj\u0101 jaut\u0101jum\u0101.",
		legalNoticeHeader : "Juridiska atruna",
		tutorial : {
			welcome : "Esiet sveicin\u0101ti Eiropas datu port\u0101la kar\u0161u skat\u012Bt\u0101j\u0101!",
			familiarise : "\u0160\u012B neliel\u0101 ievada m\u0113r\u0137is ir iepaz\u012Bstin\u0101t j\u016Bs ar kar\u0161u skat\u012Bt\u0101ja elementiem un funkcij\u0101m.",
			navigation : "Noklik\u0161\u0137iniet un turiet peles kreiso pogu, lai b\u012Bd\u012Btu karti.",
			zoom : "\u0160\u012Bs pogas maina t\u0101lummai\u0146as l\u012Bmeni kart\u0113. Lai izmain\u012Btu t\u0101lummai\u0146u, j\u016Bs varat izmantot ar\u012B peles riten\u012Bti.",
			features : "Ar \u0161o pogu pal\u012Bdz\u012Bbu ir pieejamas papildu funkcijas. T\u0101s darbojas k\u0101 p\u0101rsl\u0113gtausti\u0146i un iesp\u0113jo vai atsp\u0113jo attiec\u012Bgo funkciju.",
			legend : "Apz\u012Bm\u0113jumus var izmantot, lai p\u0113t\u012Btu pieejamos kartes sl\u0101\u0146us un iesp\u0113jotu vai atsp\u0113jotu to pa\u0161reiz\u0113jo att\u0113lo\u0161anu kart\u0113. Nosaukumi ir tie\u0161i ieg\u016Bti no \u0101r\u0113jas piek\u013Cuves pakalpojuma.",
			transparency : "J\u016Bs ar\u012B varat piel\u0101got sl\u0101\u0146a caursp\u012Bd\u012Bgumu.",
			featureinfo : "Da\u017Eus datus var izp\u0113t\u012Bt v\u0113l detaliz\u0113t\u0101k. Kad \u0161\u012B funkcija ir aktiviz\u0113ta, j\u016Bs varat noklik\u0161\u0137in\u0101t uz kartes, lai piepras\u012Btu papildu inform\u0101ciju.",
			done : "Gatavs",
			okButton : "Labi",
			closeButton : "Aizv\u0113rt",
			skip : "Izlaist",
			next : "T\u0101l\u0101k",
			back : "Atpaka\u013C"
		}
	}
});