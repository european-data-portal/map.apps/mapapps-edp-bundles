define({
	contentviewer : {
		bundleName : "Sisu Viewer",
		bundleDescription : "N\u00e4itab erinevaid sisu.",
		ui : {
			unknownContentError : "Sisu ei ole teada.",
			graphicinfotool : {
				title : "Element info",
				desc : "Element info"
			},
			content : {
				defaultTitle : "Element",
				grid : {
					detailView : "Uuri l\u00e4hemalt",
					key : "Kinnisvara",
					value : "V\u00e4\u00e4rtus"
				},
				customTemplate : {
					detailView : "Uuri l\u00e4hemalt"
				},
				attachment : {
					noAttachmentSupport : "See kiht ei toeta manuseid",
					detailView : "Uuri l\u00e4hemalt"
				},
				AGSDetail : {
					title : "Vaata l\u00e4hemalt",
					print : "Tr\u00fckk",
					serviceMetadata : "Metaandmeid",
					serviceURL : "URL",
					serviceCopyrights : "Autori\u00f5igused",
					serviceTitle : "Pealkiri",
					pager : {
						pageSizeLabel : "Feature ${currentPage} dollarit {endPage}"
					},
					key : "Kinnisvara",
					value : "V\u00e4\u00e4rtus",
					detailView : "Uuri l\u00e4hemalt"
				}
			}
		}
	},
	coordinatetransformer : {
		bundleName : "Koordinaatide trafo",
		bundleDescription : "--CORE FUNKTSIONAALSUSE-- Koordineerida trafo muudab geomeetriale \u00fchest koordinaatide s\u00fcsteemi teise."
	},
	featureinfo : {
		bundleName : "FeatureInfo",
		bundleDescription : "FeatureInfo kuvab teavet funktsioone aktiivne kihti.",
		ui : {
			featureInfo : {
				noQueryLayersFound : "No otsingumootoriga kihid saadaval!",
				contentInfoWindowTitle : "Identifitseerima",
				noResultsFound : "Tulemusi ei leitud.",
				loadingInfoText : "P\u00e4ringute kiht ${layerName} (${layerIndex} dollarit {layersTotal}).",
				featureInfoTool : "Identifitseerima",
				layer : "Kiht",
				feature : "Objekti"
			},
			wms : {
				emptyResult : 'Tulemusi ei leitud kohta WMS kiht "${layerTitle}".'
			}
		}
	},
	geojson : {
		bundleName : "GeoJSON",
		bundleDescription : "See kimp annab konverteerimise teenust, mida saab t\u00f6\u00f6delda GeoJSON ja tuntud teksti (WKT)."
	},
	geometryservice : {
		bundleName : "GeometryService",
		bundleDescription : "Kimp tagab keskse geomeetria teenust."
	},
	infoviewer : {
		bundleName : "Teave Display",
		bundleDescription : "S\u00f5num kuvatakse ekraanile kasutaja sisulist teavet koht kaardil aknas.",
		ui : {
			title : "Info Viewer",
			tools : {
				closeInfoWindowMapdeskTool : "L\u00e4hedal",
				closeInfoWindowMapTool : "L\u00e4hedal",
				focusMapTool : "Keskus kaart",
				attachToGeorefTool : "Kinnita asendisse",
				mainActivationTool : "Asukoha info"
			}
		}
	},
	languagetoggler : {
		bundleName : "Keel vahetaja",
		bundleDescription : "L\u00e4bi keele vahetaja kasutajaliidese keel saab muuta.",
		ui : {
			title : "Keel"
		}
	},
	map : {
		bundleName : "Kaart",
		bundleDescription : "Kaart kimp juhib peamist kaart ja k\u00f5ik kaardi sisu kohta. Klient saab abstraktne teenuseid kasutatakse hierarhilist Kaart mudel, nii et neid saab kuvada eri viisidel kasutaja.",
		ui : {
			nodeUpdateError : {
				info : 'V\u00e4rskenda teenuse "${title}" eba\u00f5nnestus! Viga: ${errorMsg}',
				detail : 'V\u00e4rskenda teenuse "${title}" eba\u00f5nnestus! Viga: ${url} ${errorMsg} - Detailid: ${error}'
			},
			sliderLabels : {
				country : "Riik",
				region : "Piirkond",
				town : "Linn"
			}
		},
		drawTooltips : {
			addPoint : "Kliki kaardil",
			addMultipoint : "Kl\u00f5psake alustamiseks",
			finishLabel : "L\u00f5pp"
		}
	},
	mobiletoc : {
		bundleName : "Mobiilne tasandil kontrolli",
		bundleDescription : "See kimp annab mobiilse tasandil kontroller valmis",
		tool : {
			title : "Kaart sisu",
			tooltip : "L\u00fclita sisse / v\u00e4lja Kaart sisu"
		},
		ui : {
			basemaps : "Base kaardid",
			operationalLayers : "Teemasid taset",
			switchUI : {
				noTitle : "pealkiri puudub",
				leftLabel : "Edasi",
				rightLabel : "\u00c4ra"
			}
		}
	},
	mobileview : {
		bundleName : "Mobile View",
		bundleDescription : "Sisaldab eriklassi, et m\u00e4\u00e4rata m\u00f5ned k\u00fcsimused puudutavad dojox.mobile"
	},
	notifier : {
		bundleName : "Teated",
		bundleDescription : "K\u00f5ik staatus, edu v\u00f5i veateateid kuvatakse kasutajale h\u00fcpikteadet nii, et see on selge, mis toimub taotluse.",
		ui : {
			title : "Vahti",
			tooltips : {
				close : "Sulge teade",
				glue : "Kinnitage s\u00f5num"
			}
		}
	},
	parametermanager : {
		bundleName : "Parameeter k\u00e4itlemine",
		bundleDescription : "Parameeter ettev\u00f5tja vastutab delegeerida parameetrid URL vastavalt komponendid k\u00e4ivitamisel.",
		ui : {
			encoderBtn : "Link t\u00f6\u00f6riist",
			encoderBtnTooltip : "Link t\u00f6\u00f6riist",
			sendMail : "E-kiri",
			refresh : "V\u00e4rskendama",
			linkBoxTitle : "Link URL",
			size : "Suurus (pikslid)",
			codeBoxTitle : "Kood kinnistada HTML",
			qrCode : "QRCode",
			mailBody : "${url}",
			mailSubject : "Vaata korda see kaart!",
			options : {
				small : "V\u00e4ike (480 x 320)",
				medium : "keskmise (640 x 480)",
				large : "suur (1280 x 1024)",
				custom : "kasutaja m\u00e4\u00e4ratud"
			}
		}
	},
	printing : {
		bundleName : "Tr\u00fckk",
		bundleDescription : "See kimp annab print vahend printida kaarti.",
		tool : {
			title : "Tr\u00fckk",
			tooltip : "Tr\u00fckk",
			back : "Tagasi"
		},
		resultWin : {
			title : "Tr\u00fckk"
		}
	},
	qrcode : {
		bundleName : "QRCode",
		bundleDescription : "See kimp annab teenuse, mida saab kasutada, et luua QR koodid.",
		errorMessage : "QR code p\u00f5lvkonna ei olnud edukas."
	},
	splashscreen : {
		bundleName : "Avakuva",
		bundleDescription : "Kodu ekraan n\u00e4itab edenemise riba samal ajal rakenduse k\u00e4ivitamisel.",
		loadTitle : 'Alusta rakenduse  "{appName}"',
		loadBundle : "{percent}% - {startedBundles}/{maxBundlesToStart} - laadimine {name}"
	},
	system : {
		bundleName : "S\u00fcsteem",
		bundleDescription : "--CORE FUNKTSIONAALSUSE-- S\u00fcsteemi kirjeldab peamisi p\u00f5hifunktsioone (kompileeritabaid skeleton) kohta map.apps."
	},
	templatelayout : {
		bundleName : "Vaata Layout",
		bundleDescription : "--CORE FUNKTSIONAALSUSE-- Mall Layout rakendab paigutus k\u00f5ik kasutajaliidese elemendid, mis p\u00f5hineb eelnevalt m\u00e4\u00e4ratud malle."
	},
	templates : {
		bundleName : "Vaade",
		bundleDescription : "--CORE FUNKTSIONAALSUSE-- Views kasutavad Mall paigutus.",
		templates : {
			desktop : {
				title : "Lauaarvuti",
				desc : "Desktop orienteeritud paigutuse."
			},
			modern : {
				title : "Kaasaegne",
				desc : "Kaasaegne stiilne kujundus."
			},
			minimal : {
				title : "Minimal",
				desc : "Minimalistlik kujundus"
			}
		},
		ui : {
			selectorLabelTitle : "Vaade"
		}
	},
	themes : {
		bundleName : "Stiil",
		bundleDescription : "--CORE FUNKTSIONAALSUSE-- Teemad kimp juhib k\u00f5iki CSS infot nagu v\u00e4rvid, fondistiile, taustapilte jms Kasutajad saavad valida erinevate v\u00e4limus kasutajaliidese elemente valides erinevaid stiile.",
		themes : {
			pure : {
				desc : 'Map.apps "puhas" stiilis.'
			},
			night : {
				desc : 'Map.apps "\u00f5htu" stiilis.'
			}
		},
		ui : {
			selectorLabelTitle : "Stiil"
		}
	},
	windowmanager : {
		bundleName : "Juhtimine aknas",
		bundleDescription : "--CORE FUNKTSIONAALSUSE-- Aknahaldur haldamise eest vastutab k\u00f5igi akna.",
		ui : {
			defaultWindowTitle : "Aken",
			closeBtn : {
				title : "L\u00e4hedal"
			},
			minimizeBtn : {
				title : "Minimeerima"
			},
			maximizeBtn : {
				title : "Maksimeerima"
			},
			restoreBtn : {
				title : "Taastama"
			},
			opacityBtn : {
				title : "Muuda l\u00e4bipaistvus"
			},
			collapseBtn : {
				title : "Peida sisu"
			},
			loading : {
				title : "Palun oodake!",
				message : "Laadimine ..."
			},
			okcancel : {
				title : "K\u00fcsimus",
				okButton : "Korras",
				cancelButton : "T\u00fchistama"
			}
		}
	},
	ec_legend : {
		bundleName : "Legend",
		bundleDescription : "See kimp annab Esri legend.",
		tool : {
			title : "Legend",
			tooltip : "Legend"
		}
	},
	ec_parameter_interceptor : {
		bundleName : "ec-parameter-interceptor",
		bundleDescription : "ec-parameter-interceptor",
		errors : {
			missingQueryParameter : "Missing query parameters",
			missingTypeQueryParameter : "Missing type query parameter",
			missingDatasetOrUrlQueryParameter : "Missing dataset or url query parameter",
			noComponentContextProvided : "No componentContext provided",
			noBundleContextProvided : "No bundleContext provided",
			noQueryParametersProvided : "No query parameters provided",
			datasetTypeNotSupported : "The type '${type}' is not supported",
			invalidGeoJSON : "Invalid GeoJSON",
			unsupportedCRSType : "Only named CRS are supported",
			canNotParseNamedCRS : "Can not parse named CRS to WKID: ${name}",
			mixingOfDifferentCRS : "Mixing of different CRS is not supported",
			invalidGeoJSONType : "Unknown GeoJSON type attribute: ${type}",
			typeNotFoundInResource : "No appropriate format for ${type} found in resource",
			couldNotLoadDataset : "Could not load dataset: ${cause}",
			unableToAddDataset : "Unable to add dataset: ${cause}",
			unableToParseCapabilities : "Unable to parse WMS Capabilities response",
			canNotDetermineUrl : "Could not determine the download URL of the resource"
		}
	},
	ec_feature_info : {
		tool : {
			title : "Funktsiooni teave",
			tooltip : "Funktsiooni teave"
		}
	},
	ec_map_loading_screen : {
		bundleName : "ec-map-loading-screen",
		bundleDescription : "ec-map-loading-screen",
		messages : {
			initializingMap : "Kaardi l\u00E4htestamine. Palun oodake!"
		}
	},
	ec_wms_layer_selector : {
		bundleName : "ec-wms-layer-selector",
		bundleDescription : "ec-wms-layer-selector",
		ui : {
			windowTitle : "Vali kihid",
			hint : "Valitud WMS-teenuses on ${countToProvideSelection} kihti. Valige k\u00F5ik kihid, mida tuleks kaardil kuvada.",
			selectAll : "Vali k\u00F5ik",
			closeButton : "Sobib"
		},
		tool : {
			title : "Kiht",
			tooltip : "Kiht"
		}
	},
	ec_error_messages : {
		bundleName : "ec-error-messages",
		bundleDescription : "ec-error-messages",
		general : {
			serviceNotAvailable : "Soovitud teenus pole saadaval",
			serviceNotSupported : "Soovitud teenust ei toetata",
			invalidResource : "Antud URLiga leiti kehtetu ressurss",
			unsupportedResource : "Antud URLiga leitud ressurssi ei toetata",
			errorSource : "Tehti kindlaks j\u00E4rgmine veaallikas",
			requestedUrl : "Taotletud URL",
			unsupportedServiceType : "The provided service type is not supported",
			technicalSupport : "Kui probleem edaspidi kordub, p\u00F6\u00F6rduge tehnilise toe poole.",
			technicalSupportCreateTicket : "Create support ticket",
			weWillContactProvider : "We will contact the provider of the service and report the issue.",
			closeButton : "Sobib"
		},
		detailedErrors : {
			crs : "Teenus ei paku toetatavat koordinaats\u00FCsteemi.",
			encoding : "Teenus ei paku toetatavat m\u00E4rgikodeeringut.",
			metadata : "Teenuse metaandmete dokumendis on vigu.",
			fileFormat : "Antud URLiga saadi kehtetu andmevorming.",
			dataRequest : "Teenuse andmete p\u00E4rimisel saadi kehtetu vastus. Selle p\u00F5hjused v\u00F5ivad olla n\u00E4iteks sisemine serveri viga, teenuse vale konfiguratsioon, teenuse k\u00E4itumine standardile mittevastavalt jne."
		},
		httpIssues : {
			303 : "Vastus taotlusele leidub teise URI all.",
			305 : "Taotletud ressurss on saadaval ainult puhverserveri kaudu.",
			400 : "Server ei saa v\u00F5i ei ole valmis taotlust t\u00F6\u00F6tlema n\u00E4iliselt kliendi vea t\u00F5ttu.",
			401 : "Taotletud ressurss n\u00F5udis luba.",
			403 : "Taotletud ressurss pole avalikult saadaval.",
			404 : "Taotletud ressurssi ei leitud.",
			405 : "Ressurssi taotleti lubamatu HTTP-meetodi kaudu.",
			406 : "Ressursilt taotleti vastuv\u00F5etamatut sisu.",
			407 : "Ressurss n\u00F5uab eelnevat autentimist puhvri teenuse juures.",
			500 : "Ressurss ei ole saadaval sisemise serveri vea t\u00F5ttu.",
			501 : "Ressurss ei ole saadaval tuvastamata taotlusmeetodi t\u00F5ttu.",
			502 : "Ressurss ei ole saadaval valesti konfigureeritud l\u00FC\u00FCsi t\u00F5ttu.",
			503 : "Ressurss ei ole praegu saadaval \u00FClekoormuse v\u00F5i hoolduse t\u00F5ttu.",
			504 : "Ressurss ei ole saadaval valesti konfigureeritud v\u00F5i mittevastava l\u00FC\u00FCsi t\u00F5ttu.",
			505 : "Ressurss ei toeta seda HTTP versiooni.",
			generic : "Antud ressursi taotlemisel ilmnes serveri viga."
		}
	},
	ec_user_tutorial : {
		bundleName : "ec-user-tutorial",
		bundleDescription : "ec-user-tutorial",
		externalResourceDisclaimer : {
			introduction : "See kaardivaatur p\u00F6\u00F6rdub v\u00E4liste teenuste poole ning kuvab neilt saadud andmed. Euroopa Komisjon ei halda neid v\u00E4liseid teenuseid ja seet\u00F5ttu ei saa me m\u00F5jutada nende k\u00E4ttesaadavust/t\u00F6\u00F6kindlust. Esineda v\u00F5ib j\u00E4rgmist laadi t\u00F5rkeid.",
			rotatingCircle : "Kaardivaatel kuvatav p\u00F6\u00F6rlev ring n\u00E4itab, et vaatur ootab andmeallika vastust. Seda v\u00F5ivad p\u00F5hjustada juurdep\u00E4\u00E4su- v\u00F5i v\u00F5rguprobleemid.",
			layerZoomLevel : "Teataval suurendusastmel ei pruugita m\u00F5ne kihi allikat kuvada. Selle p\u00F5hjuseks on v\u00E4lise teenusepakkuja seatud piirangud. Kihi andmete kuvamiseks tuleb teil v\u00F5ib-olla suurendust suurendada v\u00F5i v\u00E4hendada.",
			layerPanning : "V\u00E4lisserverist saadavad andmed v\u00F5ivad asuda praegusest kaardivaatest v\u00E4ljaspool. See kaardivaatur p\u00FC\u00FCab automaatselt asjakohase ala kindlaks teha. Siiski v\u00F5ib juhtuda, et metaandmed on mittet\u00E4ielikud v\u00F5i ekslikud ja teil on vaja tegelikule alale panoraamida.",
			detailInformation : "Valitud objekti kohta \u00FCksikasjaliku teabe taotlemisel suunatakse eritaotlus kaardiserverisse. Kui vastus sellele taotlusele on vigane v\u00F5i t\u00FChi (nt serveri t\u00F5rke t\u00F5ttu), siis ei saa teavet kuvada ja avatakse t\u00FChi aken.",
			mapScaling : "Selles rakenduses kasutatakse Euroopa Liidu Statistikaameti (Eurostat) aluskaarte. Nende kaartide m\u00F5\u00F5tkava on praegu kuni 1:50000. Praegu koostatakse suurema m\u00F5\u00F5tkavaga aluskaarte (nt linnasiseste andmete visualiseerimiseks). Seet\u00F5ttu v\u00F5ib juhtuda, et k\u00F5iki andmeallikaid ei saa veel parimal taustkaardil kuvada.",
			technicalSupport : "Muude probleemide korral p\u00F6\u00F6rduge kindlasti tehnilise toe poole:",
			technicalSupportContactLink : "P\u00F6\u00F6rduge tehnilise toe poole",
			mapLoading : "Kaarti laaditakse...",
			donotshow : "\u00C4ra enam n\u00E4ita seda tervituskuva."
		},
		legalNotice : "Sellel kaardil kasutatud t\u00E4hised ja materjali esitus ei t\u00E4henda mis tahes Euroopa Liidu poolset seisukoha v\u00E4ljendust mingi riigi, territooriumi, linna, ala ega nende ametiasutuste \u00F5igusliku seisundi kohta ega seoses nende piiride m\u00E4\u00E4ratlemisega. Kosovo*: k\u00F5nealune m\u00E4\u00E4ratlus ei piira Kosovo staatust k\u00E4sitlevaid seisukohti ning on koosk\u00F5las \u00DCRO Julgeolekun\u00F5ukogu resolutsiooniga 1244/1999 ja Rahvusvahelise Kohtu arvamusega Kosovo iseseisvusdeklaratsiooni kohta. Palestiina*: k\u00F5nealust m\u00E4\u00E4ratlust ei saa lugeda Palestiina riigi tunnustamiseks ja see ei piira liikmesriikide eraldi seisukohti selles k\u00FCsimuses.",
		legalNoticeHeader : "Juriidilisest vastutusest loobumine",
		tutorial : {
			welcome : "Tere! Siin on Euroopa Andmeportaali kaardivaatur!",
			familiarise : "See v\u00E4ike sissejuhatus tutvustab teile kaardivaaturi osi ja v\u00F5imalusi.",
			navigation : "Kaardi panoraamimiseks kl\u00F5psake ja hoidke all vasakut hiirenuppu.",
			zoom : "Need nupud muudavad kaardi suurendusastet. Teise v\u00F5imalusena saate suurendust muuta hiireratta abil.",
			features : "Nende nuppudega saate kasutada t\u00E4iendavaid v\u00F5imalusi. Nendega saab selle v\u00F5imaluse sisse v\u00F5i v\u00E4lja l\u00FClitada.",
			legend : "Legendi kaudu saab uurida saadaolevaid kaardikihte ning nende kaardil kuvamist sisse v\u00F5i v\u00E4lja l\u00FClitada. Nimed saadakse otse v\u00E4lisest teenusest.",
			transparency : "Samuti saab kihi l\u00E4bipaistvust muuta.",
			featureinfo : "M\u00F5ningaid andmeid saab veel \u00FCksikasjalikumalt uurida. Kui see v\u00F5imalus on sissel\u00FClitatud, saate kaardil kl\u00F5psates lisateavet p\u00E4rida.",
			done : "Tehtud",
			okButton : "Sobib",
			closeButton : "Sule",
			skip : "J\u00E4ta vahele",
			next : "Edasi",
			back : "Tagasi"
		}
	}
});