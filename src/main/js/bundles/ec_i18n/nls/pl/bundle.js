define({
	contentviewer : {
		bundleName : "Content Viewer",
		bundleDescription : "Wy\u015bwietla r\u00f3\u017cne rodzaje tre\u015bci.",
		ui : {
			unknownContentError : "Zawarto\u015b\u0107 jest znane.",
			graphicinfotool : {
				title : "Informacje elementem",
				desc : "Informacje elementem"
			},
			content : {
				defaultTitle : "Element",
				grid : {
					detailView : "Poka\u017c szczeg\u00f3\u0142y",
					key : "Nieruchomo\u015b\u0107",
					value : "Warto\u015b\u0107"
				},
				customTemplate : {
					detailView : "Poka\u017c szczeg\u00f3\u0142y"
				},
				attachment : {
					noAttachmentSupport : "Warstwa ta nie obs\u0142uguje za\u0142\u0105cznik\u00f3w",
					detailView : "Poka\u017c szczeg\u00f3\u0142y"
				},
				AGSDetail : {
					title : "Zobacz szczeg\u00f3\u0142y",
					print : "Druk",
					serviceMetadata : "Metadane us\u0142ugi",
					serviceURL : "URL",
					serviceCopyrights : "Prawa autorskie",
					serviceTitle : "Tytu\u0142",
					pager : {
						pageSizeLabel : "Funkcja currentPage} ${${endPage z}"
					},
					key : "Nieruchomo\u015b\u0107",
					value : "Warto\u015b\u0107",
					detailView : "Poka\u017c szczeg\u00f3\u0142y"
				}
			}
		}
	},
	coordinatetransformer : {
		bundleName : "Transformator wsp\u00f3\u0142rz\u0119dnych",
		bundleDescription : "--Podstawowa funkcjonalno\u015b\u0107-- Transformator zmienia geometrie wsp\u00f3\u0142rz\u0119dnych z jednego uk\u0142adu wsp\u00f3\u0142rz\u0119dnych do innego."
	},
	featureinfo : {
		bundleName : "FeatureInfo",
		bundleDescription : "FeatureInfo wy\u015bwietla informacje na temat funkcji dla aktywnych warstw.",
		ui : {
			featureInfo : {
				noQueryLayersFound : "Nie przeszukiwania warstw dost\u0119pne!",
				contentInfoWindowTitle : "Zidentyfikowa\u0107",
				noResultsFound : "Nie znaleziono \u017cadnych wynik\u00f3w.",
				loadingInfoText : "Tworzenie zapyta\u0144 warstwy ${layerName} (${layerIndex} z ${layersTotal}).",
				featureInfoTool : "Zidentyfikowa\u0107",
				layer : "Warstwa",
				feature : "Obiekt"
			},
			wms : {
				emptyResult : "Nie znaleziono w warstwie \"${} 'WMS layerTitle wyniki."
			}
		}
	},
	geometryservice : {
		bundleName : "GeometryService",
		bundleDescription : "Pakiet zapewnia centraln\u0105 obs\u0142ug\u0119 geometrii."
	},
	geojson : {
		bundleName : "GeoJSON",
		bundleDescription : "Ten pakiet zapewnia us\u0142ugi konwersji, kt\u00f3re mog\u0105 przetwarza\u0107 GeoJSON i dobrze znanego tekstu (WKT)."
	},
	infoviewer : {
		bundleName : "Wy\u015bwietlanie informacji",
		bundleDescription : "Komunikat na wy\u015bwietlaczu pokazuje obs\u0142ugi merytorycznej informacji o miejscu na mapie w oknie.",
		ui : {
			title : "Informacje Viewer",
			tools : {
				closeInfoWindowMapdeskTool : "Blisko",
				closeInfoWindowMapTool : "Blisko",
				focusMapTool : "Centrum w Mapie",
				attachToGeorefTool : "Do\u0142\u0105cz do pozycji",
				mainActivationTool : "Lokalizacja"
			}
		}
	},
	languagetoggler : {
		bundleName : "Prze\u0142\u0105cznik J\u0119zyk",
		bundleDescription : "Poprzez j\u0119zyk interfejsu j\u0119zyka prze\u0142\u0105cznika mo\u017cna zmieni\u0107.",
		ui : {
			title : "J\u0119zyk"
		}
	},
	map : {
		bundleName : "Mapa",
		bundleDescription : "Pakiet Mapa zarz\u0105dza g\u0142\u00f3wn\u0105 map\u0119 i wszystkie informacje o tre\u015bci map. Klient mo\u017ce abstract us\u0142ugi stosowane w hierarchicznego modelu map\u0119 tak, \u017ce mog\u0105 by\u0107 wy\u015bwietlane w r\u00f3\u017cny spos\u00f3b dla u\u017cytkownika.",
		ui : {
			nodeUpdateError : {
				info : "Aktualizacja us\u0142ugi '${title}' nie powiod\u0142o si\u0119! B\u0142\u0105d: ${errorMsg}",
				detail : "Aktualizacja us\u0142ugi '${title}' nie powiod\u0142o si\u0119! B\u0142\u0105d: ${url} ${errorMsg} - Szczeg\u00f3\u0142y: ${error}"
			},
			sliderLabels : {
				country : "Kraj",
				region : "Region",
				town : "Miasto"
			}
		},
		drawTooltips : {
			addPoint : "Kliknij na mapie",
			addMultipoint : "Kliknij, aby rozpocz\u0105\u0107",
			finishLabel : "Wyko\u0144czenie"
		}
	},
	mobiletoc : {
		bundleName : "Kontrola poziomu mobilna",
		bundleDescription : "Ten pakiet zawiera sterownik poziomu mobilna gotowe",
		tool : {
			title : "Mapa zawarto\u015b\u0107",
			tooltip : "W\u0142\u0105czanie / wy\u0142\u0105czanie Mapa zawarto\u015b\u0107"
		},
		ui : {
			basemaps : "Mapy bazowe",
			operationalLayers : "Poziomy temat\u00f3w",
			switchUI : {
				noTitle : "bez tytu\u0142u",
				leftLabel : "Na",
				rightLabel : "Od"
			}
		}
	},
	mobileview : {
		bundleName : "Wersja dla kom\u00f3rek",
		bundleDescription : "Zawiera specjalne zaj\u0119cia, kt\u00f3re rozwi\u0105za\u0107 niekt\u00f3re problemy dotycz\u0105ce dojox.mobile"
	},
	notifier : {
		bundleName : "Powiadomienia",
		bundleDescription : "Wszystkie komunikaty o stanie, post\u0119pu i b\u0142\u0119d\u00f3w s\u0105 wy\u015bwietlane u\u017cytkownikowi w wiadomo\u015bci pop-up, tak aby by\u0142o jasne, co si\u0119 dzieje w aplikacji.",
		ui : {
			title : "Zg\u0142aszaj\u0105cy",
			tooltips : {
				close : "Zamknij wiadomo\u015b\u0107",
				glue : "Do\u0142\u0105cz wiadomo\u015b\u0107"
			}
		}
	},
	parametermanager : {
		bundleName : "Obs\u0142uga parametr\u00f3w",
		bundleDescription : "Parametr Kierownik jest odpowiedzialny delegowa\u0107 parametry z adresu URL do element\u00f3w na starcie wg.",
		ui : {
			encoderBtn : "Narz\u0119dzie link",
			encoderBtnTooltip : "Narz\u0119dzie link",
			sendMail : "E-mail",
			refresh : "Od\u015bwie\u017c",
			linkBoxTitle : "URL Link",
			size : "Rozmiar (w pikselach)",
			codeBoxTitle : "Kod do umieszczenia w kodzie HTML",
			qrCode : "QRCode",
			mailBody : "${url}",
			mailSubject : "Sp\u00f3jrz na czas tej karty!",
			options : {
				small : "ma\u0142e (480 x 320)",
				medium : "no\u015bnik (640 x 480)",
				large : "du\u017ca (1280 x 1024)",
				custom : "zdefiniowane przez u\u017cytkownika"
			}
		}
	},
	printing : {
		bundleName : "Druk",
		bundleDescription : "Ten pakiet zawiera narz\u0119dzie drukowania, aby wydrukowa\u0107 map\u0119.",
		tool : {
			title : "Druk",
			tooltip : "Druk",
			back : "Z powrotem"
		},
		resultWin : {
			title : "Druk"
		}
	},
	qrcode : {
		bundleName : "QRCode",
		bundleDescription : "Ten pakiet zawiera us\u0142ug\u0119, kt\u00f3ra mo\u017ce by\u0107 u\u017cywana do tworzenia QR kod\u00f3w.",
		errorMessage : "Generowanie kodu QR nie powiod\u0142o si\u0119."
	},
	splashscreen : {
		bundleName : "Ekran g\u0142\u00f3wny",
		bundleDescription : "Ekran g\u0142\u00f3wny wy\u015bwietlany jest pasek post\u0119pu, gdy aplikacja jest uruchomiona.",
		loadTitle : 'Uruchom aplikacj\u0119 "{appName}"',
		loadBundle : "{percent}% - {startedBundles}/{maxBundlesToStart} - \u0141adowanie {name}"
	},
	system : {
		bundleName : "System",
		bundleDescription : "--Podstawowa funkcjonalno\u015b\u0107-- system opisuje podstawowe funkcje rdzenia (szkielet) z compilable map.apps."
	},
	templatelayout : {
		bundleName : "Widok uk\u0142adu",
		bundleDescription : "--Podstawowa funkcjonalno\u015b\u0107-- Szablon Uk\u0142ad realizuje uk\u0142ad wszystkich element\u00f3w interfejsu u\u017cytkownika na podstawie zdefiniowanych szablon\u00f3w."
	},
	templates : {
		bundleName : "Widok",
		bundleDescription : "--Podstawowa funkcjonalno\u015b\u0107-- Widoki s\u0105 wykorzystywane przez uk\u0142ad szablonu.",
		templates : {
			desktop : {
				title : "Pulpit",
				desc : "Uk\u0142ad pulpitu zorientowanych."
			},
			modern : {
				title : "Nowoczesny",
				desc : "Nowoczesny stylowy uk\u0142ad."
			},
			minimal : {
				title : "Minimalny",
				desc : "Minimalistyczny layout"
			}
		},
		ui : {
			selectorLabelTitle : "Widok"
		}
	},
	themes : {
		bundleName : "Styl",
		bundleDescription : "--Podstawowa funkcjonalno\u015b\u0107-- pakiet Tematy zarz\u0105dza wszystkie informacje CSS jak kolory, style czcionek, obraz\u00f3w t\u0142a itp U\u017cytkownicy mog\u0105 prze\u0142\u0105cza\u0107 si\u0119 pomi\u0119dzy r\u00f3\u017cnymi wygl\u0105d element\u00f3w interfejsu u\u017cytkownika, wybieraj\u0105c r\u00f3\u017cne style.",
		themes : {
			pure : {
				desc : '"Czysty" Styl map.apps.'
			},
			night : {
				desc : "The map.apps 'noc' stylu."
			}
		},
		ui : {
			selectorLabelTitle : "Styl"
		}
	},
	windowmanager : {
		bundleName : "Okno Zarz\u0105dzanie",
		bundleDescription : "--Podstawowa funkcjonalno\u015b\u0107-- Window manager jest odpowiedzialny za zarz\u0105dzanie okno dialogowe.",
		ui : {
			defaultWindowTitle : "Okno",
			closeBtn : {
				title : "Blisko"
			},
			minimizeBtn : {
				title : "Zminimalizowa\u0107"
			},
			maximizeBtn : {
				title : "Maksymalizacja"
			},
			restoreBtn : {
				title : "Odzyska\u0107"
			},
			opacityBtn : {
				title : "Zmie\u0144 przejrzysto\u015b\u0107"
			},
			collapseBtn : {
				title : "Zawarto\u015b\u0107 Ukryj"
			},
			loading : {
				title : "Prosz\u0119 czeka\u0107!",
				message : "\u0141adowanie ..."
			},
			okcancel : {
				title : "Pytanie",
				okButton : "W porz\u0105dku",
				cancelButton : "Anulowa\u0107"
			}
		}
	},
	ec_legend : {
		bundleName : "Legenda",
		bundleDescription : "This bundle provides the Esri legend.",
		tool : {
			title : "Legenda",
			tooltip : "Legenda"
		}
	},
	ec_parameter_interceptor : {
		bundleName : "ec-parameter-interceptor",
		bundleDescription : "ec-parameter-interceptor",
		errors : {
			missingQueryParameter : "Missing query parameters",
			missingTypeQueryParameter : "Missing type query parameter",
			missingDatasetOrUrlQueryParameter : "Missing dataset or url query parameter",
			noComponentContextProvided : "No componentContext provided",
			noBundleContextProvided : "No bundleContext provided",
			noQueryParametersProvided : "No query parameters provided",
			datasetTypeNotSupported : "The type '${type}' is not supported",
			invalidGeoJSON : "Invalid GeoJSON",
			unsupportedCRSType : "Only named CRS are supported",
			canNotParseNamedCRS : "Can not parse named CRS to WKID: ${name}",
			mixingOfDifferentCRS : "Mixing of different CRS is not supported",
			invalidGeoJSONType : "Unknown GeoJSON type attribute: ${type}",
			typeNotFoundInResource : "No appropriate format for ${type} found in resource",
			couldNotLoadDataset : "Could not load dataset: ${cause}",
			unableToAddDataset : "Unable to add dataset: ${cause}",
			unableToParseCapabilities : "Unable to parse WMS Capabilities response",
			canNotDetermineUrl : "Could not determine the download URL of the resource"
		}
	},
	ec_feature_info : {
		tool : {
			title : "Informacja o elemencie",
			tooltip : "Informacja o elemencie"
		}
	},
	ec_map_loading_screen : {
		bundleName : "ec-map-loading-screen",
		bundleDescription : "ec-map-loading-screen",
		messages : {
			initializingMap : "Trwa pobieranie mapy..."
		}
	},
	ec_wms_layer_selector : {
		bundleName : "ec-wms-layer-selector",
		bundleDescription : "ec-wms-layer-selector",
		ui : {
			windowTitle : "Wybierz poziomy",
			hint : "Wybrana us\u0142uga WMS zawiera ${countToProvideSelection} poziom\u00f3w, prosz\u0119 wybra\u0107 wszystkie poziomy, kt\u00f3re maj\u0105 zosta\u0107 wy\u015bwietlone na mapie.",
			selectAll : "Wybierz wszystko",
			closeButton : "OK"
		},
		tool : {
			title : "Poziom\u00f3w",
			tooltip : "Poziom\u00f3w"
		}
	},
	ec_error_messages : {
		bundleName : "ec-error-messages",
		bundleDescription : "ec-error-messages",
		general : {
			serviceNotAvailable : "\u017b\u0105dany serwis jest niedost\u0119pny",
			serviceNotSupported : "\u017b\u0105dany serwis jest nieobs\u0142ugiwany",
			invalidResource : "Pod podanym adresem URL znaleziono nieprawid\u0142owe zasoby",
			unsupportedResource : "Pod podanym adresem URL znaleziono nieobs\u0142ugiwane zasoby",
			errorSource : "Stwierdzono nast\u0119puj\u0105ce \u017ar\u00f3d\u0142o b\u0142\u0119du",
			requestedUrl : "\u017b\u0105dany adres URL",
			unsupportedServiceType : "\u017b\u0105dany serwis jest nieobs\u0142ugiwany",
			technicalSupport : "Je\u017celi problem pojawi si\u0119 ponownie, prosz\u0119 skontaktowa\u0107 si\u0119 z pomoc\u0105 techniczn\u0105",
			technicalSupportCreateTicket : "Kontakt z pomoc\u0105 techniczn\u0105",
			weWillContactProvider : "Skontaktujemy si\u0119 z dostawc\u0105 serwisu i poinformujemy o problemie.",
			closeButton : "OK"
		},
		detailedErrors : {
			crs : "Serwis nie zapewnia obs\u0142ugiwanego uk\u0142adu odniesienia wsp\u00f3\u0142rz\u0119dnych",
			encoding : "Serwis nie zapewnia obs\u0142ugiwanego kodowania znak\u00f3w",
			metadata : "Dokument metadanych serwisu zawiera b\u0142\u0105d",
			fileFormat : "Pod podanym adresem URL znaleziono nieprawid\u0142owy format danych",
			dataRequest : "Podczas przeszukiwania danych serwisu otrzymano nieprawid\u0142ow\u0105 odpowied\u017a. Przyczyn\u0105 mo\u017ce by\u0107 np.: wewn\u0119trzny b\u0142\u0105d serwera, nieprawid\u0142owa konfiguracja serwisu, niezgodne ze standardami zachowanie serwisu itp."
		},
		httpIssues : {
			303 : "Odpowied\u017a na \u017c\u0105danie mo\u017cna znale\u017a\u0107 pod innym URI",
			305 : "\u017b\u0105dane zasoby dost\u0119pne s\u0105 tylko przez serwer proxy",
			400 : "Serwer nie mo\u017ce przetworzy\u0107 \u017c\u0105dania z powodu stwierdzonego b\u0142\u0119du po stronie klienta",
			401 : "\u017b\u0105dane zasoby wymagaj\u0105 autoryzacji",
			403 : "\u017b\u0105dane zasoby nie s\u0105 publicznie dost\u0119pne",
			404 : "\u017b\u0105dane zasoby nie zosta\u0142y odnalezione",
			405 : "\u017b\u0105danie zasob\u00f3w przekazano przez niedozwolon\u0105 metod\u0119 HTTP",
			406 : "Za\u017c\u0105dano z zasob\u00f3w niedopuszczaln\u0105 tre\u015b\u0107",
			407 : "Zasoby wymagaj\u0105 uprzedniego uwierzytelnienia przez us\u0142ug\u0119 proxy",
			500 : "Zasoby s\u0105 niedost\u0119pne z powodu wewn\u0119trznego b\u0142\u0119du serwera",
			501 : "Zasoby s\u0105 niedost\u0119pne z powodu nierozpoznanej metody \u017c\u0105dania",
			502 : "Zasoby s\u0105 niedost\u0119pne z powodu b\u0142\u0119dnie skonfigurowanej bramki",
			503 : "Zasoby s\u0105 obecnie niedost\u0119pne z powodu przeci\u0105\u017cenia lub prac konserwacyjnych",
			504 : "Zasoby s\u0105 niedost\u0119pne z powodu b\u0142\u0119dnie skonfigurowanej lub nieodpowiadaj\u0105cej bramki",
			505 : "Zasoby nie obs\u0142uguj\u0105 tej wersji HTTP",
			generic : "Podczas \u017c\u0105dania danych zasob\u00f3w nast\u0105pi\u0142 b\u0142\u0105d serwera"
		}
	},
	ec_user_tutorial : {
		bundleName : "ec-user-tutorial",
		bundleDescription : "ec-user-tutorial",
		externalResourceDisclaimer : {
			introduction : "Przegl\u0105darka mapy pozwala uzyska\u0107 dost\u0119p do zewn\u0119trznych zasob\u00f3w i wy\u015bwietla\u0107 ich dane. Zasoby te nie s\u0105 administrowane przez Komisj\u0119 Europejsk\u0105 i dlatego nie ma ona wp\u0142ywu na ich dost\u0119pno\u015b\u0107/stabilno\u015b\u0107. Mog\u0105 pojawia\u0107 si\u0119 nast\u0119puj\u0105ce problemy:",
			rotatingCircle : "Obracaj\u0105ce si\u0119 k\u00f3\u0142ko w widoku mapy oznacza, \u017ce przegl\u0105darka czeka na odpowied\u017a \u017ar\u00f3d\u0142a danych. Mo\u017ce to by\u0107 spowodowane niedost\u0119pno\u015bci\u0105 serwera albo problemami z sieci\u0105.",
			layerZoomLevel : "\u0179r\u00f3d\u0142a niekt\u00f3rych poziom\u00f3w mog\u0105 nie wy\u015bwietla\u0107 si\u0119 przy niekt\u00f3rych stopniach powi\u0119kszenia. Wynika to z ogranicze\u0144 ustalonych przez dostawc\u0119 zewn\u0119trznego serwisu. Aby m\u00f3c zobaczy\u0107 dane tego poziomu, wymagane mo\u017ce by\u0107 zwi\u0119kszenie b\u0105d\u017a zmniejszenie przybli\u017cenia.",
			layerPanning : "Dane dost\u0119pne na zewn\u0119trznym serwerze mog\u0105 znajdowa\u0107 si\u0119 poza aktualnym widokiem mapy. Przegl\u0105darka mapy automatycznie podejmuje pr\u00f3by zidentyfikowania odpowiedniego obszaru. Tym niemniej w niekt\u00f3rych przypadkach metadane s\u0105 niepe\u0142ne lub b\u0142\u0119dne i mo\u017ce by\u0107 wymagane przesuni\u0119cie do prawid\u0142owego miejsca.",
			detailInformation : "\u017b\u0105dane szczeg\u00f3\u0142owych informacji na temat wybranej funkcji obs\u0142ugiwane jest za pomoc\u0105 \u017c\u0105dania wysy\u0142anego do serwera mapy. Je\u017celi odpowied\u017a na to \u017c\u0105danie b\u0119dzie zniekszta\u0142cona lub pusta (np. z powodu b\u0142\u0119du serwera), nie wy\u015bwietli si\u0119 \u017cadna informacja i pojawi si\u0119 puste okienko.",
			technicalSupport : "W przypadku innych problem\u00f3w, prosimy kontaktowa\u0107 si\u0119 z pomoc\u0105 techniczn\u0105:",
			technicalSupportContactLink : "Kontakt z pomoc\u0105 techniczn\u0105",
			donotshow : "Nie wy\u015bwietlaj ponownie ekranu powitalnego.",
			mapLoading : "Trwa pobieranie mapy...",
			mapScaling : "Mapy bazowe wykorzystywane w tej aplikacji dostarczone zosta\u0142y przez urz\u0105d statystyczny Unii Europejskiej (Eurostat). Obecnie dost\u0119pne s\u0105 mapy w skali do 1:50\u00a0000. Trwaj\u0105 prace nad mapami w wy\u017cszej rozdzielczo\u015bci (np. umo\u017cliwiaj\u0105cymi wizualizacj\u0119 danych w okre\u015blonym mie\u015bcie). Dlatego te\u017c niekt\u00f3re \u017ar\u00f3d\u0142a danych mog\u0105 na chwil\u0119 obecn\u0105 nie wy\u015bwietla\u0107 si\u0119 na optymalnie dostosowanej mapie bazowej."
		},
		legalNotice : "Zastosowane oznaczenia oraz spos\u00f3b prezentowania materia\u0142\u00f3w na mapie nie oznaczaj\u0105 wyra\u017cenia jakiejkolwiek opinii przez Uni\u0119 Europejsk\u0105 w sprawie statusu prawnego kt\u00f3regokolwiek z kraj\u00f3w, terytori\u00f3w, miast czy obszar\u00f3w b\u0105d\u017a ich w\u0142adz, ani te\u017c w sprawie wyznaczenia ich granic. Kosowo*: Oznaczenie to stosowane jest bez uszczerbku dla stanowisk dotycz\u0105cych statusu i jest zgodne z rezolucj\u0105 Rady Bezpiecze\u0144stwa ONZ (UNSCR) 1244/1999 oraz opini\u0105 Mi\u0119dzynarodowego Trybuna\u0142u Sprawiedliwo\u015bci na temat deklaracji niepodleg\u0142o\u015bci Kosowa. Palestyna*: Oznaczenie to nie mo\u017ce by\u0107 interpretowane jako uznanie Pa\u0144stwa Palesty\u0144skiego i stosowane jest bez uszczerbku dla stanowisk poszczeg\u00f3lnych Pa\u0144stw Cz\u0142onkowskich w tej sprawie.",
		legalNoticeHeader : "Informacja prawna",
		tutorial : {
			welcome : "Witamy w przegl\u0105darce mapy Europejskiego Portalu Danych",
			familiarise : "Niniejszy kr\u00f3tki wst\u0119p ma na celu zapoznanie u\u017cytkownika z elementami i funkcjami przegl\u0105darki mapy.",
			navigation : "Aby przesun\u0105\u0107 map\u0119, kliknij i przytrzymaj lewy przycisk myszy.",
			zoom : "Ten przycisk pozwala zmieni\u0107 powi\u0119kszenie mapy. Mo\u017cna te\u017c u\u017cy\u0107 k\u00f3\u0142ka myszy, aby zmieni\u0107 powi\u0119kszenie mapy.",
			features : "Te przyciski umo\u017cliwiaj\u0105 skorzystanie z dodatkowych funkcji. Dzia\u0142aj\u0105 jak prze\u0142\u0105czniki, kt\u00f3re w\u0142\u0105czaj\u0105 i wy\u0142\u0105czaj\u0105 dan\u0105 funkcj\u0119.",
			legend : "Legenda mo\u017ce pos\u0142u\u017cy\u0107 do sprawdzania dost\u0119pnych poziom\u00f3w mapy i w\u0142\u0105czania oraz wy\u0142\u0105czania ich wy\u015bwietlania na mapie. Nazwy s\u0105 pobierane bezpo\u015brednio z serwis\u00f3w zewn\u0119trznych.",
			transparency : "Mo\u017cna tak\u017ce ustawi\u0107 przezroczysto\u015b\u0107 poziomu.",
			featureinfo : "Niekt\u00f3re dane mo\u017cna przegl\u0105da\u0107 jeszcze bardziej szczeg\u00f3\u0142owo. Kiedy w\u0142\u0105czona jest ta funkcja, mo\u017cna klika\u0107 na mapie, aby za\u017c\u0105da\u0107 dodatkowych informacji.",
			okButton : "OK",
			closeButton : "Zamknij",
			next : "Nast\u0119pna",
			back : "Poprzednia",
			skip : "Pomi\u0144",
			done : "Gotowe"
		}
	}
});