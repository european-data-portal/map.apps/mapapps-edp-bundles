define({
	contentviewer : {
		bundleName : "Content Viewer",
		bundleDescription : "Displays different kinds of content.",
		ui : {
			unknownContentError : "The content is unknown.",
			graphicinfotool : {
				title : "Item Info",
				desc : "Item Info"
			},
			content : {
				defaultTitle : "Item",
				grid : {
					detailView : "Show details",
					key : "Property",
					value : "Value"
				},
				customTemplate : {
					detailView : "Show details"
				},
				attachment : {
					noAttachmentSupport : "This layer does not offer attachment support",
					detailView : "Open detail view"
				},
				AGSDetail : {
					title : "Detail View",
					print : "Print",
					serviceMetadata : "Service metadata",
					serviceURL : "URL",
					serviceCopyrights : "Copyrights",
					serviceTitle : "Title",
					pager : {
						pageSizeLabel : "Feature ${currentPage} of ${endPage}"
					},
					key : "Property",
					value : "Value",
					detailView : "Show details"
				}
			}
		}
	},
	coordinatetransformer : {
		bundleName : "Coordinate Transformer",
		bundleDescription : "--CORE FUNCTIONALITY-- The Coordinate Transformer transforms geometries from a source coordinate system to a target coordinate system."
	},
	featureinfo : {
		bundleName : "FeatureInfo",
		bundleDescription : "FeatureInfo displays information on features for active layers.",
		ui : {
			featureInfo : {
				noQueryLayersFound : "No queryable layers found!",
				contentInfoWindowTitle : "Identify",
				noResultsFound : "No results found.",
				loadingInfoText : "Querying layer ${layerName} (${layerIndex} of ${layersTotal}).",
				featureInfoTool : "Identify",
				layer : "Layer",
				feature : "Feature"
			},
			wms : {
				emptyResult : "No results found on the WMS layer '${layerTitle}'."
			}
		}
	},
	geometryservice : {
		bundleName : "GeometryService",
		bundleDescription : "The bundle provides a central geometry service."
	},
	geojson : {
		bundleName : "GeoJson",
		bundleDescription : "This bundle provides a conversion service for geojson and well-known text formats"
	},
	infoviewer : {
		bundleName : "Info Viewer",
		bundleDescription : "The Info Viewer shows content information for a location in a moveable and resizable window.",
		ui : {
			title : "Info Viewer",
			tools : {
				closeInfoWindowMapdeskTool : "Close",
				closeInfoWindowMapTool : "Close",
				focusMapTool : "Center in Map",
				attachToGeorefTool : "Attach to position",
				mainActivationTool : "Location Information"
			}
		}
	},
	languagetoggler : {
		bundleName : "Language Toggler",
		bundleDescription : "The language of the user interface can be switched by a language toggler.",
		ui : {
			title : "Language"
		}
	},
	map : {
		bundleName : "Map",
		bundleDescription : "The Map bundle manages the main map and all map content information. The client can abstract the services used in a hierarchical Map model so that they can be displayed in various ways to the user.",
		ui : {
			nodeUpdateError : {
				info : "Update of service '${title}' failed! Msg: ${errorMsg}",
				detail : "Update of service '${title}' failed! Msg: ${url} ${errorMsg} - Details: ${error}"
			},
			sliderLabels : {
				country : "Country",
				region : "Region",
				town : "Town"
			}
		},
		drawTooltips : {
			addPoint : "Click in the map",
			addMultipoint : "Click to start",
			finishLabel : "Finish"
		}
	},
	mobiletoc : {
		bundleName : "Mobile TOC",
		bundleDescription : "This bundle provides a mobile table of contents",
		tool : {
			title : "Map Content",
			tooltip : "Turn on/off Map Content"
		},
		ui : {
			basemaps : "Base maps",
			operationalLayers : "Operational layers",
			switchUI : {
				noTitle : "no title",
				leftLabel : "On",
				rightLabel : "Off"
			}
		}
	},
	mobileview : {
		bundleName : "Mobile View",
		bundleDescription : "Contains special classes that fix some issues concerning dojox.mobile"
	},
	notifier : {
		bundleName : "Notifier",
		bundleDescription : "All messages on status, progress, errors and warnings are shown in a popup message so that a user can understand what is going on in the application.",
		ui : {
			title : "Notifier",
			tooltips : {
				close : "Close this message",
				glue : "Pin this message"
			}
		}
	},
	parametermanager : {
		bundleName : "Parameter Manager",
		bundleDescription : "The Parameter Manager is responsible to delegate parameters from the URL to the according components on startup.",
		ui : {
			encoderBtn : "Link tool",
			encoderBtnTooltip : "Link tool",
			sendMail : "EMail",
			refresh : "Refresh",
			linkBoxTitle : "Link URL",
			size : "Size (Pixels)",
			codeBoxTitle : "Code to embed in HTML",
			qrCode : "QRCode",
			mailBody : "${url}",
			mailSubject : "Check out this map!",
			options : {
				small : "small (480 x 320)",
				medium : "medium (640 x 480)",
				large : "large (1280 x 1024)",
				custom : "custom"
			}
		}
	},
	printing : {
		bundleName : "Print",
		bundleDescription : "This bundle provides a print tool to print the map.",
		tool : {
			title : "Print",
			tooltip : "Print",
			back : "Back"
		},
		resultWin : {
			title : "Print"
		}
	},
	qrcode : {
		bundleName : "QRCode",
		bundleDescription : "This bundle provides a service that can be used to create QR codes.",
		errorMessage : "QR Code can not be generated."
	},
	splashscreen : {
		bundleName : "Splash Screen",
		bundleDescription : "The Splash Screen shows a progress bar and a logo (e.g. the map.apps logo) and/or a text while starting the application.",
		loadTitle : "Starting Application '{appName}'",
		loadBundle : "{percent}% - {startedBundles}/{maxBundlesToStart} - Load {name} "
	},
	system : {
		bundleName : "System",
		bundleDescription : "--CORE FUNCTIONALITY-- The System describes the basic core functionality (compilable skeleton) of map.apps."
	},
	templatelayout : {
		bundleName : "Template Layout",
		bundleDescription : "--CORE FUNCTIONALITY-- The Template Layout implements the arrangement of all UI elements based on predefined templates."
	},
	templates : {
		bundleName : "View",
		bundleDescription : "--CORE FUNCTIONALITY-- Views are used by the Template Layout.",
		templates : {
			desktop : {
				title : "Desktop",
				desc : "The desktop style layout"
			},
			modern : {
				title : "Modern",
				desc : "A modern layout"
			},
			minimal : {
				title : "Minimal",
				desc : "A minimal layout"
			}
		},
		ui : {
			selectorLabelTitle : "View"
		}
	},
	themes : {
		bundleName : "Style",
		bundleDescription : "--CORE FUNCTIONALITY-- The Themes bundle manages all CSS information like colors, font styles, background images etc. Users can switch between different looks of the user interface elements by selecting different styles.",
		themes : {
			pure : {
				desc : "The map.apps 'pure' style."
			},
			night : {
				desc : "The map.apps 'night' style."
			}
		},
		ui : {
			selectorLabelTitle : "Style"
		}
	},
	windowmanager : {
		bundleName : "Window Manager",
		bundleDescription : "--CORE FUNCTIONALITY-- The Window Manager is responsible to manage all floating windows.",
		ui : {
			defaultWindowTitle : "Window",
			closeBtn : {
				title : "Close"
			},
			minimizeBtn : {
				title : "Minimize"
			},
			maximizeBtn : {
				title : "Maximize"
			},
			restoreBtn : {
				title : "Restore"
			},
			opacityBtn : {
				title : "Change transparency"
			},
			collapseBtn : {
				title : "Hide content"
			},
			loading : {
				title : "Please wait!",
				message : "Loading..."
			},
			okcancel : {
				title : "Question",
				okButton : "OK",
				cancelButton : "Cancel"
			}
		}
	},
	ec_legend : {
		bundleName : "Legenda",
		bundleDescription : "This bundle provides the Esri legend.",
		tool : {
			title : "Legenda",
			tooltip : "Legenda"
		}
	},
	ec_parameter_interceptor : {
		bundleName : "ec-parameter-interceptor",
		bundleDescription : "ec-parameter-interceptor",
		errors : {
			missingQueryParameter : "Missing query parameters",
			missingTypeQueryParameter : "Missing type query parameter",
			missingDatasetOrUrlQueryParameter : "Missing dataset or url query parameter",
			noComponentContextProvided : "No componentContext provided",
			noBundleContextProvided : "No bundleContext provided",
			noQueryParametersProvided : "No query parameters provided",
			datasetTypeNotSupported : "The type '${type}' is not supported",
			invalidGeoJSON : "Invalid GeoJSON",
			unsupportedCRSType : "Only named CRS are supported",
			canNotParseNamedCRS : "Can not parse named CRS to WKID: ${name}",
			mixingOfDifferentCRS : "Mixing of different CRS is not supported",
			invalidGeoJSONType : "Unknown GeoJSON type attribute: ${type}",
			typeNotFoundInResource : "No appropriate format for ${type} found in resource",
			couldNotLoadDataset : "Could not load dataset: ${cause}",
			unableToAddDataset : "Unable to add dataset: ${cause}",
			unableToParseCapabilities : "Unable to parse WMS Capabilities response",
			canNotDetermineUrl : "Could not determine the download URL of the resource"
		}
	},
	ec_feature_info : {
		tool : {
			title : "Informace o prvku",
			tooltip : "Informace o prvku"
		}
	},
	ec_map_loading_screen : {
		bundleName : "ec-map-loading-screen",
		bundleDescription : "ec-map-loading-screen",
		messages : {
			initializingMap : "Mapa se na\u010D\u00EDt\u00E1..."
		}
	},
	ec_wms_layer_selector : {
		bundleName : "ec-wms-layer-selector",
		bundleDescription : "ec-wms-layer-selector",
		ui : {
			windowTitle : "Vyberte vrstvy",
			hint : "Vybran\u00E1 slu\u017Eba WMS obsahuje ${countToProvideSelection} vrstvu, vyberte pros\u00EDm v\u0161echny vrstvy, kter\u00E9 maj\u00ED b\u00FDt na map\u011B zobrazeny.",
			selectAll : "Vybrat v\u0161e",
			closeButton : "OK"
		},
		tool : {
			title : "Vrstva",
			tooltip : "Vrstva"
		}
	},
	ec_error_messages : {
		bundleName : "ec-error-messages",
		bundleDescription : "ec-error-messages",
		general : {
			serviceNotAvailable : "Po\u017Eadovan\u00E1 slu\u017Eba nen\u00ED dostupn\u00E1",
			serviceNotSupported : "Po\u017Eadovan\u00E1 slu\u017Eba nen\u00ED podporov\u00E1na",
			invalidResource : "Na doty\u010Dn\u00E9 adrese URL byl nalezen neplatn\u00FD zdroj",
			unsupportedResource : "Na doty\u010Dn\u00E9 adrese URL byl nalezen nepodporovan\u00FD zdroj",
			errorSource : "Byl nalezen n\u00E1sleduj\u00EDc\u00ED zdroj chyby",
			requestedUrl : "Po\u017Eadovan\u00E1 adresa URL",
			unsupportedServiceType : "Poskytovan\u00E9 typ slu\u017Eby nen\u00ED podporov\u00E1n",
			technicalSupport : "Pokud probl\u00E9m bude do budoucna p\u0159etrv\u00E1vat, kontaktujte technickou podporu",
			technicalSupportCreateTicket : "Create support ticket",
			weWillContactProvider : "Poskytovatele slu\u017Eby kontaktujeme a probl\u00E9m nahl\u00E1s\u00EDme.",
			closeButton : "OK"
		},
		detailedErrors : {
			crs : "Slu\u017Eba neposkytuje podporovan\u00FD referen\u010Dn\u00ED sou\u0159adnicov\u00FD syst\u00E9m",
			encoding : "Slu\u017Eba neposkytuje podporovan\u00E9 k\u00F3dov\u00E1n\u00ED znak\u016F",
			metadata : "Dokument t\u00E9to slu\u017Eby s metadaty obsahuje chyby",
			fileFormat : "Na dan\u00E9 adrese URL byl zji\u0161t\u011Bn neplatn\u00FD form\u00E1t dat",
			dataRequest : "P\u0159i dotazov\u00E1n\u00ED dat slu\u017Eby jsme obdr\u017Eeli neplatnou odpov\u011B\u010F. D\u016Fvodem m\u016F\u017Ee b\u00FDt nap\u0159\u00EDklad: vnit\u0159n\u00ED chyba serveru, chybn\u00E1 konfigurace slu\u017Eby, chov\u00E1n\u00ED slu\u017Eby neodpov\u00EDdaj\u00EDc\u00ED standard\u016Fm aj."
		},
		httpIssues : {
			303 : "Odpov\u011B\u010F na po\u017Eadavek lze nal\u00E9zt na jin\u00E9m identifik\u00E1toru URI",
			305 : "Po\u017Eadovan\u00FD zdroj je k dispozici pouze prost\u0159ednictv\u00EDm proxy serveru",
			400 : "Server nem\u016F\u017Ee zpracovat po\u017Eadavek kv\u016Fli zachycen\u00E9 chyb\u011B klienta",
			401 : "Po\u017Eadovan\u00FD zdroj po\u017Eaduje schv\u00E1len\u00ED",
			403 : "Po\u017Eadovan\u00FD zdroj nen\u00ED ve\u0159ejn\u011B dostupn\u00FD",
			404 : "Po\u017Eadovan\u00FD zdroj se nepoda\u0159ilo nal\u00E9zt",
			405 : "Zdroj byl po\u017Eadov\u00E1n prost\u0159ednictv\u00EDm neschv\u00E1len\u00E9 metody HTTP",
			406 : "Ze zdroje byl po\u017Eadov\u00E1n nep\u0159ijateln\u00FD obsah",
			407 : "Zdroj vy\u017Eaduje p\u0159edb\u011B\u017En\u00E9 schv\u00E1len\u00ED proxy slu\u017Eby",
			500 : "Zdroj nen\u00ED dostupn\u00FD kv\u016Fli vnit\u0159n\u00ED chyb\u011B serveru",
			501 : "Zdroj nen\u00ED dostupn\u00FD kv\u016Fli neidentifikovan\u00E9 metod\u011B po\u017Eadov\u00E1n\u00ED",
			502 : "Zdroj nen\u00ED dostupn\u00FD kv\u016Fli chybn\u011B konfigurovan\u00E9 br\u00E1n\u011B",
			503 : "Zdroj moment\u00E1ln\u011B nen\u00ED dostupn\u00FD kv\u016Fli p\u0159et\u00ED\u017Een\u00ED nebo \u00FAdr\u017Eb\u011B",
			504 : "Zdroj nen\u00ED dostupn\u00FD kv\u016Fli chybn\u011B konfigurovan\u00E9 nebo nereaguj\u00EDc\u00ED br\u00E1n\u011B",
			505 : "Zdroj nepodporuje danou verzi HTTP",
			generic : "P\u0159i po\u017Eadov\u00E1n\u00ED dan\u00E9ho zdroje do\u0161lo k chyb\u011B serveru"
		}
	},
	ec_user_tutorial : {
		bundleName : "ec-user-tutorial",
		bundleDescription : "ec-user-tutorial",
		externalResourceDisclaimer : {
			introduction : "Tento prohl\u00ED\u017Ee\u010D map m\u00E1 p\u0159\u00EDstup k extern\u00EDm slu\u017Eb\u00E1m a zobrazuje jejich data. Tyto extern\u00ED slu\u017Eby nespravuje Evropsk\u00E1 komise, a tud\u00ED\u017E nem\u00E1me vliv na jejich dostupnost a stabilitu. M\u016F\u017Ee doj\u00EDt k t\u011Bmto probl\u00E9m\u016Fm:",
			rotatingCircle : "Ot\u00E1\u010Dej\u00EDc\u00ED se kruh na zobrazen\u00E9 map\u011B znamen\u00E1, \u017Ee prohl\u00ED\u017Ee\u010D \u010Dek\u00E1 na odpov\u011B\u010F zdroje dat. M\u016F\u017Ee to b\u00FDt zp\u016Fsobeno nedostupn\u00FDm serverem nebo probl\u00E9my se s\u00EDt\u00ED.",
			layerZoomLevel : "N\u011Bkter\u00E9 zdroje vrstev se p\u0159i ur\u010Dit\u00E9 \u00FArovni p\u0159ibl\u00ED\u017Een\u00ED nemus\u00ED zobrazit. D\u016Fvodem mohou b\u00FDt omezen\u00ED stanoven\u00E1 extern\u00EDm poskytovatelem slu\u017Eby. K zobrazen\u00ED dat t\u00E9to vrstvy mus\u00EDte obsah p\u0159ibl\u00ED\u017Eit nebo odd\u00E1lit.",
			layerPanning : "Data z\u00EDskan\u00E1 z extern\u00EDho serveru se mohou nach\u00E1zet mimo aktu\u00E1ln\u00ED zobrazen\u00ED mapy. Tento prohl\u00ED\u017Ee\u010D mapy se sna\u017E\u00ED automaticky identifikovat p\u0159\u00EDslu\u0161nou oblast. P\u0159esto v n\u011Bkter\u00FDch p\u0159\u00EDpadech nejsou metadata \u00FApln\u00E1 nebo obsahuj\u00ED chyby, tak\u017Ee se mus\u00EDte na p\u0159\u00EDslu\u0161n\u00FD rozsah posunout.",
			detailInformation : "Pokud po\u017Eadujete podrobn\u00E9 informace ke zvolen\u00E9mu prvku, prob\u00EDh\u00E1 to formou zvl\u00E1\u0161tn\u00EDho po\u017Eadavku na server map. Pokud je odpov\u011B\u010F na tento po\u017Eadavek ve \u0161patn\u00E9m tvaru nebo pr\u00E1zdn\u00E1 (nap\u0159. kv\u016Fli chyb\u011B serveru), nelze zobrazit \u017E\u00E1dn\u00E9 informace a objev\u00ED se pr\u00E1zdn\u00E9 okno.",
			mapScaling : "V\u00FDchoz\u00ED mapy pou\u017Eit\u00E9 v t\u00E9to aplikaci poskytuje statistick\u00FD \u00FA\u0159ad Evropsk\u00E9 unie (Eurostat). Tyto mapy jsou aktu\u00E1ln\u011B k dispozici v rozli\u0161en\u00ED a\u017E 1 :\u00A050\u00A0000. Podrobn\u011Bj\u0161\u00ED v\u00FDchoz\u00ED mapy se pr\u00E1v\u011B p\u0159ipravuj\u00ED (nap\u0159. k lep\u0161\u00ED vizualizaci dat o m\u011Bstu). Pro n\u011Bkter\u00E9 zdroje dat tak nemus\u00ED b\u00FDt k dispozici optim\u00E1ln\u011B p\u0159izp\u016Fsoben\u00E1 podkladov\u00E1 mapa.",
			technicalSupport : "Jestli\u017Ee naraz\u00EDte na jak\u00E9koli probl\u00E9my, nev\u00E1hejte kontaktovat technickou podporu:",
			technicalSupportContactLink : "Kontaktovat technickou podporu",
			mapLoading : "Mapa se na\u010D\u00EDt\u00E1...",
			donotshow : "\u00DAvodn\u00ED obrazovku p\u0159\u00ED\u0161t\u011B nezobrazovat."
		},
		legalNotice : "Ozna\u010Den\u00ED pou\u017Eit\u00E1 v t\u00E9to map\u011B a prezentace zobrazen\u00FDch materi\u00E1l\u016F nevyjad\u0159uj\u00ED \u017E\u00E1dn\u00FD n\u00E1zor ze strany Evropsk\u00E9 unie, pokud jde o pr\u00E1vn\u00ED status jak\u00E9koli zem\u011B, \u00FAzem\u00ED, m\u011Bsta, oblasti nebo jejich spr\u00E1vy nebo vymezen\u00ED jejich hranic. Kosovo*: T\u00EDmto ozna\u010Den\u00EDm nejsou dot\u010Deny postoje ke statusu a je v souladu s rezoluc\u00ED Rady bezpe\u010Dnosti OSN \u010D. 1244/1999 a stanoviskem MSD k deklaraci nez\u00E1vislosti Kosova. Palestina*: Toto ozna\u010Den\u00ED nesm\u00ED b\u00FDt ch\u00E1p\u00E1no jako uzn\u00E1n\u00ED st\u00E1tu Palestina a nejsou j\u00EDm dot\u010Deny individu\u00E1ln\u00ED postoje \u010Dlensk\u00FDch st\u00E1t\u016F k tomuto t\u00E9matu.",
		legalNoticeHeader : "Pr\u00E1vn\u00ED upozorn\u011Bn\u00ED",
		tutorial : {
			welcome : "V\u00EDtejte v prohl\u00ED\u017Ee\u010Di map Evropsk\u00E9ho datov\u00E9ho port\u00E1lu!",
			familiarise : "V tomto stru\u010Dn\u00E9m \u00FAvodu v\u00E1s sezn\u00E1m\u00EDme s prvky a funkcemi prohl\u00ED\u017Ee\u010De map.",
			navigation : "K posouv\u00E1n\u00ED na map\u011B klikn\u011Bte lev\u00FDm tla\u010D\u00EDtkem my\u0161i a p\u0159idr\u017Ete je.",
			zoom : "T\u011Bmito tla\u010D\u00EDtky se m\u011Bn\u00ED \u00FArove\u0148 p\u0159ibl\u00ED\u017Een\u00ED mapy. P\u0159ibl\u00ED\u017Een\u00ED m\u016F\u017Eete tak\u00E9 m\u011Bnit kole\u010Dkem my\u0161i.",
			features : "Pomoc\u00ED t\u011Bchto tla\u010D\u00EDtek se dostanete k dal\u0161\u00EDm funkc\u00EDm. Jsou to p\u0159ep\u00EDna\u010De, kter\u00E9 aktivuj\u00ED a deaktivuj\u00ED p\u0159\u00EDslu\u0161nou funkci.",
			legend : "Pomoc\u00ED legendy m\u016F\u017Eete zkoumat dostupn\u00E9 vrstvy mapy a aktivovat \u010Di deaktivovat jejich aktu\u00E1ln\u00ED zobrazen\u00ED na map\u011B. N\u00E1zvy jsou p\u0159\u00EDmo odvozeny z extern\u00ED slu\u017Eby.",
			transparency : "M\u016F\u017Eete si tak\u00E9 upravit pr\u016Fsvitnost vrstev.",
			featureinfo : "N\u011Bkter\u00E9 \u00FAdaje je mo\u017En\u00E9 zkoumat je\u0161t\u011B podrobn\u011Bji. P\u0159i aktivaci t\u00E9to funkce m\u016F\u017Eete klik\u00E1n\u00EDm na mapu zji\u0161\u0165ovat dal\u0161\u00ED informace.",
			done : "Hotovo",
			okButton : "OK",
			closeButton : "Zav\u0159\u00EDt",
			skip : "P\u0159esko\u010Dit",
			next : "Dal\u0161\u00ED",
			back : "P\u0159edchoz\u00ED"
		}
	}
});