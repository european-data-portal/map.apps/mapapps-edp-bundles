define({
	contentviewer : {
		bundleName : "Turinio per\u017ei\u016bros",
		bundleDescription : "Rodo \u012fvairi\u0173 r\u016b\u0161i\u0173 turin\u012f.",
		ui : {
			unknownContentError : "Turinys yra ne\u017einomas.",
			graphicinfotool : {
				title : "Elementas informacijos",
				desc : "Elementas informacijos"
			},
			content : {
				defaultTitle : "Elementas",
				grid : {
					detailView : "Rodyti detales",
					key : "Turtas",
					value : "Vert\u0117"
				},
				customTemplate : {
					detailView : "Rodyti detales"
				},
				attachment : {
					noAttachmentSupport : "\u0160is sluoksnis nepalaiko fail\u0173",
					detailView : "Rodyti detales"
				},
				AGSDetail : {
					title : "I\u0161samiau",
					print : "Spausdinti",
					serviceMetadata : "Paslaugos metaduomen\u0173",
					serviceURL : "URL",
					serviceCopyrights : "Autoryst\u0117s teis\u0117s",
					serviceTitle : "Pavadinimas",
					pager : {
						pageSizeLabel : "\u017danras ${currentPage} i\u0161 ${endPage}"
					},
					key : "Turtas",
					value : "Vert\u0117",
					detailView : "Rodyti detales"
				}
			}
		}
	},
	coordinatetransformer : {
		bundleName : "Koordina\u010di\u0173 transformatorius",
		bundleDescription : "--CORE FUNKCIONALIT\u0100TE-- Koordinuoti transformatorius paver\u010dia geometrija i\u0161 vienos koordina\u010di\u0173 sistemos \u012f kit\u0105."
	},
	featureinfo : {
		bundleName : "FeatureInfo",
		bundleDescription : "FeatureInfo rodoma informacija apie funkcij\u0173 aktyvi\u0173 sluoksni\u0173.",
		ui : {
			featureInfo : {
				noQueryLayersFound : "Nr paie\u0161kos Layer prieinamas!",
				contentInfoWindowTitle : "Nustatyti",
				noResultsFound : "Nieko nerasta.",
				loadingInfoText : "U\u017eklaus\u0173 sluoksnis ${layerName} (${layerIndex} i\u0161 ${layersTotal}).",
				featureInfoTool : "Nustatyti",
				layer : "Sluoksnis",
				feature : "Objektas"
			},
			wms : {
				emptyResult : 'Nieko nerasta ant WMS sluoksnio "${layerTitle}".'
			}
		}
	},
	geometryservice : {
		bundleName : "GeometryService",
		bundleDescription : "Paketas suteikia centrin\u0119 geometrija paslaug\u0105."
	},
	geojson : {
		bundleName : "GeoJSON",
		bundleDescription : "Tai paketas suteikia konvertavimo paslauga, kuri gali apdoroti GeoJSON ir gerai \u017einom\u0105 tekst\u0105 (LCP)."
	},
	infoviewer : {
		bundleName : "Informacija Ekranas",
		bundleDescription : "Prane\u0161im\u0173 ekrane rodoma vartotojo materialin\u0119 informacij\u0105 apie d\u0117l lange \u017eem\u0117lapyje viet\u0105.",
		ui : {
			title : "Informacija per\u017ei\u016bros",
			tools : {
				closeInfoWindowMapdeskTool : "Arti",
				closeInfoWindowMapTool : "Arti",
				focusMapTool : "Centras \u017eem\u0117lapis",
				attachToGeorefTool : "Prid\u0117ti \u012f pad\u0117t\u012f",
				mainActivationTool : "Vietov\u0117 Informacija"
			}
		}
	},
	languagetoggler : {
		bundleName : "Kalba jungiklis",
		bundleDescription : "Per kalba perjungiklis s\u0105sajos kalba gali b\u016bti pakeista.",
		ui : {
			title : "Kalba"
		}
	},
	map : {
		bundleName : "\u017dem\u0117lapis",
		bundleDescription : "\u017dem\u0117lapis paketas valdo pagrindin\u012f \u017eem\u0117lap\u012f ir vis\u0105 informacij\u0105 \u017eem\u0117lapis turinio. Klientas gali abstrahuotis paslaugos, naudojami hierarchine \u017eem\u0117lapi\u0173 model\u012f taip, kad jie gali b\u016bti rodoma \u012fvairiai vartotojui.",
		ui : {
			nodeUpdateError : {
				info : 'Atnaujinti paslaugos "${title}" nepavyko! Klaida: ${errorMsg}',
				detail : 'Atnaujinti paslaugos "${title}" nepavyko! Klaida: ${url} ${errorMsg} - I\u0161samiau: ${error}'
			},
			sliderLabels : {
				country : "\u0160alis",
				region : "Regionas",
				town : "Miestas"
			}
		},
		drawTooltips : {
			addPoint : "Spauskite ant \u017eem\u0117lapio",
			addMultipoint : "Spauskite \u010dia",
			finishLabel : "Apdaila"
		}
	},
	mobiletoc : {
		bundleName : "Mobili lygio kontrol\u0117",
		bundleDescription : "Tai paketas suteikia mobili lygio reguliatorius paruo\u0161tas",
		tool : {
			title : "\u017dem\u0117lapis turinys",
			tooltip : "\u012ejungti / i\u0161jungti Medis turinys"
		},
		ui : {
			basemaps : "Netaurieji \u017eem\u0117lapiai",
			operationalLayers : "Temos lygiai",
			switchUI : {
				noTitle : "ne pavadinimas",
				leftLabel : "Apie",
				rightLabel : "Nuo"
			}
		}
	},
	mobileview : {
		bundleName : "Mobili Per\u017ei\u016br\u0117ti",
		bundleDescription : "Sud\u0117tyje yra speciali\u0173 klasi\u0173, kad nustatyti tam tikrus klausimus, susijusius su dojox.mobile"
	},
	parametermanager : {
		bundleName : "Parametras tvarkymas",
		bundleDescription : "Parametras direktorius yra atsakingas deleguoti parametrus i\u0161 URL \u012f pagal komponent\u0173 paleidimo.",
		ui : {
			encoderBtn : "Nuoroda \u012frankis",
			encoderBtnTooltip : "Nuoroda \u012frankis",
			sendMail : "E-mail",
			refresh : "Atnaujinti",
			linkBoxTitle : "Nuoroda URL",
			size : "Dydis (pikseliais)",
			codeBoxTitle : "Kodas \u012fd\u0117ti HTML",
			qrCode : "QRCode",
			mailBody : "${url}",
			mailSubject : "Pa\u017evelkite kart\u0173 \u0161i\u0105 kortel\u0119!",
			options : {
				small : "ma\u017eas (480 x 320)",
				medium : "terp\u0117 (640 x 480)",
				large : "didelis (1280 x 1024)",
				custom : "paprotys"
			}
		}
	},
	notifier : {
		bundleName : "Prane\u0161imai",
		bundleDescription : "Visi b\u016bsena, pa\u017eanga arba klaidos prane\u0161imai yra rodomi su \u012f pop-up prane\u0161imas naudotojui, kad yra ai\u0161ku, kas vyksta parai\u0161kos.",
		ui : {
			title : "Prane\u0161imai",
			tooltips : {
				close : "U\u017edaryti prane\u0161imas",
				glue : "Prid\u0117ti prane\u0161im\u0105"
			}
		}
	},
	printing : {
		bundleName : "Spausdinti",
		bundleDescription : "Tai paketas suteikia spausdinimo \u012frankis Spausdinti \u017eem\u0117lap\u012f.",
		tool : {
			title : "Spausdinti",
			tooltip : "Spausdinti",
			back : "Atgal"
		},
		resultWin : {
			title : "Spausdinti"
		}
	},
	qrcode : {
		bundleName : "QRCode",
		bundleDescription : "\u0160is pluo\u0161tas teikia paslaug\u0105, kuri gali b\u016bti naudojama siekiant sukurti QR kodus.",
		errorMessage : "QR kodas karta nebuvo s\u0117kmingas."
	},
	splashscreen : {
		bundleName : "Pagrindinis langas",
		bundleDescription : "Nam\u0173 ekrane rodomas progreso juost\u0105, o program\u0105.",
		loadTitle : 'Prad\u0117ti parai\u0161k\u0105 "{appName}"',
		loadBundle : "{percent}% - {startedBundles}/{maxBundlesToStart} - \u012ekrovimo {name}"
	},
	system : {
		bundleName : "Sistema",
		bundleDescription : "--CORE FUNKCIONALIT\u0100TE-- sistema apra\u0161yta pagrindin\u0119 pagrindin\u0119 funkcij\u0105 (compilable skeletas) i\u0161 map.apps."
	},
	templatelayout : {
		bundleName : "Per\u017ei\u016br\u0117ti i\u0161d\u0117stymas",
		bundleDescription : "--CORE FUNKCIONALIT\u0100TE-- \u0160ablonas i\u0161d\u0117stymas \u012fgyvendina vis\u0173 UI element\u0173 remiantis i\u0161 anksto apibr\u0117\u017etais \u0161ablon\u0173 susitarim\u0105."
	},
	templates : {
		bundleName : "Vaizdas",
		bundleDescription : "--CORE FUNKCIONALIT\u0100TE-- Per\u017ei\u016br\u0117jo naudoja \u0161ablon\u0173 maket\u0105.",
		templates : {
			desktop : {
				title : "Desktop",
				desc : "Darbastalio orientuotas i\u0161d\u0117stymas."
			},
			modern : {
				title : "Modernus",
				desc : "Modernus stilingas i\u0161d\u0117stymas."
			},
			minimal : {
				title : "Minimalus",
				desc : "Minimalistinis i\u0161d\u0117stymas"
			}
		},
		ui : {
			selectorLabelTitle : "Vaizdas"
		}
	},
	themes : {
		bundleName : "Stilius",
		bundleDescription : "--CORE FUNKCIONALIT\u0100TE-- Temos paketas valdo vis\u0105 CSS informacij\u0105, pavyzd\u017eiui, spalv\u0173, \u0161rift\u0173 stili\u0173, fono paveiksl\u0117lius ir tt Vartotojai galite perjungti \u012fvairi\u0173 i\u0161vaizda naudotojo s\u0105sajos element\u0173, pasirinkdami skirtingus stilius.",
		themes : {
			pure : {
				desc : 'The map.apps "grynas" stilius.'
			},
			night : {
				desc : 'The map.apps "nakties" stilius.'
			}
		},
		ui : {
			selectorLabelTitle : "Stilius"
		}
	},
	windowmanager : {
		bundleName : "Valdymo langas",
		bundleDescription : "--CORE FUNKCIONALIT\u0100TE--Lang\u0173 tvarkykl\u0117 yra atsakinga u\u017e vis\u0105 dialogo lang\u0105.",
		ui : {
			defaultWindowTitle : "Langas",
			closeBtn : {
				title : "Arti"
			},
			minimizeBtn : {
				title : "Minimizuoti"
			},
			maximizeBtn : {
				title : "Maksimizuoti"
			},
			restoreBtn : {
				title : "Atkurti"
			},
			opacityBtn : {
				title : "Pakeisti skaidrumas"
			},
			collapseBtn : {
				title : "Sl\u0117pti turin\u012f"
			},
			loading : {
				title : "Pra\u0161ome palaukti!",
				message : "\u012ekeliama ..."
			},
			okcancel : {
				title : "Klausimas",
				okButton : "Gerai",
				cancelButton : "At\u0161aukti"
			}
		}
	},
	ec_legend : {
		bundleName : "Legenda",
		bundleDescription : "Tai paketas suteikia ESRI legend\u0105.",
		tool : {
			title : "Legenda",
			tooltip : "Legenda"
		}
	},
	ec_parameter_interceptor : {
		bundleName : "ec-parameter-interceptor",
		bundleDescription : "ec-parameter-interceptor",
		errors : {
			missingQueryParameter : "Missing query parameters",
			missingTypeQueryParameter : "Missing type query parameter",
			missingDatasetOrUrlQueryParameter : "Missing dataset or url query parameter",
			noComponentContextProvided : "No componentContext provided",
			noBundleContextProvided : "No bundleContext provided",
			noQueryParametersProvided : "No query parameters provided",
			datasetTypeNotSupported : "The type '${type}' is not supported",
			invalidGeoJSON : "Invalid GeoJSON",
			unsupportedCRSType : "Only named CRS are supported",
			canNotParseNamedCRS : "Can not parse named CRS to WKID: ${name}",
			mixingOfDifferentCRS : "Mixing of different CRS is not supported",
			invalidGeoJSONType : "Unknown GeoJSON type attribute: ${type}",
			typeNotFoundInResource : "No appropriate format for ${type} found in resource",
			couldNotLoadDataset : "Could not load dataset: ${cause}",
			unableToAddDataset : "Unable to add dataset: ${cause}",
			unableToParseCapabilities : "Unable to parse WMS Capabilities response",
			canNotDetermineUrl : "Could not determine the download URL of the resource"
		}
	},
	ec_feature_info : {
		tool : {
			title : "Informacija apie funkcijas",
			tooltip : "Informace o prvku"
		}
	},
	ec_map_loading_screen : {
		bundleName : "ec-map-loading-screen",
		bundleDescription : "ec-map-loading-screen",
		messages : {
			initializingMap : "\u017Dem\u0117lapio inicijavimas. Pra\u0161ome palaukti!"
		}
	},
	ec_wms_layer_selector : {
		bundleName : "ec-wms-layer-selector",
		bundleDescription : "ec-wms-layer-selector",
		ui : {
			windowTitle : "Pasirinkti sluoksnius",
			hint : "Kai pasirinkta WMS paslauga turi ${countToProvideSelection} sluoksn\u012F (-ius), pasirinkite visus sluoksnius, kurie tur\u0117t\u0173 b\u016Bti rodomi \u017Eem\u0117lapyje.",
			selectAll : "Pasirinkti visk\u0105",
			closeButton : "Gerai"
		},
		tool : {
			title : "Sluoksnis",
			tooltip : "Sluoksnis"
		}
	},
	ec_error_messages : {
		bundleName : "ec-error-messages",
		bundleDescription : "ec-error-messages",
		general : {
			serviceNotAvailable : "Pageidaujama paslauga negalima",
			serviceNotSupported : "Pageidaujama paslauga nepalaikoma",
			invalidResource : "Netinkamas i\u0161teklius buvo nustatytas konkre\u010Diame URL",
			unsupportedResource : "Nepalaikomas i\u0161teklius buvo nustatytas konkre\u010Diame URL",
			errorSource : "Buvo nustatytas \u0161is klaidos \u0161altinis",
			requestedUrl : "Pageidaujamas URL",
			unsupportedServiceType : "The provided service type is not supported",
			technicalSupport : "Jei problema i\u0161liks, susisiekite su technin\u0117s paramos personalu",
			technicalSupportCreateTicket : "Create support ticket",
			weWillContactProvider : "We will contact the provider of the service and report the issue.",
			closeButton : "Gerai"
		},
		detailedErrors : {
			crs : "Paslauga neapima koordinuotos atskaitos sistemos",
			encoding : "Paslauga neapima \u017Eenkl\u0173 kodavimo",
			metadata : "Paslaugos metaduomen\u0173 dokumentas yra klaidingas",
			fileFormat : "Netinkamas duomen\u0173 formatas buvo gautas i\u0161 konkretaus URL",
			dataRequest : "Pateikus u\u017Eklaus\u0105 d\u0117l paslaugos duomen\u0173, buvo gautas netinkamas atsakymas. Tai gali b\u016Bti d\u0117l vidinio serverio klaidos, neteisingos paslaugos konfig\u016Bracijos, standart\u0173 neatitinkan\u010Dio paslaugos veikimo ir pan."
		},
		httpIssues : {
			303 : "Atsakymas \u012F pra\u0161ym\u0105 gali b\u016Bti pateiktas kitame URI",
			305 : "Pageidaujamas i\u0161teklius prieinamas tik per \u012Fgaliot\u0105j\u012F server\u012F",
			400 : "Serveris negali apdoroti arba neapdoros pra\u0161ymo d\u0117l pasteb\u0117tos kliento klaidos",
			401 : "Pageidaujamam i\u0161tekliui reikalingas leidimas",
			403 : "Pageidaujamas i\u0161teklius n\u0117ra vie\u0161ai prieinamas",
			404 : "Pageidaujamas i\u0161teklius negali b\u016Bti surastas",
			405 : "I\u0161tekliaus buvo papra\u0161yta naudojantis neleistinu HTTP metodu",
			406 : "I\u0161 i\u0161tekliaus buvo papra\u0161yta nepriimtino turinio",
			407 : "I\u0161tekliui reikalingas i\u0161ankstinis leidimas \u012Fgaliotajame serveryje",
			500 : "I\u0161teklius n\u0117ra prieinamas d\u0117l vidinio serverio klaidos",
			501 : "I\u0161teklius n\u0117ra prieinamas d\u0117l neatpa\u017Einto pra\u0161ymo metodo",
			502 : "I\u0161teklius n\u0117ra prieinamas d\u0117l klaidingai konfig\u016Bruoto tinkl\u0173 sietuvo",
			503 : "I\u0161teklius \u0161iuo metu n\u0117ra prieinamas d\u0117l perkrovos arba technin\u0117s prie\u017Ei\u016Bros",
			504 : "I\u0161teklius n\u0117ra prieinamas d\u0117l klaidingai konfig\u016Bruoto arba nereaguojan\u010Dio tinkl\u0173 sietuvo",
			505 : "I\u0161teklius nepalaiko esamos HTTP versijos",
			generic : "\u012Evyko serverio klaida pra\u0161ant konkretaus i\u0161tekliaus"
		}
	},
	ec_user_tutorial : {
		bundleName : "ec-user-tutorial",
		bundleDescription : "ec-user-tutorial",
		externalResourceDisclaimer : {
			introduction : "\u0160i \u017Eem\u0117lapi\u0173 per\u017Ei\u016Bros programa turi prieig\u0105 prie i\u0161or\u0117s paslaug\u0173 ir rodo j\u0173 duomenis. \u0160i\u0173 i\u0161or\u0117s paslaug\u0173 netvarko Europos Komisija, tod\u0117l mes neturime jokios \u012Ftakos j\u0173 prieinamumui\u00A0/ stabilumui. Gali kilti \u0161ios problemos:",
			rotatingCircle : "Besisukantis apskritimas \u017Eem\u0117lapyje rei\u0161kia, kad per\u017Ei\u016Bros programa laukia atsakymo i\u0161 duomen\u0173 \u0161altinio. Taip gali b\u016Bti d\u0117l neprieinamo serverio arba tinklo problem\u0173.",
			layerZoomLevel : "Kai kuri\u0173 sluoksni\u0173 \u0161altiniai gali b\u016Bti nematomi esant tam tikram masteliui. Taip yra d\u0117l apribojim\u0173, kuriuos nustat\u0117 i\u0161or\u0117s paslaug\u0173 teik\u0117jas. Jums gali prireikti padidinti arba suma\u017Einti vaizd\u0105, kad pamatytum\u0117te sluoksnio duomenis.",
			layerPanning : "Duomenys, gaunami i\u0161 i\u0161orinio serverio gali b\u016Bti pateikiami u\u017E esamo \u017Eem\u0117lapio vaizdo rib\u0173. \u0160ia \u017Eem\u0117lapi\u0173 per\u017Ei\u016Bros programa siekiama automati\u0161kai nustatyti apimam\u0105 srit\u012F. Ta\u010Diau kai kuriais atvejais metaduomenys n\u0117ra i\u0161sam\u016Bs arba yra klaidingi, tod\u0117l jums reik\u0117s pasirinkti panoramin\u012F vaizd\u0105.",
			detailInformation : "Jei pra\u0161ote i\u0161samios informacijos apie pasirinkt\u0105 funkcij\u0105, j\u016Bs\u0173 pra\u0161ymas bus perduotas \u012F \u017Eem\u0117lapi\u0173 server\u012F. Jei atsakymas \u012F \u0161\u012F pra\u0161ym\u0105 bus pateiktas netaisyklingai arba nepateiktas (pvz., d\u0117l serverio klaidos), informacija nebus rodoma, o vietoje to matysite tu\u0161\u010Di\u0105 lang\u0105.",
			mapScaling : "\u0160ioje programoje naudojamus bazinius \u017Eem\u0117lapius teikia Europos S\u0105jungos statistikos tarnyba (Eurostatas). \u0160iuo metu \u0161i\u0173 \u017Eem\u0117lapi\u0173 rezoliucija gali siekti iki 1:50.000. \u0160iuo metu rengiami smulkesni baziniai \u017Eem\u0117lapiai (pvz., kad palaikyt\u0173 miesto duomen\u0173 vizualizacij\u0105). Taigi, tam tikri duomen\u0173 \u0161altiniai kol kas gali b\u016Bti nerodomi su optimaliai pritaikytu foniniu \u017Eem\u0117lapiu.",
			technicalSupport : "Jei kils bet koki\u0173 kit\u0173 problem\u0173, nedelsdami susisiekite su technin\u0117s paramos personalu:",
			technicalSupportContactLink : "Susisiekite su technin\u0117s paramos personalu",
			mapLoading : "\u017Dem\u0117lapis \u012Fkeliamas ...",
			donotshow : "Daugiau nerodyti \u0161io pasisveikinimo ekrano."
		},
		legalNotice : "Naudojami pavadinimai ir med\u017Eiagos pateikimas \u0161iame \u017Eem\u0117lapyje nerei\u0161kia, kad Europos S\u0105jungos vardu parei\u0161kiama kokia nors nuomon\u0117 d\u0117l bet kokios \u0161alies, teritorijos, miesto, srities ar j\u0173 vald\u017Eios institucij\u0173 teisinio statuso arba d\u0117l j\u0173 sien\u0173 ar rib\u0173 nustatymo. Kosovas*: \u0160is pavadinimas nekei\u010Dia pozicij\u0173 d\u0117l statuso ir atitinka JT ST rezoliucij\u0105 1244/1999 bei Tarptautinio Teisingumo Teismo nuomon\u0119 d\u0117l Kosovo nepriklausomyb\u0117s deklaracijos. Palestina*: \u0160is pavadinimas neturi b\u016Bti ai\u0161kinamas kaip Palestinos Valstyb\u0117s pripa\u017Einimas ir jis nekei\u010Dia valstybi\u0173 nari\u0173 skirting\u0173 pozicij\u0173 \u0161iuo klausimu.",
		legalNoticeHeader : "Teisinis atsakomyb\u0117s ribojimas",
		tutorial : {
			welcome : "Sveiki atvyk\u0119 \u012F Europos duomen\u0173 portalo \u017Eem\u0117lapi\u0173 per\u017Ei\u016Bros program\u0105!",
			familiarise : "\u0160is trumpas \u012Fvadas skirtas supa\u017Eindinti jus su \u017Eem\u0117lapi\u0173 per\u017Ei\u016Bros programos elementais ir funkcionalumu.",
			navigation : "Spustel\u0117kite ir laikykite nuspaud\u0119 kair\u012Fj\u012F pel\u0117s mygtuk\u0105, kad matytum\u0117te panoramin\u012F \u017Eem\u0117lapio vaizd\u0105.",
			zoom : "\u0160ie mygtukai pakei\u010Dia \u017Eem\u0117lapio mastel\u012F. Mastel\u012F taip pat galite reguliuoti pel\u0117s ratuku.",
			features : "Papildomos funkcijos aktyvuojamos naudojant \u0161iuos mygtukus. Jie veikia kaip perjungikliai ir \u012Fjungia arba i\u0161jungia pasirinkt\u0105 funkcij\u0105.",
			legend : "Sutartiniai \u017Eym\u0117jimai gali b\u016Bti naudojami tyrin\u0117jant esamus \u017Eem\u0117lapio sluoksnius ir \u012Fjungti arba i\u0161jungti j\u0173 rodym\u0105 \u017Eem\u0117lapyje. J\u0173 pavadinimai yra tiesiogiai susij\u0119 su i\u0161or\u0117s paslaugomis.",
			transparency : "J\u016Bs taip pat galite reguliuoti sluoksnio skaidrum\u0105.",
			featureinfo : "Kai kurie duomenys gali b\u016Bti i\u0161tyrin\u0117ti dar i\u0161samiau. Kai \u0161i funkcija aktyvuota, j\u016Bs galite spustel\u0117ti ant \u017Eem\u0117lapio ir papra\u0161yti papildomos informacijos.",
			done : "Atlikta",
			okButton : "Gerai",
			closeButton : "U\u017Edaryti",
			skip : "Praleisti",
			next : "Toliau",
			back : "Atgal"
		}
	}
});