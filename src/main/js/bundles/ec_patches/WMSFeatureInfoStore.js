/*
 * Patched version of CT WMSFeatureInfoStore
 */
define(["featureinfo/WMSFeatureInfoStore"], function(WMSFeatureInfoStore) {
  var getMetadata = WMSFeatureInfoStore.prototype.getMetadata;
  if (getMetadata) {
    WMSFeatureInfoStore.prototype.getMetadata = function() {
      // just in case this gets patched at some point...
      var ret = getMetadata.apply(this, arguments);
      return ret ? ret : {};
    };
  }
});
