define(["dojo/_base/declare"], function(declare) {
  return declare([], {
    subLayoutPropertyName: "subLayoutName",
    activate: function() {
      this._mobile = false;
    },
    handleEvent: function(event) {
      if (event.getProperty(this.subLayoutPropertyName)) {
        var sublayoutname = event.getProperty(this.subLayoutPropertyName);
        this._mobile = sublayoutname.indexOf("mobile") > -1;
      }
    },
    isMobile: function() {
      return this._mobile;
    }
  });
});
